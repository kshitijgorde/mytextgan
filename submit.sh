#!/bin/bash

# parameters to shell commands
#PBS -l mem=4GB
USERNAME=npark2

# create each job
for train in *Th*.txt
do
for batch in 40
do
for temp in 0.01 0.001 0.0001
do
for radius in 0.1
do
for orig in 0.1
do
for lr in 0.003 0.001 0.0001
do

SCRIPT=train_${train}_temp_${temp}-lr_${lr}_batch_${batch}_radius_${radius}_orig_${orig}.sh

# basic slurm settings
echo \#!/bin/bash > ${SCRIPT}.sh 
echo \#PBS -l nodes=1:ppn=1  >> ${SCRIPT}.sh 
echo \#PBS -l walltime=700:00:00 >> ${SCRIPT}.sh 
echo \#PBS -l mem=8GB >> ${SCRIPT}.sh
echo \#PBS -q copperhead >> ${SCRIPT}.sh 

# commands will be executed
echo cd ${HOME}/workspace/mytextgan >> ${SCRIPT}.sh
echo module load openblas/0.2.18-nehalem >> ${SCRIPT}.sh
echo "OMP_NUM_THREADS=1 python model.py  --dataset ptb --batch_size ${batch} --pretrain_epochs 0 --hidden_size_g 64 --hidden_size_d 64 --lr_g ${lr} --temp ${temp} --lr_d ${lr} --lr_pretrain ${lr} --orig ${orig} --radius ${radius} --epochs 200 --over_num 40000 --train ${train}" >> ${SCRIPT}.sh

qsub ${SCRIPT}.sh

done
done
done
done
done
done
