nohup: ignoring input
DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0005
LEARNING_RATE_D: 0.0005
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.5
BETA1_D: 0.5
BETA2_G: 0.9
BETA2_D: 0.9
LOG_LOCATION: ./logs/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l5e-4/
SAMPLES_FOLDER: samples/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l5e-4/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6_was6/
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6_was6/pg_p_h32_l1e-2.chk
GAN_CHK_FOLDER: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l5e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l5e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l5e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5369)
(32, 20, 5369)
2017-04-15 02:44:42.502849: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 02:44:42.502887: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 02:44:42.502894: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 02:44:42.502899: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 02:44:42.502904: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 02:44:45.457745: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:86:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-15 02:44:45.457795: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-15 02:44:45.457803: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-15 02:44:45.457813: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:86:00.0)
WARNING:tensorflow:From model.py:289: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Test g_pre_loss after loading model: 6.31297604
2017-04-15 02:45:23.576918: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 5660 get requests, put_count=5625 evicted_count=1000 eviction_rate=0.177778 and unsatisfied allocation rate=0.20053
2017-04-15 02:45:23.576975: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-15 02:45:32.943896: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2201 get requests, put_count=2314 evicted_count=1000 eviction_rate=0.432152 and unsatisfied allocation rate=0.413448
2017-04-15 02:45:32.943955: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Test Loss after epoch 0: 6.30061385
Test Loss after epoch 1: 6.30174752
Test Loss after epoch 2: 6.29807220
Test Loss after epoch 3: 6.29892326
Test Loss after epoch 4: 6.30094770
Test Loss after epoch 5: 6.29566774
Test Loss after epoch 6: 6.29847204
Test Loss after epoch 7: 6.29957251
Test Loss after epoch 8: 6.29515373
Test Loss after epoch 9: 6.29575220
Test Loss after epoch 10: 6.29322324
Test Loss after epoch 11: 6.29671957
Test Loss after epoch 12: 6.29752717
Test Loss after epoch 13: 6.29977309
Test Loss after epoch 14: 6.29300547
Test Loss after epoch 15: 6.29789657
Test Loss after epoch 16: 6.29275519
Test Loss after epoch 17: 6.28793041
Test Loss after epoch 18: 6.28521496
Test Loss after epoch 19: 6.28998496
Test Loss after epoch 20: 6.29284027
Test Loss after epoch 21: 6.29318031
Test Loss after epoch 22: 6.28804885
Test Loss after epoch 23: 6.28508039
Test Loss after epoch 24: 6.28385728
Test Loss after epoch 25: 6.28538062
Test Loss after epoch 26: 6.29009528
Test Loss after epoch 27: 6.29517078
Test Loss after epoch 28: 6.29279953
Test Loss after epoch 29: 6.29591639
Test Loss after epoch 30: 6.28829622
Test Loss after epoch 31: 6.29095038
Test Loss after epoch 32: 6.29701682
Test Loss after epoch 33: 6.29880048
Test Loss after epoch 34: 6.30163493
Test Loss after epoch 35: 6.29981992
Test Loss after epoch 36: 6.29913232
Test Loss after epoch 37: 6.30536993
Test Loss after epoch 38: 6.30303003
Test Loss after epoch 39: 6.30508894
Test Loss after epoch 40: 6.30542881
Test Loss after epoch 41: 6.30036937
Test Loss after epoch 42: 6.29781788
Test Loss after epoch 43: 6.30266466
Test Loss after epoch 44: 6.30688284
Test Loss after epoch 45: 6.30637713
Test Loss after epoch 46: 6.31124220
Test Loss after epoch 47: 6.31893341
Test Loss after epoch 48: 6.32128499
Test Loss after epoch 49: 6.32137023
Test Loss after epoch 50: 6.31269735
Test Loss after epoch 51: 6.31405355
Test Loss after epoch 52: 6.31120757
Test Loss after epoch 53: 6.31292731
Test Loss after epoch 54: 6.31391893
Test Loss after epoch 55: 6.32238110
Test Loss after epoch 56: 6.32851527
Test Loss after epoch 57: 6.33528600
Test Loss after epoch 58: 6.33269598
Test Loss after epoch 59: 6.33605048
Test Loss after epoch 60: 6.33072327
Test Loss after epoch 61: 6.32278865
Test Loss after epoch 62: 6.33265754
Test Loss after epoch 63: 6.32944207
Test Loss after epoch 64: 6.32499352
Test Loss after epoch 65: 6.32254270
Test Loss after epoch 66: 6.31719925
Test Loss after epoch 67: 6.31260993
Test Loss after epoch 68: 6.31161982
Test Loss after epoch 69: 6.31275832
Test Loss after epoch 70: 6.31532843
Test Loss after epoch 71: 6.31542210
Test Loss after epoch 72: 6.31596096
Test Loss after epoch 73: 6.31261896
Test Loss after epoch 74: 6.31653047
Test Loss after epoch 75: 6.32067157
Test Loss after epoch 76: 6.31511184
Test Loss after epoch 77: 6.31164588
Test Loss after epoch 78: 6.31285251
Test Loss after epoch 79: 6.30810308
Test Loss after epoch 80: 6.31454329
Test Loss after epoch 81: 6.31221161
Test Loss after epoch 82: 6.30857125
Test Loss after epoch 83: 6.30899871
Test Loss after epoch 84: 6.30796277
Test Loss after epoch 85: 6.31317554
Test Loss after epoch 86: 6.31557502
Test Loss after epoch 87: 6.31531709
Test Loss after epoch 88: 6.31948242
Test Loss after epoch 89: 6.32533017
Test Loss after epoch 90: 6.32892882
Test Loss after epoch 91: 6.33506924
Test Loss after epoch 92: 6.32797421
Test Loss after epoch 93: 6.33060208
Test Loss after epoch 94: 6.33262696
Test Loss after epoch 95: 6.32649173
Test Loss after epoch 96: 6.32702611
Test Loss after epoch 97: 6.32572709
Test Loss after epoch 98: 6.33618789
Test Loss after epoch 99: 6.33459999
Test Loss after epoch 100: 6.32978736
Test Loss after epoch 101: 6.33422970
Test Loss after epoch 102: 6.33202137
Test Loss after epoch 103: 6.33313604
Test Loss after epoch 104: 6.33746750
Test Loss after epoch 105: 6.33736960
Test Loss after epoch 106: 6.33873044
Test Loss after epoch 107: 6.33103028
Test Loss after epoch 108: 6.32728524
Test Loss after epoch 109: 6.32243784
Test Loss after epoch 110: 6.33173524
Test Loss after epoch 111: 6.33054755
Test Loss after epoch 112: 6.32921974
Test Loss after epoch 113: 6.32476387
Test Loss after epoch 114: 6.32513011
Test Loss after epoch 115: 6.32725703
Test Loss after epoch 116: 6.32847254
Test Loss after epoch 117: 6.32997647
Test Loss after epoch 118: 6.33001715
Test Loss after epoch 119: 6.33175319
Test Loss after epoch 120: 6.32815164
Test Loss after epoch 121: 6.32290217
Test Loss after epoch 122: 6.31841540
Test Loss after epoch 123: 6.31719449
Test Loss after epoch 124: 6.31836007
Test Loss after epoch 125: 6.32304522
Test Loss after epoch 126: 6.32394035
Test Loss after epoch 127: 6.33015593
Test Loss after epoch 128: 6.32542532
Test Loss after epoch 129: 6.32529230
Test Loss after epoch 130: 6.32107458
Test Loss after epoch 131: 6.32323517
Test Loss after epoch 132: 6.32841982
Test Loss after epoch 133: 6.32724810
Test Loss after epoch 134: 6.32401337
Test Loss after epoch 135: 6.32346852
Test Loss after epoch 136: 6.32238276
Test Loss after epoch 137: 6.32587856
Test Loss after epoch 138: 6.33233786
Test Loss after epoch 139: 6.33903316
Test Loss after epoch 140: 6.33219524
Test Loss after epoch 141: 6.33391845
Test Loss after epoch 142: 6.33481584
Test Loss after epoch 143: 6.33472855
Test Loss after epoch 144: 6.32728520
Test Loss after epoch 145: 6.32938313
Test Loss after epoch 146: 6.33573337
Test Loss after epoch 147: 6.33889692
Test Loss after epoch 148: 6.33179449
Test Loss after epoch 149: 6.32570085
Test Loss after epoch 150: 6.32552604
Test Loss after epoch 151: 6.32337787
Test Loss after epoch 152: 6.32596333
Test Loss after epoch 153: 6.32696822
Test Loss after epoch 154: 6.32436212
Test Loss after epoch 155: 6.32288008
Test Loss after epoch 156: 6.32467368
Test Loss after epoch 157: 6.32034637
Test Loss after epoch 158: 6.31807613
Test Loss after epoch 159: 6.31863426
Test Loss after epoch 160: 6.31877215
Test Loss after epoch 161: 6.32658729
Test Loss after epoch 162: 6.32921256
Test Loss after epoch 163: 6.33257371
Test Loss after epoch 164: 6.33377175
Test Loss after epoch 165: 6.32951838
Test Loss after epoch 166: 6.32996894
Test Loss after epoch 167: 6.32687459
Test Loss after epoch 168: 6.32442968
Test Loss after epoch 169: 6.32244258
Test Loss after epoch 170: 6.32251309
Test Loss after epoch 171: 6.31917221
Test Loss after epoch 172: 6.32834313
Test Loss after epoch 173: 6.33044281
Test Loss after epoch 174: 6.33465918
Test Loss after epoch 175: 6.32955997
Test Loss after epoch 176: 6.33426771
Test Loss after epoch 177: 6.33144162
Test Loss after epoch 178: 6.33578033
Test Loss after epoch 179: 6.33316549
Test Loss after epoch 180: 6.33043134
Test Loss after epoch 181: 6.32771489
Test Loss after epoch 182: 6.33178591
Test Loss after epoch 183: 6.33294009
Test Loss after epoch 184: 6.33166176
Test Loss after epoch 185: 6.33452778
Test Loss after epoch 186: 6.35290973
Test Loss after epoch 187: 6.35096836
Test Loss after epoch 188: 6.33905788
Test Loss after epoch 189: 6.34277671
Test Loss after epoch 190: 6.35749179
Test Loss after epoch 191: 6.34665154
Test Loss after epoch 192: 6.34618910
Test Loss after epoch 193: 6.34641613
Test Loss after epoch 194: 6.34236891
Test Loss after epoch 195: 6.34602074
Test Loss after epoch 196: 6.34554012
Test Loss after epoch 197: 6.34460354
Test Loss after epoch 198: 6.34203888
Test Loss after epoch 199: 6.33996094
