DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 9
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 5e-05
LEARNING_RATE_D: 5e-05
N_EPOCHS: 0
NUM_D: 1
NUM_G: 3
LOG_LOCATION: ./logs/pg_g3d1_g32_d32_pe9_pl1e-2_l5e-5/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e9/
SAVE_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e9/pg_p_h32_l1e-2.chk
LOAD_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e9/pg_p_h32_l1e-2.chkb
GAN_CHK_FOLDER: ./checkpoints/pg_g3d1_g32_d32_pe9_pl1e-2_l5e-5/
SAVE_FILE_GAN: ./checkpoints/pg_g3d1_g32_d32_pe9_pl1e-2_l5e-5/chk
LOAD_FILE_GAN: ./checkpoints/pg_g3d1_g32_d32_pe9_pl1e-2_l5e-5/chk
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
2017-04-07 07:10:07.599732: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 07:10:07.599776: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 07:10:07.599782: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 07:10:07.599786: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 07:10:07.599789: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 07:10:18.008849: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:06:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-07 07:10:18.008884: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-07 07:10:18.008889: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-07 07:10:18.008896: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:06:00.0)
WARNING:tensorflow:From model.py:246: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Valid g_pre_loss before training: 8.58852345
Test g_pre_loss before training: 8.58831415
2017-04-07 07:10:37.177626: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 10728 get requests, put_count=10640 evicted_count=1000 eviction_rate=0.093985 and unsatisfied allocation rate=0.110738
2017-04-07 07:10:37.177696: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-07 07:10:43.205478: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 3816 get requests, put_count=3890 evicted_count=1000 eviction_rate=0.257069 and unsatisfied allocation rate=0.24869
2017-04-07 07:10:43.205575: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Train Loss after pre-train epoch 0: 7.22130679
Valid Loss after pre-train epoch 0: 6.94895065
Test Loss after pre-train epoch 0: 6.91328547
Train Loss after pre-train epoch 1: 6.95362685
Valid Loss after pre-train epoch 1: 6.67483887
Test Loss after pre-train epoch 1: 6.65106886
Train Loss after pre-train epoch 2: 6.67124248
Valid Loss after pre-train epoch 2: 6.49434815
Test Loss after pre-train epoch 2: 6.46192457
Train Loss after pre-train epoch 3: 6.40036229
Valid Loss after pre-train epoch 3: 6.42193814
Test Loss after pre-train epoch 3: 6.38622901
Train Loss after pre-train epoch 4: 6.16621360
Valid Loss after pre-train epoch 4: 6.39498398
Test Loss after pre-train epoch 4: 6.34734012
Train Loss after pre-train epoch 5: 5.97150309
Valid Loss after pre-train epoch 5: 6.40213564
Test Loss after pre-train epoch 5: 6.35271951
Train Loss after pre-train epoch 6: 5.81372264
Valid Loss after pre-train epoch 6: 6.42048214
Test Loss after pre-train epoch 6: 6.36993464
Train Loss after pre-train epoch 7: 5.68515543
Valid Loss after pre-train epoch 7: 6.47807126
Test Loss after pre-train epoch 7: 6.42315929
Train Loss after pre-train epoch 8: 5.57694162
Valid Loss after pre-train epoch 8: 6.51929933
Test Loss after pre-train epoch 8: 6.46957270
