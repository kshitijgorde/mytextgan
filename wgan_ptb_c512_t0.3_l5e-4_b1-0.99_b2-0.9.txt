nohup: ignoring input
DATASET: ptb
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 0
LEARNING_RATE_PRE_G: 0.001
HIDDEN_STATE_SIZE: 512
HIDDEN_STATE_SIZE_D: 512
LEARNING_RATE_G: 0.0005
LEARNING_RATE_D: 0.0005
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.99
BETA1_D: 0.99
BETA2_G: 0.9
BETA2_D: 0.9
LOG_LOCATION: ./logs/Wasspt_t0.3_b1-0.99_b2-0.9_g1d5_g512_d512_pe0_pl1e-3_l5e-4/
SAMPLES_FOLDER: samples/Wasspt_t0.3_b1-0.99_b2-0.9_g1d5_g512_d512_pe0_pl1e-3_l5e-4/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/Wasspt_t0.3_b1-0.99_b2-0.9_g1d5_g512_d512_pe0_pl1e-3_l5e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspt_t0.3_b1-0.99_b2-0.9_g1d5_g512_d512_pe0_pl1e-3_l5e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspt_t0.3_b1-0.99_b2-0.9_g1d5_g512_d512_pe0_pl1e-3_l5e-4/chk
TEMPERATURE: 0.3
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 99, 49)
(32, 99, 49)
2017-04-09 06:41:15.463568: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-09 06:41:15.463619: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-09 06:41:15.463627: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-09 06:41:15.463633: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-09 06:41:15.463638: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-09 06:41:18.484284: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:86:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-09 06:41:18.484345: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-09 06:41:18.484358: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-09 06:41:18.484372: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:86:00.0)
WARNING:tensorflow:From model.py:288: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
2017-04-09 06:44:10.265761: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6541 get requests, put_count=6518 evicted_count=1000 eviction_rate=0.153421 and unsatisfied allocation rate=0.171686
2017-04-09 06:44:10.265815: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
Test g_pre_loss before training: 3.84601382
Calculated in 465.214s
2017-04-09 06:51:57.566231: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2764873 get requests, put_count=2764577 evicted_count=3000 eviction_rate=0.00108516 and unsatisfied allocation rate=0.00119897
2017-04-09 06:52:05.040271: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 0 get requests, put_count=1033 evicted_count=1000 eviction_rate=0.968054 and unsatisfied allocation rate=0
2017-04-09 06:52:12.963834: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 4356 get requests, put_count=4407 evicted_count=1000 eviction_rate=0.226912 and unsatisfied allocation rate=0.235996
2017-04-09 06:52:12.963916: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 871 to 958
Test Loss after epoch 0: nan
Test Loss after epoch 1: nan
Test Loss after epoch 2: nan
