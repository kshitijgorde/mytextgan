DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0005
LEARNING_RATE_D: 0.0005
N_EPOCHS: 150
NUM_D: 3
NUM_G: 1
LOG_LOCATION: ./logs/pg_g1d3_g32_d32_pe6_pl1e-2_l5e-4/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6/
SAVE_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6/pg_p_h32_l1e-2.chk
LOAD_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6/pg_p_h32_l1e-2.chkb
GAN_CHK_FOLDER: ./checkpoints/pg_g1d3_g32_d32_pe6_pl1e-2_l5e-4/
SAVE_FILE_GAN: ./checkpoints/pg_g1d3_g32_d32_pe6_pl1e-2_l5e-4/chk
LOAD_FILE_GAN: ./checkpoints/pg_g1d3_g32_d32_pe6_pl1e-2_l5e-4/chk
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
2017-04-07 11:33:32.775462: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 11:33:32.775525: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 11:33:32.775544: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 11:33:32.775548: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 11:33:32.775553: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 11:33:35.711126: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:85:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-07 11:33:35.711207: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-07 11:33:35.711217: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-07 11:33:35.711229: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:85:00.0)
WARNING:tensorflow:From model.py:246: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Valid g_pre_loss after loading model: 6.32935958
Test g_pre_loss after loading model: 6.28785496
2017-04-07 11:33:57.358664: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 12298 get requests, put_count=12233 evicted_count=1000 eviction_rate=0.0817461 and unsatisfied allocation rate=0.0947309
2017-04-07 11:33:57.358718: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-07 11:34:06.462916: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6883 get requests, put_count=6936 evicted_count=1000 eviction_rate=0.144175 and unsatisfied allocation rate=0.140927
2017-04-07 11:34:06.462997: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Valid Loss after epoch 0: 6.33237839
Test Loss after epoch 0: 6.29187212
Valid Loss after epoch 1: 6.33199759
Test Loss after epoch 1: 6.28978143
Valid Loss after epoch 2: 6.33670691
Test Loss after epoch 2: 6.29438777
Valid Loss after epoch 3: 6.34641346
Test Loss after epoch 3: 6.30443914
Valid Loss after epoch 4: 6.35664786
Test Loss after epoch 4: 6.31395961
Valid Loss after epoch 5: 6.38054420
Test Loss after epoch 5: 6.33613951
Valid Loss after epoch 6: 6.38762102
Test Loss after epoch 6: 6.34461981
Valid Loss after epoch 7: 6.40064129
Test Loss after epoch 7: 6.35618374
Valid Loss after epoch 8: 6.43645743
Test Loss after epoch 8: 6.39268163
Valid Loss after epoch 9: 6.47212744
Test Loss after epoch 9: 6.42657515
Valid Loss after epoch 10: 6.53155169
Test Loss after epoch 10: 6.48511145
Valid Loss after epoch 11: 6.55955453
Test Loss after epoch 11: 6.51102154
2017-04-07 11:47:32.165627: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 5688066 get requests, put_count=5688081 evicted_count=5000 eviction_rate=0.000879031 and unsatisfied allocation rate=0.000883429
Valid Loss after epoch 12: 6.59046023
Test Loss after epoch 12: 6.54284147
Valid Loss after epoch 13: 6.59914923
Test Loss after epoch 13: 6.55159538
Valid Loss after epoch 14: 6.62752914
Test Loss after epoch 14: 6.57881955
Valid Loss after epoch 15: 6.63464846
Test Loss after epoch 15: 6.58651989
Valid Loss after epoch 16: 6.67159638
Test Loss after epoch 16: 6.62353619
Valid Loss after epoch 17: 6.69044816
Test Loss after epoch 17: 6.64296413
Valid Loss after epoch 18: 6.73560987
Test Loss after epoch 18: 6.68766068
Valid Loss after epoch 19: 6.82822534
Test Loss after epoch 19: 6.78286073
Valid Loss after epoch 20: 6.90932101
Test Loss after epoch 20: 6.86282526
Valid Loss after epoch 21: 6.99595223
Test Loss after epoch 21: 6.94825059
Valid Loss after epoch 22: 7.22263314
Test Loss after epoch 22: 7.16966092
Valid Loss after epoch 23: 7.37133533
Test Loss after epoch 23: 7.31788411
Valid Loss after epoch 24: 7.78665921
Test Loss after epoch 24: 7.73541527
Valid Loss after epoch 25: 7.59046209
Test Loss after epoch 25: 7.54016187
Valid Loss after epoch 26: 7.38023253
Test Loss after epoch 26: 7.33230116
Valid Loss after epoch 27: 7.18445387
Test Loss after epoch 27: 7.13796526
Valid Loss after epoch 28: 7.16373345
Test Loss after epoch 28: 7.12136175
Valid Loss after epoch 29: 7.19427194
Test Loss after epoch 29: 7.15218530
Valid Loss after epoch 30: 7.19546524
Test Loss after epoch 30: 7.15718714
Valid Loss after epoch 31: 7.07995230
Test Loss after epoch 31: 7.03744716
Valid Loss after epoch 32: 7.10616487
Test Loss after epoch 32: 7.06495462
Valid Loss after epoch 33: 7.25496466
Test Loss after epoch 33: 7.21282231
Valid Loss after epoch 34: 7.53814377
Test Loss after epoch 34: 7.49005079
2017-04-07 12:13:05.955498: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 16794187 get requests, put_count=16794202 evicted_count=15000 eviction_rate=0.000893165 and unsatisfied allocation rate=0.000894655
Valid Loss after epoch 35: 7.51666580
Test Loss after epoch 35: 7.46799855
Valid Loss after epoch 36: 7.57123084
Test Loss after epoch 36: 7.52558856
Valid Loss after epoch 37: 7.55392991
Test Loss after epoch 37: 7.50724259
Valid Loss after epoch 38: 7.50810149
Test Loss after epoch 38: 7.46417827
Valid Loss after epoch 39: 7.36310673
Test Loss after epoch 39: 7.31903052
Valid Loss after epoch 40: 7.18733245
Test Loss after epoch 40: 7.14645930
Valid Loss after epoch 41: 7.10629438
Test Loss after epoch 41: 7.06530429
Valid Loss after epoch 42: 7.07697956
Test Loss after epoch 42: 7.03680029
Valid Loss after epoch 43: 7.38161598
Test Loss after epoch 43: 7.34638356
Valid Loss after epoch 44: 7.33206323
Test Loss after epoch 44: 7.29348856
Valid Loss after epoch 45: 7.43279311
Test Loss after epoch 45: 7.39181937
Valid Loss after epoch 46: 7.23810145
Test Loss after epoch 46: 7.20395268
Valid Loss after epoch 47: 7.53735466
Test Loss after epoch 47: 7.49851005
Valid Loss after epoch 48: 8.13505928
Test Loss after epoch 48: 8.09417575
Valid Loss after epoch 49: 8.15459266
Test Loss after epoch 49: 8.11109772
Valid Loss after epoch 50: 7.77840494
Test Loss after epoch 50: 7.74369284
Valid Loss after epoch 51: 7.44502337
Test Loss after epoch 51: 7.41207595
Valid Loss after epoch 52: 7.13527425
Test Loss after epoch 52: 7.09447772
Valid Loss after epoch 53: 7.08137277
Test Loss after epoch 53: 7.04187710
Valid Loss after epoch 54: 7.01320860
Test Loss after epoch 54: 6.97137434
Valid Loss after epoch 55: 7.07371461
Test Loss after epoch 55: 7.03022086
Valid Loss after epoch 56: 7.06073017
Test Loss after epoch 56: 7.01597951
Valid Loss after epoch 57: 7.08098739
Test Loss after epoch 57: 7.03606759
2017-04-07 12:38:14.426443: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 27812720 get requests, put_count=27812735 evicted_count=25000 eviction_rate=0.000898869 and unsatisfied allocation rate=0.000899768
Valid Loss after epoch 58: 7.05643757
Test Loss after epoch 58: 7.01055261
Valid Loss after epoch 59: 7.05141157
Test Loss after epoch 59: 7.00507461
Valid Loss after epoch 60: 7.05654725
Test Loss after epoch 60: 7.00672416
Valid Loss after epoch 61: 7.06468728
Test Loss after epoch 61: 7.01367884
Valid Loss after epoch 62: 7.08343638
Test Loss after epoch 62: 7.03353008
Valid Loss after epoch 63: 7.11606818
Test Loss after epoch 63: 7.06434901
Valid Loss after epoch 64: 7.11722231
Test Loss after epoch 64: 7.06477308
Valid Loss after epoch 65: 7.14345482
Test Loss after epoch 65: 7.09228563
Valid Loss after epoch 66: 7.18652545
Test Loss after epoch 66: 7.13330580
Valid Loss after epoch 67: 7.24536721
Test Loss after epoch 67: 7.19285727
Valid Loss after epoch 68: 7.57038711
Test Loss after epoch 68: 7.52065989
Valid Loss after epoch 69: 7.56869227
Test Loss after epoch 69: 7.51758843
Valid Loss after epoch 70: 7.28481744
Test Loss after epoch 70: 7.23780223
Valid Loss after epoch 71: 7.15357533
Test Loss after epoch 71: 7.10351346
Valid Loss after epoch 72: 7.12152423
Test Loss after epoch 72: 7.07089246
Valid Loss after epoch 73: 7.11036313
Test Loss after epoch 73: 7.05996049
Valid Loss after epoch 74: 7.12708671
Test Loss after epoch 74: 7.07894311
Valid Loss after epoch 75: 7.11442031
Test Loss after epoch 75: 7.06494979
Valid Loss after epoch 76: 7.12155022
Test Loss after epoch 76: 7.07056152
Valid Loss after epoch 77: 7.15130900
Test Loss after epoch 77: 7.10212738
Valid Loss after epoch 78: 7.16694000
Test Loss after epoch 78: 7.11923387
2017-04-07 13:00:59.974916: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 37749896 get requests, put_count=37749911 evicted_count=35000 eviction_rate=0.000927155 and unsatisfied allocation rate=0.000927817
Valid Loss after epoch 79: 7.19645527
Test Loss after epoch 79: 7.14808492
Valid Loss after epoch 80: 7.19539298
Test Loss after epoch 80: 7.14701765
Valid Loss after epoch 81: 7.22037673
Test Loss after epoch 81: 7.17193505
Valid Loss after epoch 82: 7.27359919
Test Loss after epoch 82: 7.22674491
Valid Loss after epoch 83: 7.33526352
Test Loss after epoch 83: 7.28586646
Valid Loss after epoch 84: 7.39031962
Test Loss after epoch 84: 7.34186791
Valid Loss after epoch 85: 7.51325001
Test Loss after epoch 85: 7.46230615
Valid Loss after epoch 86: 7.73163083
Test Loss after epoch 86: 7.67812209
Valid Loss after epoch 87: 7.88521008
Test Loss after epoch 87: 7.82798759
Valid Loss after epoch 88: 8.30024967
Test Loss after epoch 88: 8.23992039
Valid Loss after epoch 89: 8.52154538
Test Loss after epoch 89: 8.45901277
Valid Loss after epoch 90: 8.31594028
Test Loss after epoch 90: 8.25470037
Valid Loss after epoch 91: 8.35341805
Test Loss after epoch 91: 8.29062045
Valid Loss after epoch 92: 8.15618604
Test Loss after epoch 92: 8.10026695
Valid Loss after epoch 93: 9.06712776
Test Loss after epoch 93: 9.00408511
Valid Loss after epoch 94: 8.68947873
Test Loss after epoch 94: 8.62805886
Valid Loss after epoch 95: 8.32447091
Test Loss after epoch 95: 8.26712922
Valid Loss after epoch 96: 8.26029183
Test Loss after epoch 96: 8.20934276
Valid Loss after epoch 97: 8.46484384
Test Loss after epoch 97: 8.41496040
Valid Loss after epoch 98: 9.42327350
Test Loss after epoch 98: 9.36510129
Valid Loss after epoch 99: 9.83237338
Test Loss after epoch 99: 9.77820649
2017-04-07 13:24:46.651681: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 48151002 get requests, put_count=48151017 evicted_count=45000 eviction_rate=0.00093456 and unsatisfied allocation rate=0.000935079
Valid Loss after epoch 100: 9.83149201
Test Loss after epoch 100: 9.79605561
Valid Loss after epoch 101: 9.77338409
Test Loss after epoch 101: 9.75053486
Valid Loss after epoch 102: 9.06150842
Test Loss after epoch 102: 9.04314681
Valid Loss after epoch 103: 8.53786084
Test Loss after epoch 103: 8.52301973
Valid Loss after epoch 104: 8.72185108
Test Loss after epoch 104: 8.69588710
Valid Loss after epoch 105: 9.33658695
Test Loss after epoch 105: 9.32354552
Valid Loss after epoch 106: 10.36243480
Test Loss after epoch 106: 10.35006031
Valid Loss after epoch 107: 11.08142141
Test Loss after epoch 107: 11.05636664
Valid Loss after epoch 108: 11.81100053
Test Loss after epoch 108: 11.78644377
Valid Loss after epoch 109: 12.56725383
Test Loss after epoch 109: 12.52670802
Valid Loss after epoch 110: 11.62018153
Test Loss after epoch 110: 11.60234541
Valid Loss after epoch 111: 9.76077017
Test Loss after epoch 111: 9.75678176
Valid Loss after epoch 112: 9.24701157
Test Loss after epoch 112: 9.24048178
Valid Loss after epoch 113: 8.98017144
Test Loss after epoch 113: 8.96954779
Valid Loss after epoch 114: 9.10032171
Test Loss after epoch 114: 9.09463363
Valid Loss after epoch 115: 9.17748281
Test Loss after epoch 115: 9.17071558
Valid Loss after epoch 116: 8.91290694
Test Loss after epoch 116: 8.90202455
Valid Loss after epoch 117: 8.81889495
Test Loss after epoch 117: 8.80469568
Valid Loss after epoch 118: 8.70092136
Test Loss after epoch 118: 8.68099902
Valid Loss after epoch 119: 8.54241872
Test Loss after epoch 119: 8.52160801
Valid Loss after epoch 120: 8.35166903
Test Loss after epoch 120: 8.31961123
Valid Loss after epoch 121: 8.42269707
Test Loss after epoch 121: 8.38535309
Valid Loss after epoch 122: 7.78547518
Test Loss after epoch 122: 7.75146180
Valid Loss after epoch 123: 7.52863412
Test Loss after epoch 123: 7.50366792
2017-04-07 13:50:20.658573: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 59273988 get requests, put_count=59273980 evicted_count=55000 eviction_rate=0.000927894 and unsatisfied allocation rate=0.000928704
Valid Loss after epoch 124: 7.40869109
Test Loss after epoch 124: 7.38345177
Valid Loss after epoch 125: 7.35601452
Test Loss after epoch 125: 7.33090598
Valid Loss after epoch 126: 7.29575478
Test Loss after epoch 126: 7.27386187
Valid Loss after epoch 127: 7.30066076
Test Loss after epoch 127: 7.27801140
Valid Loss after epoch 128: 7.46638528
Test Loss after epoch 128: 7.44663219
Valid Loss after epoch 129: 7.64958422
Test Loss after epoch 129: 7.63427291
Valid Loss after epoch 130: 7.70513465
Test Loss after epoch 130: 7.68860882
Valid Loss after epoch 131: 7.46729003
Test Loss after epoch 131: 7.45181565
Valid Loss after epoch 132: 7.36651401
Test Loss after epoch 132: 7.34794632
Valid Loss after epoch 133: 7.37978402
Test Loss after epoch 133: 7.36341355
Valid Loss after epoch 134: 7.37273294
Test Loss after epoch 134: 7.35301270
Valid Loss after epoch 135: 7.46691172
Test Loss after epoch 135: 7.44941958
Valid Loss after epoch 136: 7.45189074
Test Loss after epoch 136: 7.43493922
Valid Loss after epoch 137: 7.46986137
Test Loss after epoch 137: 7.45295012
Valid Loss after epoch 138: 7.49888818
Test Loss after epoch 138: 7.48375350
Valid Loss after epoch 139: 7.49819639
Test Loss after epoch 139: 7.48289860
Valid Loss after epoch 140: 7.49254978
Test Loss after epoch 140: 7.47249868
Valid Loss after epoch 141: 7.57527412
Test Loss after epoch 141: 7.55710917
Valid Loss after epoch 142: 7.63034779
Test Loss after epoch 142: 7.60935776
Valid Loss after epoch 143: 7.57974611
Test Loss after epoch 143: 7.55663621
Valid Loss after epoch 144: 7.59580973
Test Loss after epoch 144: 7.56909424
Valid Loss after epoch 145: 7.56393705
Test Loss after epoch 145: 7.53518752
Valid Loss after epoch 146: 7.57317916
Test Loss after epoch 146: 7.54636592
2017-04-07 14:15:28.144222: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 70289738 get requests, put_count=70289753 evicted_count=65000 eviction_rate=0.000924744 and unsatisfied allocation rate=0.000925099
Valid Loss after epoch 147: 7.58103029
Test Loss after epoch 147: 7.55004400
Valid Loss after epoch 148: 7.71406582
Test Loss after epoch 148: 7.68986651
Valid Loss after epoch 149: 7.71124369
Test Loss after epoch 149: 7.68470300
