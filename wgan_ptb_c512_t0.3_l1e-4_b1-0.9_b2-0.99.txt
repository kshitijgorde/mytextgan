nohup: ignoring input
DATASET: ptb
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 0
LEARNING_RATE_PRE_G: 0.001
HIDDEN_STATE_SIZE: 512
HIDDEN_STATE_SIZE_D: 512
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.9
BETA1_D: 0.9
BETA2_G: 0.99
BETA2_D: 0.99
LOG_LOCATION: ./logs/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l1e-4/
SAMPLES_FOLDER: samples/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l1e-4/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l1e-4/chk
TEMPERATURE: 0.3
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 99, 49)
(32, 99, 49)
2017-04-10 00:27:38.169071: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-10 00:27:38.169119: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-10 00:27:38.169127: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-10 00:27:38.169132: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-10 00:27:38.169138: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-10 00:27:41.207150: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:86:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-10 00:27:41.207214: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-10 00:27:41.207235: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-10 00:27:41.207272: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:86:00.0)
WARNING:tensorflow:From model.py:289: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
2017-04-10 00:30:37.919399: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6562 get requests, put_count=6559 evicted_count=1000 eviction_rate=0.152462 and unsatisfied allocation rate=0.168089
2017-04-10 00:30:37.919453: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
Test g_pre_loss before training: 3.85601837
Calculated in 465.339s
2017-04-10 00:38:24.863558: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2767597 get requests, put_count=2767401 evicted_count=3000 eviction_rate=0.00108405 and unsatisfied allocation rate=0.00116166
2017-04-10 00:38:32.420701: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 791 get requests, put_count=1446 evicted_count=1000 eviction_rate=0.691563 and unsatisfied allocation rate=0.482933
2017-04-10 00:38:32.420782: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 409 to 449
2017-04-10 00:38:42.930450: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6534 get requests, put_count=6633 evicted_count=1000 eviction_rate=0.150761 and unsatisfied allocation rate=0.152433
2017-04-10 00:38:42.930561: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 1053 to 1158
Test Loss after epoch 0: 3.22791257
Test Loss after epoch 1: 3.37006854
Test Loss after epoch 2: 3.31785391
Test Loss after epoch 3: 3.24058483
Test Loss after epoch 4: 3.30875553
Test Loss after epoch 5: 3.03110025
Test Loss after epoch 6: 3.00821111
Test Loss after epoch 7: 2.99075319
Test Loss after epoch 8: 2.96840726
Test Loss after epoch 9: 2.97184825
Test Loss after epoch 10: 2.96973277
Test Loss after epoch 11: 2.97172506
Test Loss after epoch 12: 2.97707381
Test Loss after epoch 13: 2.97901509
Test Loss after epoch 14: 2.98136545
Test Loss after epoch 15: 3.02457338
Test Loss after epoch 16: 3.02912677
Test Loss after epoch 17: 3.01264813
Test Loss after epoch 18: 3.06049395
Test Loss after epoch 19: 3.06831336
Test Loss after epoch 20: 3.02134365
Test Loss after epoch 21: 3.01458078
Test Loss after epoch 22: 3.04899179
Test Loss after epoch 23: 3.15260121
Test Loss after epoch 24: 3.07091667
2017-04-10 11:15:50.636988: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 159915237 get requests, put_count=159915339 evicted_count=8000 eviction_rate=5.00265e-05 and unsatisfied allocation rate=5.01828e-05
Test Loss after epoch 25: 3.04361019
Test Loss after epoch 26: 3.09819183
Test Loss after epoch 27: 3.06119251
Test Loss after epoch 28: 3.05346425
Test Loss after epoch 29: 3.08865934
Test Loss after epoch 30: 3.10313634
Test Loss after epoch 31: 3.17549985
Test Loss after epoch 32: 3.26756429
Test Loss after epoch 33: 3.09896201
Test Loss after epoch 34: 3.10814890
Test Loss after epoch 35: 3.09188331
Test Loss after epoch 36: 3.79592487
Test Loss after epoch 37: 3.03358286
Test Loss after epoch 38: 3.03906661
Test Loss after epoch 39: 3.02724013
Test Loss after epoch 40: 3.06617270
Test Loss after epoch 41: 3.10422757
Test Loss after epoch 42: 3.01862978
Test Loss after epoch 43: 3.11219213
Test Loss after epoch 44: 3.01469213
Test Loss after epoch 45: 3.01980749
Test Loss after epoch 46: 3.01210803
Test Loss after epoch 47: 4.17789262
Test Loss after epoch 48: 5.98923189
Test Loss after epoch 49: 4.18096992
Test Loss after epoch 50: 3.12024722
Test Loss after epoch 51: 3.09989329
Test Loss after epoch 52: 3.80782476
Test Loss after epoch 53: 3.02511943
Test Loss after epoch 54: 3.07485088
2017-04-10 23:05:26.363849: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 342291447 get requests, put_count=342291529 evicted_count=18000 eviction_rate=5.25868e-05 and unsatisfied allocation rate=5.27182e-05
Test Loss after epoch 55: 3.03529483
Test Loss after epoch 56: 3.05218267
Test Loss after epoch 57: 3.07679237
Test Loss after epoch 58: 3.04988964
