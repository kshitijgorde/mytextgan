DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 5e-05
LEARNING_RATE_D: 5e-05
N_EPOCHS: 0
NUM_D: 1
NUM_G: 3
LOG_LOCATION: ./logs/pg_g3d1_g32_d32_pe6_pl1e-2_l5e-5/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6_0/
SAVE_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6_0/pg_p_h32_l1e-2.chk
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/pg_g3d1_g32_d32_pe6_pl1e-2_l5e-5/
SAVE_FILE_GAN: ./checkpoints/pg_g3d1_g32_d32_pe6_pl1e-2_l5e-5/chk
LOAD_FILE_GAN: ./checkpoints/pg_g3d1_g32_d32_pe6_pl1e-2_l5e-5/chk
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
2017-04-07 08:43:46.679089: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:43:46.679128: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:43:46.679134: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:43:46.679139: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:43:46.679144: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:43:57.317615: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:06:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-07 08:43:57.317656: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-07 08:43:57.317663: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-07 08:43:57.317672: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:06:00.0)
WARNING:tensorflow:From model.py:246: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Valid g_pre_loss before training: 8.58941698
Test g_pre_loss before training: 8.58924223
2017-04-07 08:44:23.255524: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 10728 get requests, put_count=10643 evicted_count=1000 eviction_rate=0.0939585 and unsatisfied allocation rate=0.110459
2017-04-07 08:44:23.255600: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-07 08:44:29.128862: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 4293 get requests, put_count=4232 evicted_count=1000 eviction_rate=0.236295 and unsatisfied allocation rate=0.252504
2017-04-07 08:44:29.128941: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Train Loss after pre-train epoch 0: 7.22341149
Valid Loss after pre-train epoch 0: 6.94152848
Test Loss after pre-train epoch 0: 6.89825636
Train Loss after pre-train epoch 1: 6.93488058
Valid Loss after pre-train epoch 1: 6.64139596
Test Loss after pre-train epoch 1: 6.60413982
Train Loss after pre-train epoch 2: 6.62806936
Valid Loss after pre-train epoch 2: 6.49508359
Test Loss after pre-train epoch 2: 6.45551766
Train Loss after pre-train epoch 3: 6.35447094
Valid Loss after pre-train epoch 3: 6.35213518
Test Loss after pre-train epoch 3: 6.30789755
Train Loss after pre-train epoch 4: 6.10625169
Valid Loss after pre-train epoch 4: 6.30890846
Test Loss after pre-train epoch 4: 6.25715433
Train Loss after pre-train epoch 5: 5.91650272
Valid Loss after pre-train epoch 5: 6.31816612
Test Loss after pre-train epoch 5: 6.26607892
