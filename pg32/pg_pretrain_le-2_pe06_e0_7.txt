nohup: ignoring input
DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 0
NUM_D: 5
NUM_G: 1
BETA1_G: 0.5
BETA1_D: 0.5
BETA2_G: 0.9
BETA2_D: 0.9
LOG_LOCATION: ./logs/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAMPLES_FOLDER: samples/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6_was7/
SAVE_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6_was7/pg_p_h32_l1e-2.chk
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5369)
(32, 20, 5369)
2017-04-15 00:26:14.352337: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 00:26:14.352367: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 00:26:14.352373: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 00:26:14.352378: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 00:26:14.352382: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 00:26:20.018295: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:85:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-15 00:26:20.018344: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-15 00:26:20.018354: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-15 00:26:20.018367: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:85:00.0)
WARNING:tensorflow:From model.py:289: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Test g_pre_loss before training: 8.58760511
Calculated in 3.764s
2017-04-15 00:26:55.900225: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6248 get requests, put_count=6190 evicted_count=1000 eviction_rate=0.161551 and unsatisfied allocation rate=0.185339
2017-04-15 00:26:55.900306: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-15 00:27:02.299773: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 4293 get requests, put_count=4252 evicted_count=1000 eviction_rate=0.235183 and unsatisfied allocation rate=0.247845
2017-04-15 00:27:02.299849: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Train Loss after pre-train epoch 0: 7.22854127
Test Loss after pre-train epoch 0: 6.92067166
Train Loss after pre-train epoch 1: 6.95679057
Test Loss after pre-train epoch 1: 6.64049252
Train Loss after pre-train epoch 2: 6.66504931
Test Loss after pre-train epoch 2: 6.44265195
Train Loss after pre-train epoch 3: 6.37618961
Test Loss after pre-train epoch 3: 6.32143697
Train Loss after pre-train epoch 4: 6.13043794
Test Loss after pre-train epoch 4: 6.28149571
Train Loss after pre-train epoch 5: 5.94300433
Test Loss after pre-train epoch 5: 6.28157853
