nohup: ignoring input
DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.9
BETA1_D: 0.9
BETA2_G: 0.99
BETA2_D: 0.99
LOG_LOCATION: ./logs/Wasspg_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAMPLES_FOLDER: samples/Wasspg_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6_was6/
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6_was6/pg_p_h32_l1e-2.chk
GAN_CHK_FOLDER: ./checkpoints/Wasspg_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5369)
(32, 20, 5369)
2017-04-15 02:14:40.824847: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 02:14:40.824892: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 02:14:40.824898: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 02:14:40.824903: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 02:14:40.824908: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 02:14:49.011521: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:07:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-15 02:14:49.011564: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-15 02:14:49.011571: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-15 02:14:49.011579: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:07:00.0)
WARNING:tensorflow:From model.py:289: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Test g_pre_loss after loading model: 6.31064601
2017-04-15 02:15:22.747527: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 5660 get requests, put_count=5627 evicted_count=1000 eviction_rate=0.177715 and unsatisfied allocation rate=0.200177
2017-04-15 02:15:22.747579: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-15 02:15:32.121650: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2643 get requests, put_count=2574 evicted_count=1000 eviction_rate=0.3885 and unsatisfied allocation rate=0.413167
2017-04-15 02:15:32.121698: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Test Loss after epoch 0: 6.30956825
Test Loss after epoch 1: 6.30864369
Test Loss after epoch 2: 6.30881879
Test Loss after epoch 3: 6.30621333
Test Loss after epoch 4: 6.30644746
Test Loss after epoch 5: 6.30468535
Test Loss after epoch 6: 6.30451907
Test Loss after epoch 7: 6.30461753
Test Loss after epoch 8: 6.30369196
Test Loss after epoch 9: 6.30393760
Test Loss after epoch 10: 6.30370617
Test Loss after epoch 11: 6.30279796
Test Loss after epoch 12: 6.30213311
Test Loss after epoch 13: 6.30093092
Test Loss after epoch 14: 6.30022453
Test Loss after epoch 15: 6.29991533
Test Loss after epoch 16: 6.29954057
Test Loss after epoch 17: 6.29865633
Test Loss after epoch 18: 6.29891506
Test Loss after epoch 19: 6.29856090
Test Loss after epoch 20: 6.29896565
Test Loss after epoch 21: 6.29656139
Test Loss after epoch 22: 6.29724589
Test Loss after epoch 23: 6.29711617
Test Loss after epoch 24: 6.29620531
Test Loss after epoch 25: 6.29610472
Test Loss after epoch 26: 6.29785322
Test Loss after epoch 27: 6.29639922
Test Loss after epoch 28: 6.29676908
Test Loss after epoch 29: 6.29581838
Test Loss after epoch 30: 6.29630559
Test Loss after epoch 31: 6.29704832
Test Loss after epoch 32: 6.29658667
Test Loss after epoch 33: 6.29592916
Test Loss after epoch 34: 6.29519838
Test Loss after epoch 35: 6.29375312
Test Loss after epoch 36: 6.29524640
Test Loss after epoch 37: 6.29567263
Test Loss after epoch 38: 6.29477289
Test Loss after epoch 39: 6.29328557
Test Loss after epoch 40: 6.29302429
Test Loss after epoch 41: 6.29454513
Test Loss after epoch 42: 6.29412868
Test Loss after epoch 43: 6.29519412
Test Loss after epoch 44: 6.29269555
Test Loss after epoch 45: 6.29217898
Test Loss after epoch 46: 6.29333112
Test Loss after epoch 47: 6.29485186
Test Loss after epoch 48: 6.29320714
Test Loss after epoch 49: 6.29359210
Test Loss after epoch 50: 6.29510632
Test Loss after epoch 51: 6.29285399
Test Loss after epoch 52: 6.29253455
Test Loss after epoch 53: 6.29071802
Test Loss after epoch 54: 6.29150766
Test Loss after epoch 55: 6.29103350
Test Loss after epoch 56: 6.29084319
Test Loss after epoch 57: 6.29087420
Test Loss after epoch 58: 6.29015676
Test Loss after epoch 59: 6.28989080
Test Loss after epoch 60: 6.29027357
Test Loss after epoch 61: 6.28962500
Test Loss after epoch 62: 6.29047091
Test Loss after epoch 63: 6.28900902
Test Loss after epoch 64: 6.29070777
Test Loss after epoch 65: 6.28975636
Test Loss after epoch 66: 6.28904546
Test Loss after epoch 67: 6.28785143
Test Loss after epoch 68: 6.28927458
Test Loss after epoch 69: 6.28808065
Test Loss after epoch 70: 6.28818672
Test Loss after epoch 71: 6.28882676
Test Loss after epoch 72: 6.28803725
Test Loss after epoch 73: 6.28674278
Test Loss after epoch 74: 6.28665267
Test Loss after epoch 75: 6.28610440
Test Loss after epoch 76: 6.28651860
Test Loss after epoch 77: 6.28500671
Test Loss after epoch 78: 6.28517629
Test Loss after epoch 79: 6.28490516
Test Loss after epoch 80: 6.28494903
Test Loss after epoch 81: 6.28488733
Test Loss after epoch 82: 6.28524548
Test Loss after epoch 83: 6.28484349
Test Loss after epoch 84: 6.28463896
Test Loss after epoch 85: 6.28668262
Test Loss after epoch 86: 6.28488690
Test Loss after epoch 87: 6.28469116
Test Loss after epoch 88: 6.28666789
Test Loss after epoch 89: 6.28685996
Test Loss after epoch 90: 6.28637668
Test Loss after epoch 91: 6.28476890
Test Loss after epoch 92: 6.28571590
Test Loss after epoch 93: 6.28708040
Test Loss after epoch 94: 6.28770808
Test Loss after epoch 95: 6.28747322
Test Loss after epoch 96: 6.28761390
Test Loss after epoch 97: 6.28581945
Test Loss after epoch 98: 6.28785124
Test Loss after epoch 99: 6.28816314
Test Loss after epoch 100: 6.28779587
Test Loss after epoch 101: 6.28785423
Test Loss after epoch 102: 6.28819083
Test Loss after epoch 103: 6.28836821
Test Loss after epoch 104: 6.28675104
Test Loss after epoch 105: 6.28785732
Test Loss after epoch 106: 6.28752767
Test Loss after epoch 107: 6.28730562
Test Loss after epoch 108: 6.28909033
Test Loss after epoch 109: 6.28720570
Test Loss after epoch 110: 6.28732829
Test Loss after epoch 111: 6.28742210
Test Loss after epoch 112: 6.28878841
Test Loss after epoch 113: 6.28998643
Test Loss after epoch 114: 6.28882514
Test Loss after epoch 115: 6.29065629
Test Loss after epoch 116: 6.29019726
Test Loss after epoch 117: 6.29117904
Test Loss after epoch 118: 6.29205511
Test Loss after epoch 119: 6.29070739
Test Loss after epoch 120: 6.28984419
Test Loss after epoch 121: 6.29007487
Test Loss after epoch 122: 6.29092533
Test Loss after epoch 123: 6.28970074
Test Loss after epoch 124: 6.29086091
Test Loss after epoch 125: 6.29076792
Test Loss after epoch 126: 6.28935974
Test Loss after epoch 127: 6.29033707
Test Loss after epoch 128: 6.29167984
Test Loss after epoch 129: 6.29200925
Test Loss after epoch 130: 6.28999975
Test Loss after epoch 131: 6.29150904
Test Loss after epoch 132: 6.28994317
Test Loss after epoch 133: 6.28945997
Test Loss after epoch 134: 6.28957064
Test Loss after epoch 135: 6.29148149
Test Loss after epoch 136: 6.28949313
Test Loss after epoch 137: 6.28861500
Test Loss after epoch 138: 6.29037325
Test Loss after epoch 139: 6.29066655
Test Loss after epoch 140: 6.29236517
Test Loss after epoch 141: 6.29084304
Test Loss after epoch 142: 6.28741809
Test Loss after epoch 143: 6.28749004
Test Loss after epoch 144: 6.28951287
Test Loss after epoch 145: 6.28978925
Test Loss after epoch 146: 6.28933376
Test Loss after epoch 147: 6.28687831
Test Loss after epoch 148: 6.28662334
Test Loss after epoch 149: 6.28697177
Test Loss after epoch 150: 6.28805670
Test Loss after epoch 151: 6.29027290
Test Loss after epoch 152: 6.28796545
Test Loss after epoch 153: 6.28804745
Test Loss after epoch 154: 6.28718207
Test Loss after epoch 155: 6.28685767
Test Loss after epoch 156: 6.28607670
Test Loss after epoch 157: 6.28593439
Test Loss after epoch 158: 6.28536143
Test Loss after epoch 159: 6.28537410
Test Loss after epoch 160: 6.28622827
Test Loss after epoch 161: 6.28512693
Test Loss after epoch 162: 6.28631456
Test Loss after epoch 163: 6.28560172
Test Loss after epoch 164: 6.28633533
Test Loss after epoch 165: 6.28620795
Test Loss after epoch 166: 6.28842986
Test Loss after epoch 167: 6.28682675
Test Loss after epoch 168: 6.28718098
Test Loss after epoch 169: 6.28721797
Test Loss after epoch 170: 6.28563835
Test Loss after epoch 171: 6.28667931
Test Loss after epoch 172: 6.28807682
Test Loss after epoch 173: 6.28872881
Test Loss after epoch 174: 6.28898627
Test Loss after epoch 175: 6.28760533
Test Loss after epoch 176: 6.28668445
Test Loss after epoch 177: 6.28613006
Test Loss after epoch 178: 6.28658524
Test Loss after epoch 179: 6.28621603
Test Loss after epoch 180: 6.28646408
Test Loss after epoch 181: 6.28561011
Test Loss after epoch 182: 6.28416132
Test Loss after epoch 183: 6.28445682
Test Loss after epoch 184: 6.28590725
Test Loss after epoch 185: 6.28515027
Test Loss after epoch 186: 6.28437710
Test Loss after epoch 187: 6.28532313
Test Loss after epoch 188: 6.28408887
Test Loss after epoch 189: 6.28355371
Test Loss after epoch 190: 6.28541673
Test Loss after epoch 191: 6.28543600
Test Loss after epoch 192: 6.28694233
Test Loss after epoch 193: 6.28553403
Test Loss after epoch 194: 6.28579498
Test Loss after epoch 195: 6.28614183
Test Loss after epoch 196: 6.28745028
Test Loss after epoch 197: 6.28742452
Test Loss after epoch 198: 6.28784058
Test Loss after epoch 199: 6.28743439
