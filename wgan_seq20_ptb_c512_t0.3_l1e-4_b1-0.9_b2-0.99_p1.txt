nohup: ignoring input
DATASET: ptb
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 1
LEARNING_RATE_PRE_G: 0.001
HIDDEN_STATE_SIZE: 512
HIDDEN_STATE_SIZE_D: 512
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.9
BETA1_D: 0.9
BETA2_G: 0.99
BETA2_D: 0.99
LOG_LOCATION: ./logs/Wass_seq20pt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe1_pl1e-3_l1e-4/
SAMPLES_FOLDER: samples/Wass_seq20pt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe1_pl1e-3_l1e-4/
PRETRAIN_CHK_FOLDER: ./checkpoints/Wass_seq20pt_p_h512_l1e-3_e1/
SAVE_FILE_PRETRAIN: ./checkpoints/Wass_seq20pt_p_h512_l1e-3_e1/pt_p_h512_l1e-3.chk
LOAD_FILE_PRETRAIN: ./checkpoints/Wass_seq20pt_p_h512_l1e-3_e1/pt_p_h512_l1e-3.chk
GAN_CHK_FOLDER: ./checkpoints/Wass_seq20pt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe1_pl1e-3_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wass_seq20pt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe1_pl1e-3_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wass_seq20pt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe1_pl1e-3_l1e-4/chk
TEMPERATURE: 0.3
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 19, 49)
(32, 19, 49)
2017-04-11 23:53:07.019623: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-11 23:53:07.019659: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-11 23:53:07.019666: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-11 23:53:07.019671: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-11 23:53:07.019677: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-11 23:53:12.608059: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:06:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-11 23:53:12.608113: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-11 23:53:12.608124: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-11 23:53:12.608136: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:06:00.0)
WARNING:tensorflow:From model.py:289: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Test g_pre_loss before training: 3.91359045
Calculated in 386.953s
2017-04-12 00:00:12.123429: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2816358 get requests, put_count=2816344 evicted_count=1000 eviction_rate=0.00035507 and unsatisfied allocation rate=0.000395546
2017-04-12 00:00:14.121491: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2325 get requests, put_count=2333 evicted_count=1000 eviction_rate=0.428633 and unsatisfied allocation rate=0.432688
2017-04-12 00:00:14.121556: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 160 to 176
Train Loss after pre-train epoch 0: 1.72289704
Test Loss after pre-train epoch 0: 1.52413129
2017-04-12 00:12:57.426553: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 5869712 get requests, put_count=5869693 evicted_count=2000 eviction_rate=0.000340733 and unsatisfied allocation rate=0.000349591
2017-04-12 00:14:35.876442: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6195878 get requests, put_count=6195843 evicted_count=12000 eviction_rate=0.00193678 and unsatisfied allocation rate=0.00194775
Test Loss after epoch 0: 1.58071766
Test Loss after epoch 1: 1.61998229
Test Loss after epoch 2: 1.67119205
Test Loss after epoch 3: 1.69107847
Test Loss after epoch 4: 1.70494650
Test Loss after epoch 5: 1.71082121
Test Loss after epoch 6: 1.72830922
Test Loss after epoch 7: 1.72355601
Test Loss after epoch 8: 1.74943156
Test Loss after epoch 9: 1.76795240
Test Loss after epoch 10: 1.76888575
Test Loss after epoch 11: 1.80864737
Test Loss after epoch 12: 1.81935709
Test Loss after epoch 13: 1.86101063
Test Loss after epoch 14: 1.92603799
Test Loss after epoch 15: 1.92890748
Test Loss after epoch 16: 1.92747878
Test Loss after epoch 17: 1.94076250
Test Loss after epoch 18: 1.94782274
