nohup: ignoring input
DATASET: oracle
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 20
LEARNING_RATE_PRE_G: 0.001
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0005
LEARNING_RATE_D: 0.0005
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.5
BETA1_D: 0.5
BETA2_G: 0.9
BETA2_D: 0.9
LOG_LOCATION: ./logs/Wassor_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe20_pl1e-3_l5e-4/
SAMPLES_FOLDER: samples/Wassor_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe20_pl1e-3_l5e-4/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/Wassor_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe20_pl1e-3_l5e-4/
SAVE_FILE_GAN: ./checkpoints/Wassor_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe20_pl1e-3_l5e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wassor_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe20_pl1e-3_l5e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5000)
(32, 20, 5000)
2017-04-13 21:35:53.748669: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-13 21:35:53.748713: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-13 21:35:53.748721: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-13 21:35:53.748727: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-13 21:35:53.748734: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-13 21:35:59.145766: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:85:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-13 21:35:59.145834: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-13 21:35:59.145850: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-13 21:35:59.145868: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:85:00.0)
WARNING:tensorflow:From model.py:289: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
NLL Oracle Loss before training: 12.14638710
2017-04-13 21:36:57.116404: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 49644 get requests, put_count=49557 evicted_count=1000 eviction_rate=0.0201788 and unsatisfied allocation rate=0.0239102
2017-04-13 21:36:57.116470: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-13 21:37:02.965280: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 4293 get requests, put_count=4228 evicted_count=1000 eviction_rate=0.236518 and unsatisfied allocation rate=0.253436
2017-04-13 21:37:02.965418: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Train Loss after pre-train epoch 0: 8.04530833
NLL Oracle Loss after pre-train epoch 0: 9.24201679
Train Loss after pre-train epoch 1: 7.86396872
NLL Oracle Loss after pre-train epoch 1: 9.18905735
Train Loss after pre-train epoch 2: 7.83980383
NLL Oracle Loss after pre-train epoch 2: 9.39304924
Train Loss after pre-train epoch 3: 7.82108058
NLL Oracle Loss after pre-train epoch 3: 9.50913334
Train Loss after pre-train epoch 4: 7.80282524
NLL Oracle Loss after pre-train epoch 4: 9.64306450
Train Loss after pre-train epoch 5: 7.78606164
NLL Oracle Loss after pre-train epoch 5: 9.90043163
Train Loss after pre-train epoch 6: 7.77326861
NLL Oracle Loss after pre-train epoch 6: 10.14828682
Train Loss after pre-train epoch 7: 7.76254115
NLL Oracle Loss after pre-train epoch 7: 10.47246075
Train Loss after pre-train epoch 8: 7.74980328
NLL Oracle Loss after pre-train epoch 8: 10.67151260
Train Loss after pre-train epoch 9: 7.72845962
NLL Oracle Loss after pre-train epoch 9: 9.63410950
Train Loss after pre-train epoch 10: 7.70364515
NLL Oracle Loss after pre-train epoch 10: 8.85552311
Train Loss after pre-train epoch 11: 7.67920160
NLL Oracle Loss after pre-train epoch 11: 8.52491665
Train Loss after pre-train epoch 12: 7.65516565
NLL Oracle Loss after pre-train epoch 12: 8.45150661
Train Loss after pre-train epoch 13: 7.63113560
NLL Oracle Loss after pre-train epoch 13: 8.46538353
Train Loss after pre-train epoch 14: 7.60615813
NLL Oracle Loss after pre-train epoch 14: 8.47180462
Train Loss after pre-train epoch 15: 7.58143982
NLL Oracle Loss after pre-train epoch 15: 8.48430634
Train Loss after pre-train epoch 16: 7.55675131
NLL Oracle Loss after pre-train epoch 16: 8.48747826
Train Loss after pre-train epoch 17: 7.53316854
NLL Oracle Loss after pre-train epoch 17: 8.48422146
Train Loss after pre-train epoch 18: 7.50644510
NLL Oracle Loss after pre-train epoch 18: 8.49939156
Train Loss after pre-train epoch 19: 7.47825167
NLL Oracle Loss after pre-train epoch 19: 8.51539612
2017-04-13 21:56:28.881868: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 3913779 get requests, put_count=3913732 evicted_count=7000 eviction_rate=0.00178857 and unsatisfied allocation rate=0.00180899
NLL Oracle Loss after epoch 0: 8.36758709
NLL Oracle Loss after epoch 1: 8.36515331
NLL Oracle Loss after epoch 2: 8.83006096
NLL Oracle Loss after epoch 3: 8.45406151
NLL Oracle Loss after epoch 4: 8.44446850
NLL Oracle Loss after epoch 5: 8.55150318
NLL Oracle Loss after epoch 6: 8.28042698
NLL Oracle Loss after epoch 7: 8.32470131
NLL Oracle Loss after epoch 8: 7.83431768
NLL Oracle Loss after epoch 9: 8.52696037
NLL Oracle Loss after epoch 10: 9.34701252
NLL Oracle Loss after epoch 11: 8.17250919
NLL Oracle Loss after epoch 12: 8.86408997
NLL Oracle Loss after epoch 13: 8.85016441
NLL Oracle Loss after epoch 14: 8.73015690
NLL Oracle Loss after epoch 15: 8.50793743
NLL Oracle Loss after epoch 16: 8.53762913
NLL Oracle Loss after epoch 17: 8.51741886
NLL Oracle Loss after epoch 18: 8.79665947
NLL Oracle Loss after epoch 19: 8.88508987
NLL Oracle Loss after epoch 20: 8.74486065
NLL Oracle Loss after epoch 21: 8.17092419
NLL Oracle Loss after epoch 22: 8.50330067
NLL Oracle Loss after epoch 23: 8.64903736
NLL Oracle Loss after epoch 24: 8.34917164
NLL Oracle Loss after epoch 25: 8.35171890
NLL Oracle Loss after epoch 26: 8.30775833
NLL Oracle Loss after epoch 27: 8.54445362
NLL Oracle Loss after epoch 28: 8.29297161
NLL Oracle Loss after epoch 29: 8.38180065
NLL Oracle Loss after epoch 30: 8.47443295
NLL Oracle Loss after epoch 31: 8.47711182
NLL Oracle Loss after epoch 32: 8.52606487
NLL Oracle Loss after epoch 33: 8.56193542
NLL Oracle Loss after epoch 34: 8.61509800
NLL Oracle Loss after epoch 35: 8.39306355
NLL Oracle Loss after epoch 36: 8.17188454
NLL Oracle Loss after epoch 37: 8.50570488
NLL Oracle Loss after epoch 38: 8.47097206
NLL Oracle Loss after epoch 39: 8.62191391
NLL Oracle Loss after epoch 40: 8.28262424
NLL Oracle Loss after epoch 41: 8.46677208
NLL Oracle Loss after epoch 42: 8.61268806
NLL Oracle Loss after epoch 43: 8.85943794
NLL Oracle Loss after epoch 44: 8.53599358
NLL Oracle Loss after epoch 45: 8.51552486
NLL Oracle Loss after epoch 46: 8.75209522
NLL Oracle Loss after epoch 47: 8.39103889
NLL Oracle Loss after epoch 48: 8.47297192
NLL Oracle Loss after epoch 49: 8.65940189
NLL Oracle Loss after epoch 50: 8.62218475
NLL Oracle Loss after epoch 51: 8.70749569
NLL Oracle Loss after epoch 52: 8.79550743
NLL Oracle Loss after epoch 53: 8.57786179
NLL Oracle Loss after epoch 54: 8.63147736
NLL Oracle Loss after epoch 55: 8.42634010
NLL Oracle Loss after epoch 56: 8.40343571
NLL Oracle Loss after epoch 57: 8.90875530
NLL Oracle Loss after epoch 58: 8.65655994
NLL Oracle Loss after epoch 59: 8.19700146
NLL Oracle Loss after epoch 60: 8.90472031
NLL Oracle Loss after epoch 61: 9.27600098
NLL Oracle Loss after epoch 62: 9.41814423
NLL Oracle Loss after epoch 63: 8.83953381
NLL Oracle Loss after epoch 64: 8.64944363
NLL Oracle Loss after epoch 65: 8.48955345
NLL Oracle Loss after epoch 66: 8.57771206
NLL Oracle Loss after epoch 67: 8.84576607
NLL Oracle Loss after epoch 68: 8.98365879
NLL Oracle Loss after epoch 69: 8.70098686
NLL Oracle Loss after epoch 70: 8.68916035
NLL Oracle Loss after epoch 71: 9.14675713
NLL Oracle Loss after epoch 72: 8.76438522
NLL Oracle Loss after epoch 73: 8.71981812
NLL Oracle Loss after epoch 74: 8.71866417
NLL Oracle Loss after epoch 75: 8.91183376
NLL Oracle Loss after epoch 76: 8.91623116
NLL Oracle Loss after epoch 77: 8.73250484
NLL Oracle Loss after epoch 78: 8.72533512
NLL Oracle Loss after epoch 79: 8.73431110
NLL Oracle Loss after epoch 80: 8.65736294
NLL Oracle Loss after epoch 81: 8.67414093
NLL Oracle Loss after epoch 82: 8.62403011
NLL Oracle Loss after epoch 83: 8.67744350
NLL Oracle Loss after epoch 84: 8.86131859
NLL Oracle Loss after epoch 85: 8.64347363
NLL Oracle Loss after epoch 86: 8.64120674
NLL Oracle Loss after epoch 87: 8.73522186
NLL Oracle Loss after epoch 88: 8.75246525
NLL Oracle Loss after epoch 89: 8.80537319
NLL Oracle Loss after epoch 90: 8.67113400
NLL Oracle Loss after epoch 91: 8.61443043
NLL Oracle Loss after epoch 92: 8.73805618
NLL Oracle Loss after epoch 93: 8.84867859
NLL Oracle Loss after epoch 94: 8.78678989
NLL Oracle Loss after epoch 95: 8.76620197
NLL Oracle Loss after epoch 96: 9.04549313
NLL Oracle Loss after epoch 97: 9.06293297
NLL Oracle Loss after epoch 98: 8.69444561
NLL Oracle Loss after epoch 99: 8.84654999
NLL Oracle Loss after epoch 100: 9.06886101
NLL Oracle Loss after epoch 101: 8.76229095
NLL Oracle Loss after epoch 102: 8.72312164
NLL Oracle Loss after epoch 103: 8.79211521
NLL Oracle Loss after epoch 104: 8.76819801
NLL Oracle Loss after epoch 105: 8.72028637
NLL Oracle Loss after epoch 106: 8.83242893
NLL Oracle Loss after epoch 107: 8.44363308
NLL Oracle Loss after epoch 108: 8.60283756
NLL Oracle Loss after epoch 109: 8.68232250
NLL Oracle Loss after epoch 110: 8.55122375
NLL Oracle Loss after epoch 111: 8.58090115
NLL Oracle Loss after epoch 112: 8.46301460
NLL Oracle Loss after epoch 113: 8.45882130
NLL Oracle Loss after epoch 114: 8.53970718
NLL Oracle Loss after epoch 115: 8.34093761
NLL Oracle Loss after epoch 116: 8.40963364
NLL Oracle Loss after epoch 117: 8.34996033
NLL Oracle Loss after epoch 118: 8.44121647
NLL Oracle Loss after epoch 119: 8.40174866
NLL Oracle Loss after epoch 120: 8.37429619
NLL Oracle Loss after epoch 121: 8.29476833
NLL Oracle Loss after epoch 122: 8.46960831
NLL Oracle Loss after epoch 123: 8.75661373
NLL Oracle Loss after epoch 124: 8.64601326
NLL Oracle Loss after epoch 125: 8.64797688
NLL Oracle Loss after epoch 126: 8.51372337
NLL Oracle Loss after epoch 127: 8.55372620
NLL Oracle Loss after epoch 128: 8.37978458
NLL Oracle Loss after epoch 129: 8.20319748
NLL Oracle Loss after epoch 130: 8.25695133
NLL Oracle Loss after epoch 131: 8.36188793
NLL Oracle Loss after epoch 132: 8.55876637
NLL Oracle Loss after epoch 133: 8.51857185
NLL Oracle Loss after epoch 134: 8.55701733
NLL Oracle Loss after epoch 135: 8.47157955
NLL Oracle Loss after epoch 136: 8.39810371
NLL Oracle Loss after epoch 137: 8.42404270
NLL Oracle Loss after epoch 138: 8.63096809
NLL Oracle Loss after epoch 139: 8.61339092
NLL Oracle Loss after epoch 140: 8.50224590
NLL Oracle Loss after epoch 141: 8.35904217
NLL Oracle Loss after epoch 142: 8.46715832
NLL Oracle Loss after epoch 143: 8.42057896
NLL Oracle Loss after epoch 144: 8.47507286
NLL Oracle Loss after epoch 145: 8.43102360
NLL Oracle Loss after epoch 146: 8.53334236
NLL Oracle Loss after epoch 147: 8.54669666
NLL Oracle Loss after epoch 148: 8.58783913
NLL Oracle Loss after epoch 149: 8.37573433
NLL Oracle Loss after epoch 150: 8.40848637
NLL Oracle Loss after epoch 151: 8.47945595
NLL Oracle Loss after epoch 152: 8.41966057
NLL Oracle Loss after epoch 153: 8.53682232
NLL Oracle Loss after epoch 154: 8.48231888
NLL Oracle Loss after epoch 155: 8.41575718
NLL Oracle Loss after epoch 156: 8.46417427
NLL Oracle Loss after epoch 157: 8.50725079
NLL Oracle Loss after epoch 158: 8.39838409
NLL Oracle Loss after epoch 159: 8.25977516
NLL Oracle Loss after epoch 160: 8.41379452
NLL Oracle Loss after epoch 161: 8.36251450
NLL Oracle Loss after epoch 162: 8.37118626
NLL Oracle Loss after epoch 163: 8.27740479
NLL Oracle Loss after epoch 164: 8.34540081
NLL Oracle Loss after epoch 165: 8.46288872
NLL Oracle Loss after epoch 166: 8.48446941
NLL Oracle Loss after epoch 167: 8.49087715
NLL Oracle Loss after epoch 168: 8.47626495
NLL Oracle Loss after epoch 169: 8.39854527
NLL Oracle Loss after epoch 170: 8.30539131
NLL Oracle Loss after epoch 171: 8.30362129
NLL Oracle Loss after epoch 172: 8.42115211
NLL Oracle Loss after epoch 173: 8.61016750
NLL Oracle Loss after epoch 174: 8.55727386
NLL Oracle Loss after epoch 175: 8.57550240
NLL Oracle Loss after epoch 176: 8.51846695
NLL Oracle Loss after epoch 177: 8.51348591
NLL Oracle Loss after epoch 178: 8.34639835
NLL Oracle Loss after epoch 179: 8.24885273
NLL Oracle Loss after epoch 180: 8.39890575
NLL Oracle Loss after epoch 181: 8.36891937
NLL Oracle Loss after epoch 182: 8.49668026
NLL Oracle Loss after epoch 183: 8.57344532
NLL Oracle Loss after epoch 184: 8.48773384
NLL Oracle Loss after epoch 185: 8.60667038
NLL Oracle Loss after epoch 186: 8.66390133
NLL Oracle Loss after epoch 187: 8.51562309
NLL Oracle Loss after epoch 188: 8.43643570
NLL Oracle Loss after epoch 189: 8.40371609
NLL Oracle Loss after epoch 190: 8.46934128
NLL Oracle Loss after epoch 191: 8.69733429
NLL Oracle Loss after epoch 192: 8.41411209
NLL Oracle Loss after epoch 193: 8.31083679
NLL Oracle Loss after epoch 194: 8.34752655
NLL Oracle Loss after epoch 195: 8.42525578
NLL Oracle Loss after epoch 196: 8.48166752
NLL Oracle Loss after epoch 197: 8.49579144
NLL Oracle Loss after epoch 198: 8.45492649
NLL Oracle Loss after epoch 199: 8.37042999
