nohup: ignoring input
DATASET: oracle
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 100
LEARNING_RATE_PRE_G: 5e-05
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 0
NUM_D: 5
NUM_G: 1
BETA1_G: 0.5
BETA1_D: 0.5
BETA2_G: 0.9
BETA2_D: 0.9
LOG_LOCATION: ./logs/Wass_or_t0.2_start_b1-0.5_b2-0.9_g1d5_g32_d32_pe100_pl5e-5_l1e-4/
SAMPLES_FOLDER: samples/Wass_or_t0.2_start_b1-0.5_b2-0.9_g1d5_g32_d32_pe100_pl5e-5_l1e-4/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: ./checkpoints/or_pi_h32_le-3_e15/or_h32_le-3.chk
LOAD_FILE_PRETRAIN: ./checkpoints/or_pi_h32_le-3_e15/or_h32_le-3.chk
GAN_CHK_FOLDER: ./checkpoints/Wass_or_t0.2_start_b1-0.5_b2-0.9_g1d5_g32_d32_pe100_pl5e-5_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wass_or_t0.2_start_b1-0.5_b2-0.9_g1d5_g32_d32_pe100_pl5e-5_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wass_or_t0.2_start_b1-0.5_b2-0.9_g1d5_g32_d32_pe100_pl5e-5_l1e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5000)
(32, 20, 5000)
2017-04-15 10:54:21.636627: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 10:54:21.636673: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 10:54:21.636679: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 10:54:21.636683: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 10:54:21.636688: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 10:54:24.513649: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:86:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-15 10:54:24.513693: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-15 10:54:24.513700: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-15 10:54:24.513713: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:86:00.0)
WARNING:tensorflow:From lstm_sched_was.py:297: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
NLL Oracle Loss after loading model: 8.60867023
2017-04-15 10:55:23.705280: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 49644 get requests, put_count=49542 evicted_count=1000 eviction_rate=0.0201849 and unsatisfied allocation rate=0.0242124
2017-04-15 10:55:23.705357: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-15 10:55:29.306395: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 3816 get requests, put_count=3871 evicted_count=1000 eviction_rate=0.258331 and unsatisfied allocation rate=0.253669
2017-04-15 10:55:29.306465: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Train Loss after pre-train epoch 0: 7.58889811
NLL Oracle Loss after pre-train epoch 0: 8.54418278
Train Loss after pre-train epoch 1: 7.58482148
NLL Oracle Loss after pre-train epoch 1: 8.56336021
Train Loss after pre-train epoch 2: 7.58289249
NLL Oracle Loss after pre-train epoch 2: 8.57063866
Train Loss after pre-train epoch 3: 7.58119848
NLL Oracle Loss after pre-train epoch 3: 8.57320499
Train Loss after pre-train epoch 4: 7.57958767
NLL Oracle Loss after pre-train epoch 4: 8.54834461
Train Loss after pre-train epoch 5: 7.57809861
NLL Oracle Loss after pre-train epoch 5: 8.57956505
Train Loss after pre-train epoch 6: 7.57668210
NLL Oracle Loss after pre-train epoch 6: 8.57719517
Train Loss after pre-train epoch 7: 7.57505507
NLL Oracle Loss after pre-train epoch 7: 8.60127449
Train Loss after pre-train epoch 8: 7.57369548
NLL Oracle Loss after pre-train epoch 8: 8.57610035
Train Loss after pre-train epoch 9: 7.57243666
NLL Oracle Loss after pre-train epoch 9: 8.59259033
Train Loss after pre-train epoch 10: 7.57088031
NLL Oracle Loss after pre-train epoch 10: 8.59035110
Train Loss after pre-train epoch 11: 7.56956440
NLL Oracle Loss after pre-train epoch 11: 8.59065723
Train Loss after pre-train epoch 12: 7.56810927
NLL Oracle Loss after pre-train epoch 12: 8.59716320
Train Loss after pre-train epoch 13: 7.56686453
NLL Oracle Loss after pre-train epoch 13: 8.59703636
Train Loss after pre-train epoch 14: 7.56536959
NLL Oracle Loss after pre-train epoch 14: 8.57997608
Train Loss after pre-train epoch 15: 7.56392698
NLL Oracle Loss after pre-train epoch 15: 8.58233452
Train Loss after pre-train epoch 16: 7.56263893
NLL Oracle Loss after pre-train epoch 16: 8.59696960
Train Loss after pre-train epoch 17: 7.56149318
NLL Oracle Loss after pre-train epoch 17: 8.57969570
Train Loss after pre-train epoch 18: 7.55992896
NLL Oracle Loss after pre-train epoch 18: 8.59346485
Train Loss after pre-train epoch 19: 7.55858760
NLL Oracle Loss after pre-train epoch 19: 8.58412170
Train Loss after pre-train epoch 20: 7.55730948
NLL Oracle Loss after pre-train epoch 20: 8.56886292
Train Loss after pre-train epoch 21: 7.55599251
NLL Oracle Loss after pre-train epoch 21: 8.59714794
Train Loss after pre-train epoch 22: 7.55448367
NLL Oracle Loss after pre-train epoch 22: 8.56797218
Train Loss after pre-train epoch 23: 7.55331593
NLL Oracle Loss after pre-train epoch 23: 8.58755493
Train Loss after pre-train epoch 24: 7.55187253
NLL Oracle Loss after pre-train epoch 24: 8.57048321
Train Loss after pre-train epoch 25: 7.55043188
NLL Oracle Loss after pre-train epoch 25: 8.57974434
Train Loss after pre-train epoch 26: 7.54914008
NLL Oracle Loss after pre-train epoch 26: 8.56472683
Train Loss after pre-train epoch 27: 7.54767592
NLL Oracle Loss after pre-train epoch 27: 8.58363819
Train Loss after pre-train epoch 28: 7.54619146
NLL Oracle Loss after pre-train epoch 28: 8.58518505
Train Loss after pre-train epoch 29: 7.54494554
NLL Oracle Loss after pre-train epoch 29: 8.57647896
Train Loss after pre-train epoch 30: 7.54349342
NLL Oracle Loss after pre-train epoch 30: 8.57751942
Train Loss after pre-train epoch 31: 7.54221731
NLL Oracle Loss after pre-train epoch 31: 8.55792618
Train Loss after pre-train epoch 32: 7.54083299
NLL Oracle Loss after pre-train epoch 32: 8.58382702
Train Loss after pre-train epoch 33: 7.53954372
NLL Oracle Loss after pre-train epoch 33: 8.55978012
Train Loss after pre-train epoch 34: 7.53811690
NLL Oracle Loss after pre-train epoch 34: 8.56759548
Train Loss after pre-train epoch 35: 7.53663589
NLL Oracle Loss after pre-train epoch 35: 8.57152653
Train Loss after pre-train epoch 36: 7.53537163
NLL Oracle Loss after pre-train epoch 36: 8.55085754
Train Loss after pre-train epoch 37: 7.53387459
NLL Oracle Loss after pre-train epoch 37: 8.55223942
Train Loss after pre-train epoch 38: 7.53253330
NLL Oracle Loss after pre-train epoch 38: 8.54399967
Train Loss after pre-train epoch 39: 7.53111229
NLL Oracle Loss after pre-train epoch 39: 8.55769539
Train Loss after pre-train epoch 40: 7.52977456
NLL Oracle Loss after pre-train epoch 40: 8.54381275
Train Loss after pre-train epoch 41: 7.52836357
NLL Oracle Loss after pre-train epoch 41: 8.56035042
Train Loss after pre-train epoch 42: 7.52705849
NLL Oracle Loss after pre-train epoch 42: 8.54128170
Train Loss after pre-train epoch 43: 7.52577557
NLL Oracle Loss after pre-train epoch 43: 8.54987335
Train Loss after pre-train epoch 44: 7.52411014
NLL Oracle Loss after pre-train epoch 44: 8.55564880
Train Loss after pre-train epoch 45: 7.52268867
NLL Oracle Loss after pre-train epoch 45: 8.56425762
Train Loss after pre-train epoch 46: 7.52111866
NLL Oracle Loss after pre-train epoch 46: 8.54944801
Train Loss after pre-train epoch 47: 7.52006018
NLL Oracle Loss after pre-train epoch 47: 8.54953194
Train Loss after pre-train epoch 48: 7.51852272
NLL Oracle Loss after pre-train epoch 48: 8.55115032
Train Loss after pre-train epoch 49: 7.51693275
NLL Oracle Loss after pre-train epoch 49: 8.54168415
Train Loss after pre-train epoch 50: 7.51568711
NLL Oracle Loss after pre-train epoch 50: 8.55130863
Train Loss after pre-train epoch 51: 7.51444452
NLL Oracle Loss after pre-train epoch 51: 8.56071568
Train Loss after pre-train epoch 52: 7.51283025
NLL Oracle Loss after pre-train epoch 52: 8.55747986
Train Loss after pre-train epoch 53: 7.51137775
NLL Oracle Loss after pre-train epoch 53: 8.57087421
Train Loss after pre-train epoch 54: 7.50992625
NLL Oracle Loss after pre-train epoch 54: 8.54368687
Train Loss after pre-train epoch 55: 7.50861447
NLL Oracle Loss after pre-train epoch 55: 8.54088116
Train Loss after pre-train epoch 56: 7.50721373
NLL Oracle Loss after pre-train epoch 56: 8.55113220
Train Loss after pre-train epoch 57: 7.50573872
NLL Oracle Loss after pre-train epoch 57: 8.53944874
Train Loss after pre-train epoch 58: 7.50446020
NLL Oracle Loss after pre-train epoch 58: 8.54170132
Train Loss after pre-train epoch 59: 7.50276132
NLL Oracle Loss after pre-train epoch 59: 8.55835915
Train Loss after pre-train epoch 60: 7.50131770
NLL Oracle Loss after pre-train epoch 60: 8.52607059
Train Loss after pre-train epoch 61: 7.50003449
NLL Oracle Loss after pre-train epoch 61: 8.54269028
Train Loss after pre-train epoch 62: 7.49847280
NLL Oracle Loss after pre-train epoch 62: 8.55557919
Train Loss after pre-train epoch 63: 7.49715177
NLL Oracle Loss after pre-train epoch 63: 8.56183243
Train Loss after pre-train epoch 64: 7.49548096
NLL Oracle Loss after pre-train epoch 64: 8.53758049
Train Loss after pre-train epoch 65: 7.49414492
NLL Oracle Loss after pre-train epoch 65: 8.54742908
Train Loss after pre-train epoch 66: 7.49230939
NLL Oracle Loss after pre-train epoch 66: 8.55153084
Train Loss after pre-train epoch 67: 7.49137646
NLL Oracle Loss after pre-train epoch 67: 8.53061581
Train Loss after pre-train epoch 68: 7.48962494
NLL Oracle Loss after pre-train epoch 68: 8.56093121
Train Loss after pre-train epoch 69: 7.48810428
NLL Oracle Loss after pre-train epoch 69: 8.54647636
Train Loss after pre-train epoch 70: 7.48692990
NLL Oracle Loss after pre-train epoch 70: 8.55960941
Train Loss after pre-train epoch 71: 7.48531163
NLL Oracle Loss after pre-train epoch 71: 8.56876850
Train Loss after pre-train epoch 72: 7.48374040
NLL Oracle Loss after pre-train epoch 72: 8.54222202
Train Loss after pre-train epoch 73: 7.48242512
NLL Oracle Loss after pre-train epoch 73: 8.55237293
Train Loss after pre-train epoch 74: 7.48105358
NLL Oracle Loss after pre-train epoch 74: 8.54674053
Train Loss after pre-train epoch 75: 7.47935737
NLL Oracle Loss after pre-train epoch 75: 8.56614017
Train Loss after pre-train epoch 76: 7.47793525
NLL Oracle Loss after pre-train epoch 76: 8.54072666
Train Loss after pre-train epoch 77: 7.47645408
NLL Oracle Loss after pre-train epoch 77: 8.55941963
Train Loss after pre-train epoch 78: 7.47514903
NLL Oracle Loss after pre-train epoch 78: 8.57133102
Train Loss after pre-train epoch 79: 7.47336100
NLL Oracle Loss after pre-train epoch 79: 8.55123806
Train Loss after pre-train epoch 80: 7.47208650
NLL Oracle Loss after pre-train epoch 80: 8.56001949
Train Loss after pre-train epoch 81: 7.47038563
NLL Oracle Loss after pre-train epoch 81: 8.55409908
Train Loss after pre-train epoch 82: 7.46907686
NLL Oracle Loss after pre-train epoch 82: 8.55171394
Train Loss after pre-train epoch 83: 7.46739659
NLL Oracle Loss after pre-train epoch 83: 8.55443287
Train Loss after pre-train epoch 84: 7.46619872
NLL Oracle Loss after pre-train epoch 84: 8.55933285
Train Loss after pre-train epoch 85: 7.46454863
NLL Oracle Loss after pre-train epoch 85: 8.55205631
Train Loss after pre-train epoch 86: 7.46321898
NLL Oracle Loss after pre-train epoch 86: 8.53794003
Train Loss after pre-train epoch 87: 7.46152749
NLL Oracle Loss after pre-train epoch 87: 8.53991222
Train Loss after pre-train epoch 88: 7.46008280
NLL Oracle Loss after pre-train epoch 88: 8.56359196
Train Loss after pre-train epoch 89: 7.45850307
NLL Oracle Loss after pre-train epoch 89: 8.57781982
Train Loss after pre-train epoch 90: 7.45712950
NLL Oracle Loss after pre-train epoch 90: 8.55043125
Train Loss after pre-train epoch 91: 7.45561526
NLL Oracle Loss after pre-train epoch 91: 8.54979324
Train Loss after pre-train epoch 92: 7.45387770
NLL Oracle Loss after pre-train epoch 92: 8.54649830
Train Loss after pre-train epoch 93: 7.45235411
NLL Oracle Loss after pre-train epoch 93: 8.55965900
Train Loss after pre-train epoch 94: 7.45111788
NLL Oracle Loss after pre-train epoch 94: 8.54394913
Train Loss after pre-train epoch 95: 7.44943970
NLL Oracle Loss after pre-train epoch 95: 8.56040955
Train Loss after pre-train epoch 96: 7.44772468
NLL Oracle Loss after pre-train epoch 96: 8.56995583
Train Loss after pre-train epoch 97: 7.44637378
NLL Oracle Loss after pre-train epoch 97: 8.54445076
Train Loss after pre-train epoch 98: 7.44497561
NLL Oracle Loss after pre-train epoch 98: 8.59186077
Train Loss after pre-train epoch 99: 7.44334031
NLL Oracle Loss after pre-train epoch 99: 8.57370377
