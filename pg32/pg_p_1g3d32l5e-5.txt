DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 5e-05
LEARNING_RATE_D: 5e-05
N_EPOCHS: 150
NUM_D: 3
NUM_G: 1
LOG_LOCATION: ./logs/pg_g1d3_g32_d32_pe6_pl1e-2_l5e-5/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6/
SAVE_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6/pg_p_h32_l1e-2.chk
LOAD_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6/pg_p_h32_l1e-2.chkb
GAN_CHK_FOLDER: ./checkpoints/pg_g1d3_g32_d32_pe6_pl1e-2_l5e-5/
SAVE_FILE_GAN: ./checkpoints/pg_g1d3_g32_d32_pe6_pl1e-2_l5e-5/chk
LOAD_FILE_GAN: ./checkpoints/pg_g1d3_g32_d32_pe6_pl1e-2_l5e-5/chk
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
2017-04-07 08:20:02.758047: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:20:02.758086: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:20:02.758092: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:20:02.758097: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:20:02.758101: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:20:10.725292: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:06:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-07 08:20:10.725336: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-07 08:20:10.725343: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-07 08:20:10.725352: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:06:00.0)
WARNING:tensorflow:From model.py:246: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Valid g_pre_loss before training: 8.58830059
Test g_pre_loss before training: 8.58821521
2017-04-07 08:20:29.512811: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 10728 get requests, put_count=10648 evicted_count=1000 eviction_rate=0.0939144 and unsatisfied allocation rate=0.109993
2017-04-07 08:20:29.512887: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
