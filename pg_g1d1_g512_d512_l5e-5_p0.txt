nohup: ignoring input
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
2017-04-02 03:31:12.819003: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-02 03:31:12.819043: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-02 03:31:12.819050: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-02 03:31:12.819055: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-02 03:31:12.819059: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-02 03:31:15.881629: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:06:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-02 03:31:15.881667: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-02 03:31:15.881685: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-02 03:31:15.881693: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:06:00.0)
WARNING:tensorflow:From model.py:153: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Test g_pre_loss before training: 8.58944090
2017-04-02 03:31:41.293701: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 15756 get requests, put_count=15489 evicted_count=1000 eviction_rate=0.0645619 and unsatisfied allocation rate=0.0867606
2017-04-02 03:31:41.293754: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-02 03:31:58.164579: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 8393 get requests, put_count=8371 evicted_count=1000 eviction_rate=0.11946 and unsatisfied allocation rate=0.124509
2017-04-02 03:31:58.164637: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Test Loss after epoch 0: 8.58941460
Test Loss after epoch 1: 8.58968422
Test Loss after epoch 2: 8.58959481
2017-04-02 03:42:56.310381: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 1764792 get requests, put_count=1764797 evicted_count=3000 eviction_rate=0.00169991 and unsatisfied allocation rate=0.00172485
Test Loss after epoch 3: 8.58947386
Test Loss after epoch 4: 8.58956491
Test Loss after epoch 5: 8.58969898
Test Loss after epoch 6: 8.58970259
Test Loss after epoch 7: 8.60512990
Test Loss after epoch 8: 12.82938473
Test Loss after epoch 9: 14.96518829
Test Loss after epoch 10: 15.60346467
Test Loss after epoch 11: 16.00928933
Test Loss after epoch 12: 15.30477811
2017-04-02 04:12:16.954741: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 7279195 get requests, put_count=7279200 evicted_count=13000 eviction_rate=0.00178591 and unsatisfied allocation rate=0.00179196
Test Loss after epoch 13: 14.60685627
Test Loss after epoch 14: 16.07794418
Test Loss after epoch 15: 16.58317433
Test Loss after epoch 16: 17.15321235
Test Loss after epoch 17: 16.35611716
Test Loss after epoch 18: 14.32019593
Test Loss after epoch 19: 15.65967748
Test Loss after epoch 20: 15.92269453
Test Loss after epoch 21: 15.65569975
Test Loss after epoch 22: 17.79315025
2017-04-02 04:41:45.080757: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 12818828 get requests, put_count=12818825 evicted_count=23000 eviction_rate=0.00179424 and unsatisfied allocation rate=0.00179829
Test Loss after epoch 23: 18.91373864
Test Loss after epoch 24: 19.77074467
Test Loss after epoch 25: 20.33538929
Test Loss after epoch 26: 20.30114999
Test Loss after epoch 27: 19.74797348
Test Loss after epoch 28: 17.97877254
Test Loss after epoch 29: 18.46273542
Test Loss after epoch 30: 19.79296234
Test Loss after epoch 31: 20.52263071
2017-04-02 05:09:59.683879: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 18130014 get requests, put_count=18130010 evicted_count=33000 eviction_rate=0.00182019 and unsatisfied allocation rate=0.00182311
Test Loss after epoch 32: 21.09967655
Test Loss after epoch 33: 21.56901309
Test Loss after epoch 34: 21.66340812
Test Loss after epoch 35: 21.15395921
Test Loss after epoch 36: 20.18576774
Test Loss after epoch 37: 21.28583630
Test Loss after epoch 38: 21.85834331
Test Loss after epoch 39: 24.59611006
Test Loss after epoch 40: 25.64234462
2017-04-02 05:37:34.715140: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 23318411 get requests, put_count=23318415 evicted_count=43000 eviction_rate=0.00184404 and unsatisfied allocation rate=0.00184597
Test Loss after epoch 41: 26.24081623
Test Loss after epoch 42: 26.46515724
Test Loss after epoch 43: 26.05863877
Test Loss after epoch 44: 23.80754271
Test Loss after epoch 45: 24.03564030
Test Loss after epoch 46: 26.59754204
Test Loss after epoch 47: 27.50740966
Test Loss after epoch 48: 28.06839941
Test Loss after epoch 49: 28.37437832
2017-04-02 06:04:51.035473: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 28446289 get requests, put_count=28446294 evicted_count=53000 eviction_rate=0.00186316 and unsatisfied allocation rate=0.00186471
Test Loss after epoch 50: 28.32545621
Test Loss after epoch 51: 27.98603915
Test Loss after epoch 52: 27.22239251
Test Loss after epoch 53: 25.65427364
Test Loss after epoch 54: 26.62150091
Test Loss after epoch 55: 26.95368925
Test Loss after epoch 56: 26.90413323
Test Loss after epoch 57: 25.91744179
Test Loss after epoch 58: 27.64337809
2017-04-02 06:31:25.208477: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 33441364 get requests, put_count=33441361 evicted_count=63000 eviction_rate=0.00188389 and unsatisfied allocation rate=0.00188545
Test Loss after epoch 59: 27.87209587
Test Loss after epoch 60: 27.26020829
Test Loss after epoch 61: 29.41257541
Test Loss after epoch 62: 29.80917186
Test Loss after epoch 63: 29.71538475
Test Loss after epoch 64: 30.84600074
Test Loss after epoch 65: 31.62947200
Test Loss after epoch 66: 32.03862229
Test Loss after epoch 67: 32.34293464
2017-04-02 06:58:16.310608: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 38493553 get requests, put_count=38493553 evicted_count=73000 eviction_rate=0.00189642 and unsatisfied allocation rate=0.00189769
Test Loss after epoch 68: 32.55784589
Test Loss after epoch 69: 32.47933376
Test Loss after epoch 70: 31.77868639
Test Loss after epoch 71: 30.28280842
Test Loss after epoch 72: 31.79542036
