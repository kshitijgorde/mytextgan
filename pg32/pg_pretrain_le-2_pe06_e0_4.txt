nohup: ignoring input
DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 0
NUM_D: 5
NUM_G: 1
BETA1_G: 0.5
BETA1_D: 0.5
BETA2_G: 0.9
BETA2_D: 0.9
LOG_LOCATION: ./logs/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAMPLES_FOLDER: samples/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6_was4/
SAVE_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6_was4/pg_p_h32_l1e-2.chk
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5369)
(32, 20, 5369)
2017-04-15 00:11:59.044896: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 00:11:59.044937: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 00:11:59.044945: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 00:11:59.044962: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 00:11:59.044968: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 00:12:04.535230: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:85:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-15 00:12:04.535307: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-15 00:12:04.535320: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-15 00:12:04.535335: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:85:00.0)
WARNING:tensorflow:From model.py:289: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Test g_pre_loss before training: 8.58708120
Calculated in 3.655s
2017-04-15 00:12:42.987542: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6248 get requests, put_count=6160 evicted_count=1000 eviction_rate=0.162338 and unsatisfied allocation rate=0.190141
2017-04-15 00:12:42.987605: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-15 00:12:48.602939: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 3816 get requests, put_count=3880 evicted_count=1000 eviction_rate=0.257732 and unsatisfied allocation rate=0.25131
2017-04-15 00:12:48.603035: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Train Loss after pre-train epoch 0: 7.22257263
Test Loss after pre-train epoch 0: 6.89925232
Train Loss after pre-train epoch 1: 6.93314206
Test Loss after pre-train epoch 1: 6.61944085
Train Loss after pre-train epoch 2: 6.64449131
Test Loss after pre-train epoch 2: 6.45496404
Train Loss after pre-train epoch 3: 6.36157968
Test Loss after pre-train epoch 3: 6.30731473
Train Loss after pre-train epoch 4: 6.11009660
Test Loss after pre-train epoch 4: 6.27528109
Train Loss after pre-train epoch 5: 5.91891477
Test Loss after pre-train epoch 5: 6.28645109
