nohup: ignoring input
DATASET: oracle
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 100
LEARNING_RATE_PRE_G: 1e-05
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 0
NUM_D: 5
NUM_G: 1
BETA1_G: 0.5
BETA1_D: 0.5
BETA2_G: 0.9
BETA2_D: 0.9
LOG_LOCATION: ./logs/Wass_or_t0.2_start_b1-0.5_b2-0.9_g1d5_g32_d32_pe100_pl1e-5_l1e-4/
SAMPLES_FOLDER: samples/Wass_or_t0.2_start_b1-0.5_b2-0.9_g1d5_g32_d32_pe100_pl1e-5_l1e-4/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: ./checkpoints/or_pi_h32_le-3_e15/or_h32_le-3.chk
LOAD_FILE_PRETRAIN: ./checkpoints/or_pi_h32_le-3_e15/or_h32_le-3.chk
GAN_CHK_FOLDER: ./checkpoints/Wass_or_t0.2_start_b1-0.5_b2-0.9_g1d5_g32_d32_pe100_pl1e-5_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wass_or_t0.2_start_b1-0.5_b2-0.9_g1d5_g32_d32_pe100_pl1e-5_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wass_or_t0.2_start_b1-0.5_b2-0.9_g1d5_g32_d32_pe100_pl1e-5_l1e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5000)
(32, 20, 5000)
2017-04-15 10:54:06.316453: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 10:54:06.316504: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 10:54:06.316521: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 10:54:06.316527: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 10:54:06.316542: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-15 10:54:11.990140: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:85:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-15 10:54:11.990209: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-15 10:54:11.990227: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-15 10:54:11.990246: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:85:00.0)
WARNING:tensorflow:From lstm_sched_was.py:297: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
NLL Oracle Loss after loading model: 8.59380722
2017-04-15 10:55:12.981434: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 49644 get requests, put_count=49559 evicted_count=1000 eviction_rate=0.020178 and unsatisfied allocation rate=0.02387
2017-04-15 10:55:12.981499: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-15 10:55:18.621894: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 4293 get requests, put_count=4280 evicted_count=1000 eviction_rate=0.233645 and unsatisfied allocation rate=0.241323
2017-04-15 10:55:18.622007: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Train Loss after pre-train epoch 0: 7.59322588
NLL Oracle Loss after pre-train epoch 0: 8.50258732
Train Loss after pre-train epoch 1: 7.58878690
NLL Oracle Loss after pre-train epoch 1: 8.47512722
Train Loss after pre-train epoch 2: 7.58706826
NLL Oracle Loss after pre-train epoch 2: 8.51021004
Train Loss after pre-train epoch 3: 7.58613641
NLL Oracle Loss after pre-train epoch 3: 8.53268242
Train Loss after pre-train epoch 4: 7.58526715
NLL Oracle Loss after pre-train epoch 4: 8.52529335
Train Loss after pre-train epoch 5: 7.58460299
NLL Oracle Loss after pre-train epoch 5: 8.51693535
Train Loss after pre-train epoch 6: 7.58414814
NLL Oracle Loss after pre-train epoch 6: 8.55148983
Train Loss after pre-train epoch 7: 7.58376539
NLL Oracle Loss after pre-train epoch 7: 8.55154800
Train Loss after pre-train epoch 8: 7.58332612
NLL Oracle Loss after pre-train epoch 8: 8.52893639
Train Loss after pre-train epoch 9: 7.58282454
NLL Oracle Loss after pre-train epoch 9: 8.52060699
Train Loss after pre-train epoch 10: 7.58236223
NLL Oracle Loss after pre-train epoch 10: 8.54947376
Train Loss after pre-train epoch 11: 7.58201218
NLL Oracle Loss after pre-train epoch 11: 8.52788544
Train Loss after pre-train epoch 12: 7.58173939
NLL Oracle Loss after pre-train epoch 12: 8.52452278
Train Loss after pre-train epoch 13: 7.58134725
NLL Oracle Loss after pre-train epoch 13: 8.54445553
Train Loss after pre-train epoch 14: 7.58104503
NLL Oracle Loss after pre-train epoch 14: 8.53333473
Train Loss after pre-train epoch 15: 7.58073428
NLL Oracle Loss after pre-train epoch 15: 8.53845215
Train Loss after pre-train epoch 16: 7.58029692
NLL Oracle Loss after pre-train epoch 16: 8.52206039
Train Loss after pre-train epoch 17: 7.57991668
NLL Oracle Loss after pre-train epoch 17: 8.53013706
Train Loss after pre-train epoch 18: 7.57969465
NLL Oracle Loss after pre-train epoch 18: 8.55122375
Train Loss after pre-train epoch 19: 7.57927554
NLL Oracle Loss after pre-train epoch 19: 8.55652809
Train Loss after pre-train epoch 20: 7.57913158
NLL Oracle Loss after pre-train epoch 20: 8.52733898
Train Loss after pre-train epoch 21: 7.57869230
NLL Oracle Loss after pre-train epoch 21: 8.56279755
Train Loss after pre-train epoch 22: 7.57853853
NLL Oracle Loss after pre-train epoch 22: 8.55233669
Train Loss after pre-train epoch 23: 7.57811182
NLL Oracle Loss after pre-train epoch 23: 8.54594231
Train Loss after pre-train epoch 24: 7.57783741
NLL Oracle Loss after pre-train epoch 24: 8.56111336
Train Loss after pre-train epoch 25: 7.57735398
NLL Oracle Loss after pre-train epoch 25: 8.55056381
Train Loss after pre-train epoch 26: 7.57726601
NLL Oracle Loss after pre-train epoch 26: 8.55997086
Train Loss after pre-train epoch 27: 7.57693686
NLL Oracle Loss after pre-train epoch 27: 8.54204750
Train Loss after pre-train epoch 28: 7.57683128
NLL Oracle Loss after pre-train epoch 28: 8.57567406
Train Loss after pre-train epoch 29: 7.57619976
NLL Oracle Loss after pre-train epoch 29: 8.55858326
Train Loss after pre-train epoch 30: 7.57600429
NLL Oracle Loss after pre-train epoch 30: 8.55706596
Train Loss after pre-train epoch 31: 7.57570457
NLL Oracle Loss after pre-train epoch 31: 8.56322956
Train Loss after pre-train epoch 32: 7.57552427
NLL Oracle Loss after pre-train epoch 32: 8.56942177
Train Loss after pre-train epoch 33: 7.57516240
NLL Oracle Loss after pre-train epoch 33: 8.56519508
Train Loss after pre-train epoch 34: 7.57489193
NLL Oracle Loss after pre-train epoch 34: 8.54254627
Train Loss after pre-train epoch 35: 7.57466734
NLL Oracle Loss after pre-train epoch 35: 8.54828262
Train Loss after pre-train epoch 36: 7.57420583
NLL Oracle Loss after pre-train epoch 36: 8.55066681
Train Loss after pre-train epoch 37: 7.57407929
NLL Oracle Loss after pre-train epoch 37: 8.57807159
Train Loss after pre-train epoch 38: 7.57388670
NLL Oracle Loss after pre-train epoch 38: 8.54303265
Train Loss after pre-train epoch 39: 7.57348255
NLL Oracle Loss after pre-train epoch 39: 8.56568146
Train Loss after pre-train epoch 40: 7.57333412
NLL Oracle Loss after pre-train epoch 40: 8.56128883
Train Loss after pre-train epoch 41: 7.57302967
NLL Oracle Loss after pre-train epoch 41: 8.57182789
Train Loss after pre-train epoch 42: 7.57255311
NLL Oracle Loss after pre-train epoch 42: 8.55541611
Train Loss after pre-train epoch 43: 7.57235316
NLL Oracle Loss after pre-train epoch 43: 8.56199169
Train Loss after pre-train epoch 44: 7.57192313
NLL Oracle Loss after pre-train epoch 44: 8.58404732
Train Loss after pre-train epoch 45: 7.57183507
NLL Oracle Loss after pre-train epoch 45: 8.56949806
Train Loss after pre-train epoch 46: 7.57157733
NLL Oracle Loss after pre-train epoch 46: 8.57443047
Train Loss after pre-train epoch 47: 7.57132423
NLL Oracle Loss after pre-train epoch 47: 8.55450630
Train Loss after pre-train epoch 48: 7.57089044
NLL Oracle Loss after pre-train epoch 48: 8.56640530
Train Loss after pre-train epoch 49: 7.57063213
NLL Oracle Loss after pre-train epoch 49: 8.55160427
Train Loss after pre-train epoch 50: 7.57034710
NLL Oracle Loss after pre-train epoch 50: 8.57114697
Train Loss after pre-train epoch 51: 7.56997240
NLL Oracle Loss after pre-train epoch 51: 8.57072353
Train Loss after pre-train epoch 52: 7.56979090
NLL Oracle Loss after pre-train epoch 52: 8.57765102
Train Loss after pre-train epoch 53: 7.56958074
NLL Oracle Loss after pre-train epoch 53: 8.52955532
Train Loss after pre-train epoch 54: 7.56929858
NLL Oracle Loss after pre-train epoch 54: 8.55866909
Train Loss after pre-train epoch 55: 7.56897680
NLL Oracle Loss after pre-train epoch 55: 8.55989456
Train Loss after pre-train epoch 56: 7.56880362
NLL Oracle Loss after pre-train epoch 56: 8.55662537
Train Loss after pre-train epoch 57: 7.56836783
NLL Oracle Loss after pre-train epoch 57: 8.55525208
Train Loss after pre-train epoch 58: 7.56813395
NLL Oracle Loss after pre-train epoch 58: 8.55215549
Train Loss after pre-train epoch 59: 7.56795587
NLL Oracle Loss after pre-train epoch 59: 8.56944752
Train Loss after pre-train epoch 60: 7.56772719
NLL Oracle Loss after pre-train epoch 60: 8.57269669
Train Loss after pre-train epoch 61: 7.56740543
NLL Oracle Loss after pre-train epoch 61: 8.56688786
Train Loss after pre-train epoch 62: 7.56701558
NLL Oracle Loss after pre-train epoch 62: 8.55899620
Train Loss after pre-train epoch 63: 7.56668444
NLL Oracle Loss after pre-train epoch 63: 8.57730675
Train Loss after pre-train epoch 64: 7.56641132
NLL Oracle Loss after pre-train epoch 64: 8.55330086
Train Loss after pre-train epoch 65: 7.56620543
NLL Oracle Loss after pre-train epoch 65: 8.55628490
Train Loss after pre-train epoch 66: 7.56586441
NLL Oracle Loss after pre-train epoch 66: 8.54523945
Train Loss after pre-train epoch 67: 7.56568320
NLL Oracle Loss after pre-train epoch 67: 8.55744171
Train Loss after pre-train epoch 68: 7.56529141
NLL Oracle Loss after pre-train epoch 68: 8.56783772
Train Loss after pre-train epoch 69: 7.56502738
NLL Oracle Loss after pre-train epoch 69: 8.56291008
Train Loss after pre-train epoch 70: 7.56486834
NLL Oracle Loss after pre-train epoch 70: 8.56933022
Train Loss after pre-train epoch 71: 7.56439116
NLL Oracle Loss after pre-train epoch 71: 8.58182144
Train Loss after pre-train epoch 72: 7.56441972
NLL Oracle Loss after pre-train epoch 72: 8.55006886
Train Loss after pre-train epoch 73: 7.56398210
NLL Oracle Loss after pre-train epoch 73: 8.55131531
Train Loss after pre-train epoch 74: 7.56364421
NLL Oracle Loss after pre-train epoch 74: 8.57081509
Train Loss after pre-train epoch 75: 7.56342962
NLL Oracle Loss after pre-train epoch 75: 8.57700825
Train Loss after pre-train epoch 76: 7.56317065
NLL Oracle Loss after pre-train epoch 76: 8.58273125
Train Loss after pre-train epoch 77: 7.56314883
NLL Oracle Loss after pre-train epoch 77: 8.56260204
Train Loss after pre-train epoch 78: 7.56269712
NLL Oracle Loss after pre-train epoch 78: 8.56469059
Train Loss after pre-train epoch 79: 7.56256219
NLL Oracle Loss after pre-train epoch 79: 8.56396961
Train Loss after pre-train epoch 80: 7.56202142
NLL Oracle Loss after pre-train epoch 80: 8.57108879
Train Loss after pre-train epoch 81: 7.56194440
NLL Oracle Loss after pre-train epoch 81: 8.56732368
Train Loss after pre-train epoch 82: 7.56160553
NLL Oracle Loss after pre-train epoch 82: 8.56260014
Train Loss after pre-train epoch 83: 7.56133794
NLL Oracle Loss after pre-train epoch 83: 8.55660152
Train Loss after pre-train epoch 84: 7.56122977
NLL Oracle Loss after pre-train epoch 84: 8.55683327
Train Loss after pre-train epoch 85: 7.56077473
NLL Oracle Loss after pre-train epoch 85: 8.55998898
Train Loss after pre-train epoch 86: 7.56055244
NLL Oracle Loss after pre-train epoch 86: 8.57083988
Train Loss after pre-train epoch 87: 7.56025251
NLL Oracle Loss after pre-train epoch 87: 8.56082344
Train Loss after pre-train epoch 88: 7.55985351
NLL Oracle Loss after pre-train epoch 88: 8.56233215
Train Loss after pre-train epoch 89: 7.55975399
NLL Oracle Loss after pre-train epoch 89: 8.55283260
Train Loss after pre-train epoch 90: 7.55935461
NLL Oracle Loss after pre-train epoch 90: 8.55344105
Train Loss after pre-train epoch 91: 7.55915108
NLL Oracle Loss after pre-train epoch 91: 8.56727123
Train Loss after pre-train epoch 92: 7.55884230
NLL Oracle Loss after pre-train epoch 92: 8.55267048
Train Loss after pre-train epoch 93: 7.55861678
NLL Oracle Loss after pre-train epoch 93: 8.58702374
Train Loss after pre-train epoch 94: 7.55848950
NLL Oracle Loss after pre-train epoch 94: 8.57006359
Train Loss after pre-train epoch 95: 7.55811044
NLL Oracle Loss after pre-train epoch 95: 8.57175255
Train Loss after pre-train epoch 96: 7.55800468
NLL Oracle Loss after pre-train epoch 96: 8.55994606
Train Loss after pre-train epoch 97: 7.55751246
NLL Oracle Loss after pre-train epoch 97: 8.56142807
Train Loss after pre-train epoch 98: 7.55731306
NLL Oracle Loss after pre-train epoch 98: 8.56027889
Train Loss after pre-train epoch 99: 7.55694220
NLL Oracle Loss after pre-train epoch 99: 8.54919624
