DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 5e-05
LEARNING_RATE_D: 5e-05
N_EPOCHS: 0
NUM_D: 1
NUM_G: 3
LOG_LOCATION: ./logs/pg_g3d1_g32_d32_pe6_pl1e-2_l5e-5/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6/
SAVE_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6/pg_p_h32_l1e-2.chk
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/pg_g3d1_g32_d32_pe6_pl1e-2_l5e-5/
SAVE_FILE_GAN: ./checkpoints/pg_g3d1_g32_d32_pe6_pl1e-2_l5e-5/chk
LOAD_FILE_GAN: ./checkpoints/pg_g3d1_g32_d32_pe6_pl1e-2_l5e-5/chk
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
2017-04-07 08:35:49.446373: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:35:49.446403: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:35:49.446408: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:35:49.446412: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:35:49.446415: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 08:36:00.121768: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:07:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-07 08:36:00.121803: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-07 08:36:00.121809: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-07 08:36:00.121821: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:07:00.0)
WARNING:tensorflow:From model.py:246: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Valid g_pre_loss before training: 8.58943105
Test g_pre_loss before training: 8.58908807
2017-04-07 08:36:19.235994: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 10728 get requests, put_count=10658 evicted_count=1000 eviction_rate=0.0938262 and unsatisfied allocation rate=0.10906
2017-04-07 08:36:19.236066: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-07 08:36:25.041069: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 3816 get requests, put_count=3871 evicted_count=1000 eviction_rate=0.258331 and unsatisfied allocation rate=0.253669
2017-04-07 08:36:25.041143: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Train Loss after pre-train epoch 0: 7.22261449
Valid Loss after pre-train epoch 0: 6.96151051
Test Loss after pre-train epoch 0: 6.91964013
Train Loss after pre-train epoch 1: 6.95645485
Valid Loss after pre-train epoch 1: 6.67300172
Test Loss after pre-train epoch 1: 6.63697350
Train Loss after pre-train epoch 2: 6.66297461
Valid Loss after pre-train epoch 2: 6.50583376
Test Loss after pre-train epoch 2: 6.47237490
Train Loss after pre-train epoch 3: 6.39791472
Valid Loss after pre-train epoch 3: 6.45650131
Test Loss after pre-train epoch 3: 6.41565634
Train Loss after pre-train epoch 4: 6.17391728
Valid Loss after pre-train epoch 4: 6.41587625
Test Loss after pre-train epoch 4: 6.37051553
Train Loss after pre-train epoch 5: 5.98119394
Valid Loss after pre-train epoch 5: 6.42211267
Test Loss after pre-train epoch 5: 6.36562855
