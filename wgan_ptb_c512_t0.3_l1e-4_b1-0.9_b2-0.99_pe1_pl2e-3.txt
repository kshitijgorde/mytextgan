nohup: ignoring input
DATASET: ptb
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 1
LEARNING_RATE_PRE_G: 0.002
HIDDEN_STATE_SIZE: 512
HIDDEN_STATE_SIZE_D: 512
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.9
BETA1_D: 0.9
BETA2_G: 0.99
BETA2_D: 0.99
LOG_LOCATION: ./logs/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe1_pl2e-3_l1e-4/
SAMPLES_FOLDER: samples/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe1_pl2e-3_l1e-4/
PRETRAIN_CHK_FOLDER: ./checkpoints/Wass_pt_p_h512_l2e-3_e1/
SAVE_FILE_PRETRAIN: ./checkpoints/Wass_pt_p_h512_l2e-3_e1/pt_p_h512_l2e-3.chk
LOAD_FILE_PRETRAIN: ./checkpoints/Wass_pt_p_h512_l2e-3_e1/pt_p_h512_l2e-3.chk
GAN_CHK_FOLDER: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe1_pl2e-3_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe1_pl2e-3_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe1_pl2e-3_l1e-4/chk
TEMPERATURE: 0.3
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 99, 49)
(32, 99, 49)
2017-04-11 02:38:41.682124: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-11 02:38:41.682171: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-11 02:38:41.682178: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-11 02:38:41.682183: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-11 02:38:41.682199: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-11 02:38:43.068378: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:07:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-11 02:38:43.068424: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-11 02:38:43.068434: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-11 02:38:43.068446: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:07:00.0)
WARNING:tensorflow:From model.py:289: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
2017-04-11 02:41:02.134107: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6594 get requests, put_count=6592 evicted_count=1000 eviction_rate=0.151699 and unsatisfied allocation rate=0.167122
2017-04-11 02:41:02.134194: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
Test g_pre_loss before training: 3.86780551
Calculated in 468.845s
2017-04-11 02:48:48.108275: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2763955 get requests, put_count=2763754 evicted_count=3000 eviction_rate=0.00108548 and unsatisfied allocation rate=0.001165
2017-04-11 02:48:49.746069: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2374 get requests, put_count=1887 evicted_count=1000 eviction_rate=0.529942 and unsatisfied allocation rate=0.639006
2017-04-11 02:48:49.746148: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 339 to 372
2017-04-11 02:48:51.390636: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2373 get requests, put_count=3250 evicted_count=2000 eviction_rate=0.615385 and unsatisfied allocation rate=0.498104
2017-04-11 02:48:51.390698: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 655 to 720
2017-04-11 02:48:57.567158: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 28479 get requests, put_count=28516 evicted_count=1000 eviction_rate=0.035068 and unsatisfied allocation rate=0.0392219
2017-04-11 02:48:57.567203: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 1694 to 1863
Train Loss after pre-train epoch 0: 1.77311582
Test Loss after pre-train epoch 0: 1.36580065
Test Loss after epoch 0: 1.39363711
Test Loss after epoch 1: 1.41044885
Test Loss after epoch 2: 1.41794542
Test Loss after epoch 3: 1.44138515
Test Loss after epoch 4: 1.43675959
Test Loss after epoch 5: 1.43888304
Test Loss after epoch 6: 1.44135214
Test Loss after epoch 7: 1.45550725
Test Loss after epoch 8: 1.46758990
Test Loss after epoch 9: 1.48355529
Test Loss after epoch 10: 1.48112647
Test Loss after epoch 11: 1.48577647
Test Loss after epoch 12: 1.51122147
Test Loss after epoch 13: 1.53317173
Test Loss after epoch 14: 1.51092328
Test Loss after epoch 15: 1.51083456
Test Loss after epoch 16: 1.50364466
Test Loss after epoch 17: 1.50012901
Test Loss after epoch 18: 1.51078518
Test Loss after epoch 19: 1.51241566
Test Loss after epoch 20: 1.51998079
Test Loss after epoch 21: 1.53408115
Test Loss after epoch 22: 1.53253572
Test Loss after epoch 23: 1.52007870
Test Loss after epoch 24: 1.52531428
Test Loss after epoch 25: 1.51887354
Test Loss after epoch 26: 1.51822917
Test Loss after epoch 27: 1.53576605
Test Loss after epoch 28: 1.59372209
Test Loss after epoch 29: 1.57527891
Test Loss after epoch 30: 1.58079925
Test Loss after epoch 31: 1.59973065
Test Loss after epoch 32: 1.64075452
Test Loss after epoch 33: 1.62970437
Test Loss after epoch 34: 1.65095751
Test Loss after epoch 35: 1.62054689
Test Loss after epoch 36: 1.63656360
Test Loss after epoch 37: 1.65268397
Test Loss after epoch 38: 1.64846172
Test Loss after epoch 39: 1.63418716
Test Loss after epoch 40: 1.64167751
Test Loss after epoch 41: 1.63689038
Test Loss after epoch 42: 1.64516949
Test Loss after epoch 43: 1.66686864
Test Loss after epoch 44: 1.67198142
Test Loss after epoch 45: 1.78840959
Test Loss after epoch 46: 1.77352422
Test Loss after epoch 47: 1.77544321
Test Loss after epoch 48: 1.85765771
