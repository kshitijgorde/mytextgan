DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 150
NUM_D: 7
NUM_G: 1
LOG_LOCATION: ./logs/Wasspg_g1d7_g32_d32_pe6_pl1e-2_l1e-4/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/Wasspg_g1d7_g32_d32_pe6_pl1e-2_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspg_g1d7_g32_d32_pe6_pl1e-2_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspg_g1d7_g32_d32_pe6_pl1e-2_l1e-4/chk
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5369)
(32, 20, 5369)
2017-04-07 14:20:26.784825: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 14:20:26.784880: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 14:20:26.784887: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 14:20:26.784892: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 14:20:26.784897: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 14:20:32.406572: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:86:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-07 14:20:32.406638: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-07 14:20:32.406653: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-07 14:20:32.406676: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:86:00.0)
WARNING:tensorflow:From model.py:253: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Test g_pre_loss before training: 8.58989346
2017-04-07 14:21:06.715236: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6248 get requests, put_count=6159 evicted_count=1000 eviction_rate=0.162364 and unsatisfied allocation rate=0.190301
2017-04-07 14:21:06.715363: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-07 14:21:12.814068: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 3877 get requests, put_count=3912 evicted_count=1000 eviction_rate=0.255624 and unsatisfied allocation rate=0.254836
2017-04-07 14:21:12.814146: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Train Loss after pre-train epoch 0: 7.21768187
Test Loss after pre-train epoch 0: 6.88237324
Train Loss after pre-train epoch 1: 6.92832567
Test Loss after pre-train epoch 1: 6.61860654
Train Loss after pre-train epoch 2: 6.64565637
Test Loss after pre-train epoch 2: 6.46807806
Train Loss after pre-train epoch 3: 6.38364179
Test Loss after pre-train epoch 3: 6.38211186
Train Loss after pre-train epoch 4: 6.14610383
Test Loss after pre-train epoch 4: 6.32301189
Train Loss after pre-train epoch 5: 5.95116912
Test Loss after pre-train epoch 5: 6.33081858
Epoch: [0] Batch: 7 d_loss: 0.03216648, g_loss: -0.31613654
Epoch: [0] Batch: 14 d_loss: 0.00040800, g_loss: -0.31687924
Epoch: [0] Batch: 21 d_loss: -0.00033270, g_loss: -0.31845409
Epoch: [0] Batch: 28 d_loss: 0.00229932, g_loss: -0.31674176
Epoch: [0] Batch: 35 d_loss: 0.00233911, g_loss: -0.31530899
Epoch: [0] Batch: 42 d_loss: 0.00397950, g_loss: -0.31566748
Epoch: [0] Batch: 49 d_loss: -0.00296818, g_loss: -0.31698740
Epoch: [0] Batch: 56 d_loss: -0.00194054, g_loss: -0.31508446
Epoch: [0] Batch: 63 d_loss: -0.00256611, g_loss: -0.31538078
Epoch: [0] Batch: 70 d_loss: -0.00130634, g_loss: -0.31630072
Epoch: [0] Batch: 77 d_loss: -0.00409137, g_loss: -0.31575149
Epoch: [0] Batch: 84 d_loss: -0.00012500, g_loss: -0.31464794
Epoch: [0] Batch: 91 d_loss: -0.00090613, g_loss: -0.31551409
Epoch: [0] Batch: 98 d_loss: -0.00041310, g_loss: -0.31668794
Epoch: [0] Batch: 105 d_loss: 0.00239985, g_loss: -0.31557882
Epoch: [0] Batch: 112 d_loss: -0.00061791, g_loss: -0.31430325
Epoch: [0] Batch: 119 d_loss: 0.00143878, g_loss: -0.31789422
Epoch: [0] Batch: 126 d_loss: 0.00029036, g_loss: -0.31555194
Epoch: [0] Batch: 133 d_loss: 0.00035548, g_loss: -0.31532708
Epoch: [0] Batch: 140 d_loss: -0.00071918, g_loss: -0.31597340
Epoch: [0] Batch: 147 d_loss: -0.00126302, g_loss: -0.31768161
Epoch: [0] Batch: 154 d_loss: 0.00010207, g_loss: -0.31873828
Epoch: [0] Batch: 161 d_loss: 0.00232121, g_loss: -0.31522542
Epoch: [0] Batch: 168 d_loss: -0.00119290, g_loss: -0.31453609
Epoch: [0] Batch: 175 d_loss: -0.00100443, g_loss: -0.31591508
Epoch: [0] Batch: 182 d_loss: -0.00294992, g_loss: -0.31642970
Epoch: [0] Batch: 189 d_loss: -0.00009296, g_loss: -0.31578502
Epoch: [0] Batch: 196 d_loss: 0.00233629, g_loss: -0.31254953
Epoch: [0] Batch: 203 d_loss: -0.00045971, g_loss: -0.31598958
Epoch: [0] Batch: 210 d_loss: -0.00118162, g_loss: -0.31534985
Epoch: [0] Batch: 217 d_loss: -0.00240840, g_loss: -0.31450489
Epoch: [0] Batch: 224 d_loss: -0.00512102, g_loss: -0.31249934
Epoch: [0] Batch: 231 d_loss: 0.00220063, g_loss: -0.31417117
Epoch: [0] Batch: 238 d_loss: 0.00188583, g_loss: -0.31248152
Epoch: [0] Batch: 245 d_loss: 0.00264612, g_loss: -0.31544006
Epoch: [0] Batch: 252 d_loss: -0.00210750, g_loss: -0.31385344
Epoch: [0] Batch: 259 d_loss: -0.00143741, g_loss: -0.31335363
Epoch: [0] Batch: 266 d_loss: 0.00081309, g_loss: -0.31167835
Epoch: [0] Batch: 273 d_loss: 0.00126265, g_loss: -0.31555408
Epoch: [0] Batch: 280 d_loss: -0.00109380, g_loss: -0.31403667
Epoch: [0] Batch: 287 d_loss: 0.00022619, g_loss: -0.31464803
Epoch: [0] Batch: 294 d_loss: 0.00109330, g_loss: -0.31502110
Epoch: [0] Batch: 301 d_loss: -0.00041947, g_loss: -0.31399071
Epoch: [0] Batch: 308 d_loss: -0.00223374, g_loss: -0.31321022
Epoch: [0] Batch: 315 d_loss: 0.00164997, g_loss: -0.31624588
Epoch: [0] Batch: 322 d_loss: 0.00107333, g_loss: -0.31360030
Epoch: [0] Batch: 329 d_loss: -0.00383073, g_loss: -0.31224400
Epoch: [0] Batch: 336 d_loss: 0.00038123, g_loss: -0.31309316
Epoch: [0] Batch: 343 d_loss: 0.00054413, g_loss: -0.31301713
Epoch: [0] Batch: 350 d_loss: 0.00148120, g_loss: -0.31307417
Test Loss after epoch 0: 6.33049862
Traceback (most recent call last):
  File "model.py", line 415, in <module>
    samples.extend(generate_samples())
  File "model.py", line 247, in generate_samples
    decoded.append(c.idx2char[samples[i][j]])
TypeError: only integer scalar arrays can be converted to a scalar index
