#!/usr/bin/env python
import tensorflow as tf
from tensorflow.contrib.rnn import LSTMCell
from tensorflow.contrib import rnn
from tensorflow.contrib import legacy_seq2seq
from gumbel_softmax import gumbel_softmax
from constants import *
import numpy as np
import os
import time
import sys
from data.ptb import CharLevelPTB, WordLevelPTB
from data.rnnpg import CharLevelRNNPG
from data.oracle import OracleDataloader, OracleVerifier
from utils import *

import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='gumbel-gans')

    parser.add_argument('--dataset', type=str, default=DATASET, choices=['ptb', 'pg', 'oracle'])
    parser.add_argument('--batch_size', type=int, default=BATCH_SIZE)
    parser.add_argument('--pretrain_epochs', type=int, default=PRETRAIN_EPOCHS)
    parser.add_argument('--lr_pretrain', type=float, default=LEARNING_RATE_PRE_G)
    parser.add_argument('--hidden_size_g', type=int, default=HIDDEN_STATE_SIZE)
    parser.add_argument('--hidden_size_d', type=int, default=HIDDEN_STATE_SIZE_D)
    parser.add_argument('--lr_g', type=float, default=LEARNING_RATE_G)
    parser.add_argument('--lr_d', type=float, default=LEARNING_RATE_D)
    parser.add_argument('--epochs', type=int, default=N_EPOCHS)
    parser.add_argument('--beta1_g', type=float, default=BETA1_G)
    parser.add_argument('--beta1_d', type=float, default=BETA1_D)
    parser.add_argument('--beta2_g', type=float, default=BETA2_G)
    parser.add_argument('--beta2_d', type=float, default=BETA2_D)
    parser.add_argument('--num_d', type=int, default=NUM_D)
    parser.add_argument('--num_g', type=int, default=NUM_G)
    parser.add_argument('--temp', type=float, default=TEMPERATURE)
    parser.add_argument('--radius', type=float, default=1.)
    parser.add_argument('--orig', type=float, default=1.)
    parser.add_argument('--first_char', type=str, default=FIRST_CHAR, choices=['start', 'rand'])
    parser.add_argument('--over_num', type=int, default=1000)
    parser.add_argument('--train', type=str, default='train')
    parser.add_argument('--seed', type=int, default=0)

    return parser.parse_args()

args = parse_args()

DATASET = args.dataset
BATCH_SIZE = args.batch_size
PRETRAIN_EPOCHS = args.pretrain_epochs
LEARNING_RATE_PRE_G = args.lr_pretrain
HIDDEN_STATE_SIZE = args.hidden_size_g
HIDDEN_STATE_SIZE_D = args.hidden_size_d
LEARNING_RATE_G = args.lr_g
LEARNING_RATE_D = args.lr_d
N_EPOCHS = args.epochs
BETA1_G = args.beta1_g
BETA1_D = args.beta1_d
BETA2_G = args.beta2_g
BETA2_D = args.beta2_d
NUM_D = args.num_d
NUM_G = args.num_g
TEMPERATURE = args.temp
FIRST_CHAR = args.first_char
SEED = args.seed

TRAIN = [args.train, args.train, args.train]

from decimal import Decimal

# define other constants
SUBFOLDER_NAME = 'RWass' + DATASET[:2] + '_s' + str(args.seed) + '_r' + str(args.radius) + '_o' + str(args.orig) + '_t' + str(TEMPERATURE) + '_b1-' + str(BETA1_G) + '_b2-' + str(BETA2_G) + '_g' + str(NUM_G) + 'd' + str(NUM_D) + '_g' + str(HIDDEN_STATE_SIZE) + '_d' + str(HIDDEN_STATE_SIZE_D) + '_pe' + str(PRETRAIN_EPOCHS) + '_pl' + str("{:.0e}".format(Decimal(LEARNING_RATE_PRE_G))) + '_l'+ str("{:.0e}".format(Decimal(LEARNING_RATE_G))) + '/'
LOG_LOCATION = './logs/' + SUBFOLDER_NAME
PRETRAIN_CHK_FOLDER = False
SAVE_FILE_PRETRAIN = False
LOAD_FILE_PRETRAIN = SAVE_FILE_PRETRAIN
GAN_CHK_FOLDER = './checkpoints/' + SUBFOLDER_NAME
SAVE_FILE_GAN = GAN_CHK_FOLDER + 'chk'
LOAD_FILE_GAN = SAVE_FILE_GAN
SAMPLES_FOLDER = 'samples/%s/'%args.train + SUBFOLDER_NAME

if DATASET == 'ptb':
    TEST_BATCH_SIZE = BATCH_SIZE
else:
    # TODO: Make this larger for faster validation, test time
    TEST_BATCH_SIZE = BATCH_SIZE

def print_hyperparam_consts():
    print('DATASET: ' + str(DATASET))
    print('BATCH_SIZE: ' + str(BATCH_SIZE))
    print('PRETRAIN_EPOCHS: ' + str(PRETRAIN_EPOCHS))
    print('LEARNING_RATE_PRE_G: ' + str(LEARNING_RATE_PRE_G))
    print('HIDDEN_STATE_SIZE: ' + str(HIDDEN_STATE_SIZE))
    print('HIDDEN_STATE_SIZE_D: ' + str(HIDDEN_STATE_SIZE_D))
    print('LEARNING_RATE_G: ' + str(LEARNING_RATE_G))
    print('LEARNING_RATE_D: ' + str(LEARNING_RATE_D))
    print('N_EPOCHS: ' + str(N_EPOCHS))
    print('NUM_D: ' + str(NUM_D))
    print('NUM_G: ' + str(NUM_G))
    print('BETA1_G: ' + str(BETA1_G))
    print('BETA1_D: ' + str(BETA1_D))
    print('BETA2_G: ' + str(BETA2_G))
    print('BETA2_D: ' + str(BETA2_D))
    print('LOG_LOCATION: ' + str(LOG_LOCATION))
    print('SAMPLES_FOLDER: ' + str(SAMPLES_FOLDER))
    print('PRETRAIN_CHK_FOLDER: ' + str(PRETRAIN_CHK_FOLDER))
    print('SAVE_FILE_PRETRAIN: ' + str(SAVE_FILE_PRETRAIN))
    print('LOAD_FILE_PRETRAIN: ' + str(LOAD_FILE_PRETRAIN))
    print('GAN_CHK_FOLDER: ' + str(GAN_CHK_FOLDER))
    print('SAVE_FILE_GAN: ' + str(SAVE_FILE_GAN))
    print('LOAD_FILE_GAN: ' + str(LOAD_FILE_GAN))
    print('TEMPERATURE: ' + str(TEMPERATURE))
    print('FIRST_CHAR: ' + str(FIRST_CHAR))

print_hyperparam_consts()

# create dirs that don't exist
if SAVE_FILE_PRETRAIN:
    create_dir_if_not_exists('/'.join(SAVE_FILE_PRETRAIN.split('/')[:-1]))
if SAVE_FILE_GAN:
    create_dir_if_not_exists('/'.join(SAVE_FILE_GAN.split('/')[:-1]))
create_dir_if_not_exists(LOG_LOCATION)
create_dir_if_not_exists(SAMPLES_FOLDER)

SEQ_LENGTH, VOCAB_SIZE, TEST_SIZE, c = None, None, None, None

if DATASET == 'ptb':
    c = CharLevelPTB(TRAIN, SEED)
    VOCAB_SIZE = c.vocab_size
    SEQ_LENGTH = c.seqlen
elif DATASET == 'pg':
    c = CharLevelRNNPG()
    VOCAB_SIZE = PG_VOCAB_SIZE
    SEQ_LENGTH = PG_SEQ_LENGTH
elif DATASET == 'oracle':
    c = OracleDataloader(BATCH_SIZE, ORACLE_VOCAB_SIZE)
    VOCAB_SIZE = ORACLE_VOCAB_SIZE
    SEQ_LENGTH = ORACLE_SEQ_LENGTH
    TEST_SIZE = ORACLE_TEST_SIZE

initial_c = tf.placeholder(tf.float32, shape=(BATCH_SIZE, HIDDEN_STATE_SIZE))
initial_h = tf.placeholder(tf.float32, shape=(BATCH_SIZE, HIDDEN_STATE_SIZE))

inputs_pre = tf.placeholder(tf.float32, [BATCH_SIZE, SEQ_LENGTH-1, VOCAB_SIZE], name='inputs_pre')
inputs = tf.placeholder(tf.float32, [BATCH_SIZE, SEQ_LENGTH-1, VOCAB_SIZE], name='inputs')

targets = tf.placeholder(tf.float32, [BATCH_SIZE, SEQ_LENGTH-1, VOCAB_SIZE])

p_radius = tf.placeholder(tf.float32, shape=(), name='p_radius')
p_radius_ = tf.placeholder(tf.float32, shape=(), name='p_radius_')
p_center = tf.placeholder(tf.float32, shape=(HIDDEN_STATE_SIZE_D * (SEQ_LENGTH-1)), name='p_center')
p_center_ = tf.placeholder(tf.float32, shape=(HIDDEN_STATE_SIZE_D * (SEQ_LENGTH-1)), name='p_center_')


def generator(initial_c, initial_h, mode='gan', first='start', inputs=None, targets=None, reuse=False):
    assert mode in ['test', 'pretrain', 'gan']
    assert first in ['start', 'rand']
    with tf.variable_scope("generator") as scope:
        if reuse:
            scope.reuse_variables()
        softmax_w = tf.get_variable("softmax_w", [HIDDEN_STATE_SIZE, VOCAB_SIZE])
        softmax_b = tf.get_variable("softmax_b", [VOCAB_SIZE])

        def loop_function(prev_output, _):
            prev_output = tf.matmul(prev_output, softmax_w) + softmax_b
            return gumbel_softmax(prev_output, temperature=TEMPERATURE, hard=True)

        first_input = np.zeros((BATCH_SIZE, VOCAB_SIZE))
        # Create a dummy first input
        if first == 'start':
            first_input[:, c.start_char_idx] = 1
        # Create a first input randomly
        elif first == 'rand':
            start_chars = np.random.randint(0, VOCAB_SIZE, BATCH_SIZE)
            first_input[np.arange(BATCH_SIZE), start_chars] = 1
        # TODO: Create a first input based on statistical distribution
        first_input = tf.constant(first_input, dtype=tf.float32)

        cell = LSTMCell(HIDDEN_STATE_SIZE, state_is_tuple=True, reuse=reuse)
        if mode == 'pretrain':
            inputs = tf.unstack(inputs, axis=1)
            outputs, states = legacy_seq2seq.rnn_decoder(decoder_inputs=inputs,
                                                         initial_state=(initial_c, initial_h),
                                                         cell=cell, loop_function=loop_function, scope=scope)
            logits = [tf.matmul(output, softmax_w) + softmax_b for output in outputs]

            targets = tf.unstack(tf.transpose(targets, [1, 0, 2]))

            loss = [tf.nn.softmax_cross_entropy_with_logits(labels=target, logits=logit)
                    for target, logit in zip(targets, logits)] # ignore start token
            loss = tf.reduce_mean(loss)
            return loss, states
        elif mode == 'test':
            outputs, states = legacy_seq2seq.rnn_decoder(decoder_inputs=BATCH_SIZE * [first_input],
                                                         initial_state=(initial_c, initial_h),
                                                         cell=cell, loop_function=loop_function, scope=scope)
            logits = [tf.matmul(output, softmax_w) + softmax_b for output in outputs]
            return tf.transpose(tf.argmax(logits, axis=2))
        else:
            inputs = tf.unstack(inputs, axis=1)
            outputs, states = legacy_seq2seq.rnn_decoder(decoder_inputs=inputs,
                                                         initial_state=(initial_c, initial_h),
                                                         cell=cell, loop_function=loop_function, scope=scope)

            logits = [tf.matmul(output, softmax_w) + softmax_b for output in outputs]

            ys = [gumbel_softmax(logit, temperature=TEMPERATURE, hard=True) for logit in logits]
            ys = tf.stack(ys, axis=1)
            return ys


def discriminator(x, reuse=False):
    with tf.variable_scope("discriminator") as scope:
        if reuse:
            scope.reuse_variables()
        lstm = LSTMCell(HIDDEN_STATE_SIZE_D, state_is_tuple=True, reuse=reuse)
        softmax_w = tf.get_variable("softmax_w", [HIDDEN_STATE_SIZE_D, N_D_CLASSES])
        softmax_b = tf.get_variable("softmax_b", [N_D_CLASSES])
        # softmax_w = tf.get_variable("softmax_w", [SEQ_LENGTH-1, HIDDEN_STATE_SIZE_D, N_D_CLASSES])
        # softmax_b = tf.get_variable("softmax_b", [SEQ_LENGTH-1, N_D_CLASSES])

        x = tf.unstack(x, axis=1)
        lstm_outputs, _states = tf.contrib.rnn.static_rnn(lstm, x, dtype=tf.float32, scope=scope)

        # npark
        # Previously, D(x) was made at the end of a URL. --> tf.matmul(lstm_outputs[-1], softmax_w) + softmax_b
        # Now, D(x_i) is calculated for every character of a URL.
        # http://www.uncc.com -> D(x), D(x_0) for h, d(x_1) for ht.... , d(x_i) http://www.unc
        h = tf.stack(lstm_outputs, axis=1)
        h = tf.transpose(h, [1, 0, 2])

        # Returns two values, D(x_i) and vectorized features
        # return tf.transpose(tf.matmul(h, softmax_w), [1, 0, 2]) + softmax_b, tf.reshape(h, [BATCH_SIZE, HIDDEN_STATE_SIZE_D * (SEQ_LENGTH-1)])
        return tf.matmul(lstm_outputs[-1], softmax_w) + softmax_b, tf.reshape(h, [BATCH_SIZE,
                                                                                            HIDDEN_STATE_SIZE_D * (
                                                                                            SEQ_LENGTH - 1)])

def generate_test_file(g_test, sess, eval_file):
    z = sample_Z(BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
    c_z, h_z = np.vsplit(z, 2)

    with open(eval_file, 'w') as f:
        for _ in xrange(TEST_SIZE/BATCH_SIZE):
            oracle_out = sess.run([g_test], feed_dict={
                inputs_pre: sample_Z_seq(BATCH_SIZE, SEQ_LENGTH-1, VOCAB_SIZE),
                initial_c: c_z,
                initial_h: h_z
            })
            for l in oracle_out[0]:
                buffer = ' '.join([str(x) for x in l]) + '\n'
                f.write(buffer)

# Evaluate the losses
d_logits, d_features = discriminator(inputs) # inputs = real URLs
g_pre_loss, g_Z_states = generator(initial_c, initial_h, mode='pretrain', inputs=inputs_pre, targets=targets)

# Optimizers
t_vars = tf.trainable_variables()
#print [var.name for var in t_vars]
g_vars = [var for var in t_vars if var.name.startswith('generator')]
d_vars = [var for var in t_vars if var.name.startswith('discriminator')]

g_pre_optim = tf.train.AdamOptimizer(LEARNING_RATE_PRE_G, name="Adam_g_pre").minimize(g_pre_loss, var_list=g_vars)
g_pre_loss_sum = tf.summary.scalar("g_pre_loss", g_pre_loss)

g = generator(initial_c, initial_h, first=FIRST_CHAR, inputs=inputs_pre, reuse=True)
g_test = generator(initial_c, initial_h, reuse=True, mode='test')
d_logits_, g_features = discriminator(g, reuse=True)

# npark: Calculate two centroids
d_mean = tf.reduce_mean(d_features, axis=0)
g_mean = tf.reduce_mean(g_features, axis=0)

# npark: Moving average of centroids
m_center = 0.99*p_center + 0.01*d_mean
m_center_ = 0.99*p_center_ + 0.01*g_mean

# npark: Calculate two radius values
d_dist = tf.reduce_mean(tf.norm(d_mean - d_features, ord=2, axis=1))
g_dist = tf.reduce_mean(tf.norm(g_mean - g_features, ord=2, axis=1))

# npark: Moving average of radius values
m_radius = 0.99*p_radius + 0.01*d_dist
m_radius_ = 0.99*p_radius_ + 0.01*g_dist

d_loss = tf.reduce_mean(d_logits_) - tf.reduce_mean(d_logits)

# Sphere manifold matching
g_loss = args.orig*tf.norm(m_center - m_center_, ord=1) + args.orig*tf.abs(m_radius - m_radius_) - tf.reduce_mean(d_logits_)

# WGAN lipschitz-penalty
alpha = tf.random_uniform(
    shape=[BATCH_SIZE, 1, 1],
    minval=0.,
    maxval=1.
)
fake_inputs, real_inputs = g, inputs
differences = fake_inputs - real_inputs
interpolates = real_inputs + (alpha*differences)
print interpolates.get_shape()
gradients = tf.gradients(discriminator(interpolates, reuse=True)[0], [interpolates])[0]
print gradients.get_shape()
slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1, 2]))
gradient_penalty = tf.reduce_mean((slopes-1.)**2)
d_loss += LAMBDA*gradient_penalty

g_loss_sum = tf.summary.scalar("g_loss", g_loss)
d_loss_sum = tf.summary.scalar("d_loss", d_loss)

d_optim = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE_D, beta1=BETA1_D, beta2=BETA2_D).minimize(d_loss, var_list=d_vars)
g_optim = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE_G, beta1=BETA1_G, beta2=BETA2_G, name="Adam_g").minimize(g_loss, var_list=g_vars)

session_conf = tf.ConfigProto(
      intra_op_parallelism_threads=1,
      inter_op_parallelism_threads=1)
with tf.Session(config=session_conf) as sess:

    def generate_samples():
        z = sample_Z(BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
        c_z, h_z = np.vsplit(z, 2)

        samples = sess.run(g, feed_dict={
            inputs_pre: sample_Z_seq(BATCH_SIZE, SEQ_LENGTH-1, VOCAB_SIZE),
            initial_c: c_z,
            initial_h: h_z
        })

        decoded_samples = []
        for i in xrange(len(samples)):
            decoded = []
            for j in xrange(len(samples[i])):
                char = c.idx2char[np.argmax(samples[i][j])]
                if char in ['<pad>', '<end>']: break
                decoded.append(char)
            decoded_samples.append(tuple(decoded))
        return decoded_samples

    if DATASET == 'oracle':
        t=OracleVerifier(BATCH_SIZE, sess)
    tf.initialize_all_variables().run()
    writer = tf.summary.FileWriter(LOG_LOCATION, sess.graph)
    counter = 1

    saver = tf.train.Saver()

    if LOAD_FILE_PRETRAIN and tf.train.latest_checkpoint(PRETRAIN_CHK_FOLDER) == LOAD_FILE_PRETRAIN:
        # saver = tf.train.import_meta_graph(LOAD_FILE_PRETRAIN + '.meta')
        saver.restore(sess, LOAD_FILE_PRETRAIN)
        if DATASET == 'oracle':
            generate_test_file(g_test, sess, t.eval_file)
            print "NLL Oracle Loss after loading model: %.8f" % t.get_loss()
        else:
            batch_idx = 0
            ltot = 0.
            z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
            c_z, h_z = np.vsplit(z, 2)
            for batch in c.get_test_batch(TEST_BATCH_SIZE):
                batch = c.convert_batch_to_input_target(batch)
                batch_input, batch_targets = batch

                g_pre_loss_curr, summary_str, z_stateful_out = sess.run([g_pre_loss, g_pre_loss_sum, g_Z_states], feed_dict={
                    inputs_pre: batch_input,
                    initial_c: c_z,
                    initial_h: h_z,
                    targets: batch_targets
                })

                if TEST_BATCH_SIZE == 1:
                    c_z, h_z = z_stateful_out
                else:
                    z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
                    c_z, h_z = np.vsplit(z, 2)
                ltot += g_pre_loss_curr
                # writer.add_summary(summary_str, counter)
                batch_idx += 1
            print "Test g_pre_loss after loading model: %.8f" % (ltot/batch_idx)
    else:
        if DATASET == 'oracle':
            generate_test_file(g_test, sess, t.eval_file)
            print "NLL Oracle Loss before training: %.8f" % t.get_loss()
        else:
            start_time = time.time()
            batch_idx = 0
            ltot = 0.
            z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
            c_z, h_z = np.vsplit(z, 2)
            for batch in c.get_test_batch(TEST_BATCH_SIZE):
                batch = c.convert_batch_to_input_target(batch)
                batch_input, batch_targets = batch

                g_pre_loss_curr, summary_str, z_stateful_out = sess.run([g_pre_loss, g_pre_loss_sum, g_Z_states], feed_dict={
                    inputs_pre: batch_input,
                    initial_c: c_z,
                    initial_h: h_z,
                    targets: batch_targets
                })

                if TEST_BATCH_SIZE == 1:
                    c_z, h_z = z_stateful_out
                else:
                    z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
                    c_z, h_z = np.vsplit(z, 2)
                ltot += g_pre_loss_curr
                # writer.add_summary(summary_str, counter)
                batch_idx += 1
            print "Test g_pre_loss before training: %.8f" % (ltot/batch_idx)
            print "Calculated in {:.3f}s".format(time.time() - start_time)

        for pre_epoch in xrange(PRETRAIN_EPOCHS):
            batch_idx = 0
            train_ltot = 0.
            for batch in c.get_train_batch(BATCH_SIZE):
                batch = c.convert_batch_to_input_target(batch)
                batch_input, batch_targets = batch

                z = sample_Z(BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
                c_z, h_z = np.vsplit(z, 2)
                _, g_pre_loss_curr, summary_str = sess.run([g_pre_optim, g_pre_loss, g_pre_loss_sum], feed_dict={
                    inputs_pre: batch_input,
                    initial_c: c_z,
                    initial_h: h_z,
                    targets: batch_targets
                })
                train_ltot += g_pre_loss_curr
                # writer.add_summary(summary_str, counter)
                batch_idx += 1
            print "Train Loss after pre-train epoch %d: %.8f" % (pre_epoch, train_ltot / batch_idx)
                # print "Epoch: [%d] Batch: %d, g_pre_loss: %.8f" % (pre_epoch, batch_idx, g_pre_loss_curr)
            if DATASET == 'oracle':
                generate_test_file(g_test, sess, t.eval_file)
                print "NLL Oracle Loss after pre-train epoch %d: %.8f" % (pre_epoch, t.get_loss())
            else:
                batch_idx = 0
                ltot = 0.
                z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
                c_z, h_z = np.vsplit(z, 2)
                for batch in c.get_test_batch(TEST_BATCH_SIZE):
                    batch = c.convert_batch_to_input_target(batch)
                    batch_input, batch_targets = batch

                    g_pre_loss_curr, summary_str, z_stateful_out = sess.run([g_pre_loss, g_pre_loss_sum, g_Z_states], feed_dict={
                        inputs_pre: batch_input,
                        initial_c: c_z,
                        initial_h: h_z,
                        targets: batch_targets
                    })

                    if TEST_BATCH_SIZE == 1:
                        c_z, h_z = z_stateful_out
                    else:
                        z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
                        c_z, h_z = np.vsplit(z, 2)
                    ltot += g_pre_loss_curr
                    # writer.add_summary(summary_str, counter)
                    batch_idx += 1
                print "Test Loss after pre-train epoch %d: %.8f" % (pre_epoch, ltot / batch_idx)

            if SAVE_FILE_PRETRAIN:
                saver.save(sess, SAVE_FILE_PRETRAIN)

            samples = []

        if DATASET != 'oracle':
            for i in xrange(args.over_num / BATCH_SIZE):
                samples.extend(generate_samples())
                with open(SAMPLES_FOLDER + 'pre_samples.txt', 'w') as f:
                    for s in samples:
                        s = "".join(s)
                        f.write(s + "\n")

    counter = 1
    best_loss = np.inf
    for epoch in xrange(N_EPOCHS):
        sys.stdout.flush()
        batch_idx = 0
        d_center = np.zeros(HIDDEN_STATE_SIZE_D * (SEQ_LENGTH-1))
        d_radius = 0
        g_center = np.zeros(HIDDEN_STATE_SIZE_D * (SEQ_LENGTH-1))
        g_radius = 0
        for batch in c.get_train_batch(BATCH_SIZE, shuffle=True):
            batch_idx += 1

            # To remove start token, since generator doesn't generate it either
            batch = c.convert_batch_to_input_target(batch)
            _,batch_targets = batch

            # if batch_idx % NUM_G == 0:
            z = sample_Z(BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
            c_z, h_z = np.vsplit(z, 2)
            _, d_loss_curr, summary_str = sess.run([d_optim, d_loss, d_loss_sum], feed_dict={
                inputs: batch_targets,
                inputs_pre: sample_Z_seq(BATCH_SIZE, SEQ_LENGTH-1, VOCAB_SIZE),
                initial_c: c_z,
                initial_h: h_z
            })
            # writer.add_summary(summary_str, counter)

            # if batch_idx % NUM_D != 0:
            #     continue

            z = sample_Z(BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
            c_z, h_z = np.vsplit(z, 2)
            for _ in range(NUM_G):
                _, g_loss_curr, summary_str, c_mean, c_mean_, c_dist, c_dist_ = sess.run([g_optim, g_loss, g_loss_sum, m_center, m_center_, m_radius, m_radius_], feed_dict={
                    inputs: batch_targets,
                    inputs_pre: sample_Z_seq(BATCH_SIZE, SEQ_LENGTH-1, VOCAB_SIZE),
                    initial_c: c_z,
                    initial_h: h_z,
                    p_center: d_center,
                    p_center_: g_center,
                    p_radius: d_radius,
                    p_radius_: g_radius,
                })
                d_center = c_mean
                g_center = c_mean_
                d_radius = c_dist
                g_radius = c_dist_
                # print "--------------------"
                # print d_center, d_radius
                # print g_center, g_radius

            # writer.add_summary(summary_str, counter)

            counter += 1
            # print "Epoch: [%d] Batch: %d d_loss: %.8f, g_loss: %.8f" % (epoch, batch_idx, d_loss_curr, g_loss_curr)

        if DATASET == 'oracle':
            generate_test_file(g_test, sess, t.eval_file)
            print "NLL Oracle Loss after epoch %d: %.8f" % (epoch, t.get_loss())
        else:
            batch_idx = 0
            ltot = 0.
            z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
            c_z, h_z = np.vsplit(z, 2)
            for batch in c.get_test_batch(TEST_BATCH_SIZE):
                batch = c.convert_batch_to_input_target(batch)
                batch_input, batch_targets = batch

                g_pre_loss_curr, summary_str, z_stateful_out = sess.run([g_pre_loss, g_pre_loss_sum, g_Z_states], feed_dict={
                    inputs_pre: batch_input,
                    initial_c: c_z,
                    initial_h: h_z,
                    targets: batch_targets
                })

                if TEST_BATCH_SIZE == 1:
                    c_z, h_z = z_stateful_out
                else:
                    z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
                    c_z, h_z = np.vsplit(z, 2)
                ltot += g_pre_loss_curr
                # writer.add_summary(summary_str, counter)
                batch_idx += 1
            print "Test Loss after epoch %d: %.8f" % (epoch, ltot / batch_idx)
            test_loss = ltot / batch_idx

        samples = []

        if DATASET != 'oracle' and best_loss > test_loss:
            best_loss = test_loss
            for i in xrange(args.over_num / BATCH_SIZE):
                samples.extend(generate_samples())
                with open(SAMPLES_FOLDER + 'best_samples.txt', 'w') as f:
                    for s in samples:
                        s = "".join(s)
                        f.write(s + "\n")
        else:
            for i in xrange(args.over_num / BATCH_SIZE):
                samples.extend(generate_samples())
                with open(SAMPLES_FOLDER + 'current_samples.txt', 'w') as f:
                    for s in samples:
                        s = "".join(s)
                        f.write(s + "\n")
