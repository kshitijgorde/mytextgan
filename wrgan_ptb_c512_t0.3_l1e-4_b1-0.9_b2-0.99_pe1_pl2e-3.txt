nohup: ignoring input
DATASET: ptb
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 1
LEARNING_RATE_PRE_G: 0.002
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.9
BETA1_D: 0.9
BETA2_G: 0.99
BETA2_D: 0.99
LOG_LOCATION: ./logs/RWasspt_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe1_pl2e-3_l1e-4/
SAMPLES_FOLDER: samples/RWasspt_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe1_pl2e-3_l1e-4/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/RWasspt_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe1_pl2e-3_l1e-4/
SAVE_FILE_GAN: ./checkpoints/RWasspt_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe1_pl2e-3_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/RWasspt_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe1_pl2e-3_l1e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 99, 49)
(32, 99, 49)
2017-04-20 08:57:47.430897: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-20 08:57:47.430947: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-20 08:57:47.430954: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-20 08:57:47.430971: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-20 08:57:47.430975: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-20 08:57:55.541342: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:06:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-20 08:57:55.541382: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-20 08:57:55.541388: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-20 08:57:55.541397: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:06:00.0)
WARNING:tensorflow:From model.py:292: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
2017-04-20 09:00:40.873544: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6534 get requests, put_count=6528 evicted_count=1000 eviction_rate=0.153186 and unsatisfied allocation rate=0.169268
2017-04-20 09:00:40.873601: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
Test g_pre_loss before training: 3.91943821
Calculated in 438.824s
2017-04-20 09:07:56.251778: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2764649 get requests, put_count=2764266 evicted_count=3000 eviction_rate=0.00108528 and unsatisfied allocation rate=0.00123054
2017-04-20 09:07:57.297662: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2374 get requests, put_count=1864 evicted_count=1000 eviction_rate=0.536481 and unsatisfied allocation rate=0.648694
2017-04-20 09:07:57.297734: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 339 to 372
2017-04-20 09:07:58.371008: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2373 get requests, put_count=2307 evicted_count=1000 eviction_rate=0.433463 and unsatisfied allocation rate=0.474083
2017-04-20 09:07:58.371096: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 655 to 720
2017-04-20 09:08:02.460003: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 28478 get requests, put_count=28531 evicted_count=1000 eviction_rate=0.0350496 and unsatisfied allocation rate=0.0386614
2017-04-20 09:08:02.460059: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 1694 to 1863
Train Loss after pre-train epoch 0: 2.38667254
Test Loss after pre-train epoch 0: 2.11376369
2017-04-20 09:19:10.827117: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6506062 get requests, put_count=6504617 evicted_count=10000 eviction_rate=0.00153737 and unsatisfied allocation rate=0.0017851
2017-04-20 09:19:22.865105: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 22377 get requests, put_count=23297 evicted_count=2000 eviction_rate=0.085848 and unsatisfied allocation rate=0.059302
2017-04-20 09:19:22.865159: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 2725 to 2997
Test Loss after epoch 0: 2.11971330
Test Loss after epoch 1: 2.11738232
Test Loss after epoch 2: 2.11609677
Test Loss after epoch 3: 2.11654900
Test Loss after epoch 4: 2.11753173
Test Loss after epoch 5: 2.12067182
Test Loss after epoch 6: 2.12230394
Test Loss after epoch 7: 2.12279140
Test Loss after epoch 8: 2.11819402
Test Loss after epoch 9: 2.11539136
Test Loss after epoch 10: 2.11547287
Test Loss after epoch 11: 2.11589525
Test Loss after epoch 12: 2.11683329
Test Loss after epoch 13: 2.11525500
Test Loss after epoch 14: 2.11449425
Test Loss after epoch 15: 2.11503917
Test Loss after epoch 16: 2.11626650
Test Loss after epoch 17: 2.11811578
Test Loss after epoch 18: 2.11661042
Test Loss after epoch 19: 2.11607661
Test Loss after epoch 20: 2.11628334
Test Loss after epoch 21: 2.11605582
Test Loss after epoch 22: 2.11764785
Test Loss after epoch 23: 2.11686646
Test Loss after epoch 24: 2.11674596
Test Loss after epoch 25: 2.11688480
Test Loss after epoch 26: 2.11676911
Test Loss after epoch 27: 2.11713915
Test Loss after epoch 28: 2.11788554
Test Loss after epoch 29: 2.11775727
Test Loss after epoch 30: 2.11961040
Test Loss after epoch 31: 2.12024484
Test Loss after epoch 32: 2.12010438
Test Loss after epoch 33: 2.12247207
Test Loss after epoch 34: 2.12086703
Test Loss after epoch 35: 2.12239886
Test Loss after epoch 36: 2.12270809
Test Loss after epoch 37: 2.12431873
Test Loss after epoch 38: 2.12586863
Test Loss after epoch 39: 2.12767304
Test Loss after epoch 40: 2.12571264
Test Loss after epoch 41: 2.12684254
Test Loss after epoch 42: 2.12976368
Test Loss after epoch 43: 2.12916462
Test Loss after epoch 44: 2.12791871
Test Loss after epoch 45: 2.12708838
Test Loss after epoch 46: 2.12564405
Test Loss after epoch 47: 2.12700822
Test Loss after epoch 48: 2.12379516
Test Loss after epoch 49: 2.12463538
Test Loss after epoch 50: 2.12407281
Test Loss after epoch 51: 2.12601050
Test Loss after epoch 52: 2.12673835
Test Loss after epoch 53: 2.13151266
Test Loss after epoch 54: 2.12970970
Test Loss after epoch 55: 2.13060567
Test Loss after epoch 56: 2.13123731
Test Loss after epoch 57: 2.12908675
Test Loss after epoch 58: 2.13128661
Test Loss after epoch 59: 2.13222762
Test Loss after epoch 60: 2.13567573
Test Loss after epoch 61: 2.13694711
Test Loss after epoch 62: 2.13987641
Test Loss after epoch 63: 2.13854448
Test Loss after epoch 64: 2.14136821
Test Loss after epoch 65: 2.14814670
Test Loss after epoch 66: 2.15298057
Test Loss after epoch 67: 2.15793591
Test Loss after epoch 68: 2.15300600
Test Loss after epoch 69: 2.15441927
Test Loss after epoch 70: 2.15904391
Test Loss after epoch 71: 2.15577158
Test Loss after epoch 72: 2.15267073
Test Loss after epoch 73: 2.15507393
Test Loss after epoch 74: 2.15251272
Test Loss after epoch 75: 2.15785685
Test Loss after epoch 76: 2.15258695
Test Loss after epoch 77: 2.15592649
Test Loss after epoch 78: 2.15570913
Test Loss after epoch 79: 2.16330526
Test Loss after epoch 80: 2.15875852
Test Loss after epoch 81: 2.16206188
Test Loss after epoch 82: 2.15140316
Test Loss after epoch 83: 2.15364213
Test Loss after epoch 84: 2.15686130
Test Loss after epoch 85: 2.15923223
Test Loss after epoch 86: 2.16782792
Test Loss after epoch 87: 2.16779740
Test Loss after epoch 88: 2.16834563
Test Loss after epoch 89: 2.15926478
Test Loss after epoch 90: 2.15713371
Test Loss after epoch 91: 2.15457789
Test Loss after epoch 92: 2.15300880
Test Loss after epoch 93: 2.15340000
Test Loss after epoch 94: 2.15985893
Test Loss after epoch 95: 2.16295792
Test Loss after epoch 96: 2.17034323
Test Loss after epoch 97: 2.17000757
Test Loss after epoch 98: 2.16765287
Test Loss after epoch 99: 2.16849996
Test Loss after epoch 100: 2.15907004
Test Loss after epoch 101: 2.16083466
Test Loss after epoch 102: 2.16347700
Test Loss after epoch 103: 2.17204949
Test Loss after epoch 104: 2.16795832
Test Loss after epoch 105: 2.17506267
Test Loss after epoch 106: 2.16354130
Test Loss after epoch 107: 2.16320579
Test Loss after epoch 108: 2.16480776
Test Loss after epoch 109: 2.15784145
Test Loss after epoch 110: 2.15400129
Test Loss after epoch 111: 2.15237321
Test Loss after epoch 112: 2.15464194
Test Loss after epoch 113: 2.15872598
Test Loss after epoch 114: 2.15925625
Test Loss after epoch 115: 2.16052086
Test Loss after epoch 116: 2.15466704
Test Loss after epoch 117: 2.15544540
Test Loss after epoch 118: 2.15487009
Test Loss after epoch 119: 2.15529750
Test Loss after epoch 120: 2.15596956
Test Loss after epoch 121: 2.15482595
Test Loss after epoch 122: 2.15657599
Test Loss after epoch 123: 2.15885844
Test Loss after epoch 124: 2.16333473
Test Loss after epoch 125: 2.16392031
Test Loss after epoch 126: 2.16099852
Test Loss after epoch 127: 2.16228801
Test Loss after epoch 128: 2.16714193
Test Loss after epoch 129: 2.17333346
Test Loss after epoch 130: 2.17114482
Test Loss after epoch 131: 2.17517735
Test Loss after epoch 132: 2.18372887
Test Loss after epoch 133: 2.18075939
Test Loss after epoch 134: 2.18123037
Test Loss after epoch 135: 2.17953631
Test Loss after epoch 136: 2.18058702
Test Loss after epoch 137: 2.17447610
Test Loss after epoch 138: 2.18237678
Test Loss after epoch 139: 2.18029826
Test Loss after epoch 140: 2.18193112
Test Loss after epoch 141: 2.17637819
Test Loss after epoch 142: 2.17582867
Test Loss after epoch 143: 2.17676639
Test Loss after epoch 144: 2.17845027
Test Loss after epoch 145: 2.17549262
Test Loss after epoch 146: 2.17253629
Test Loss after epoch 147: 2.17382454
Test Loss after epoch 148: 2.18105721
Test Loss after epoch 149: 2.18383190
Test Loss after epoch 150: 2.17854086
Test Loss after epoch 151: 2.17897324
Test Loss after epoch 152: 2.17905189
Test Loss after epoch 153: 2.17957504
Test Loss after epoch 154: 2.18074109
Test Loss after epoch 155: 2.17885315
Test Loss after epoch 156: 2.18035793
Test Loss after epoch 157: 2.17908199
Test Loss after epoch 158: 2.18150768
Test Loss after epoch 159: 2.17672793
Test Loss after epoch 160: 2.18138649
Test Loss after epoch 161: 2.17903077
Test Loss after epoch 162: 2.18618696
Test Loss after epoch 163: 2.18382706
Test Loss after epoch 164: 2.18324820
Test Loss after epoch 165: 2.18218892
Test Loss after epoch 166: 2.18140112
Test Loss after epoch 167: 2.18480751
Test Loss after epoch 168: 2.18378858
Test Loss after epoch 169: 2.19522837
Test Loss after epoch 170: 2.19037259
Test Loss after epoch 171: 2.19789021
Test Loss after epoch 172: 2.20254011
Test Loss after epoch 173: 2.20205867
Test Loss after epoch 174: 2.20301456
Test Loss after epoch 175: 2.19934995
Test Loss after epoch 176: 2.20296804
Test Loss after epoch 177: 2.20147087
Test Loss after epoch 178: 2.20260017
Test Loss after epoch 179: 2.20189961
Test Loss after epoch 180: 2.20340331
Test Loss after epoch 181: 2.20653690
Test Loss after epoch 182: 2.21392499
Test Loss after epoch 183: 2.21636047
Test Loss after epoch 184: 2.21846764
Test Loss after epoch 185: 2.21552680
Test Loss after epoch 186: 2.22738091
Test Loss after epoch 187: 2.22524521
Test Loss after epoch 188: 2.22490742
Test Loss after epoch 189: 2.23062173
Test Loss after epoch 190: 2.21466658
Test Loss after epoch 191: 2.22036454
Test Loss after epoch 192: 2.21938191
Test Loss after epoch 193: 2.22555688
Test Loss after epoch 194: 2.24238745
Test Loss after epoch 195: 2.24142096
Test Loss after epoch 196: 2.25325614
Test Loss after epoch 197: 2.26663871
Test Loss after epoch 198: 2.26487001
Test Loss after epoch 199: 2.25692114
