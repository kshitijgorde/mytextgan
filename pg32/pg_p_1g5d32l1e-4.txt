DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 150
NUM_D: 5
NUM_G: 1
LOG_LOCATION: ./logs/pg_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6/
SAVE_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6/pg_p_h32_l1e-2.chk
LOAD_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6/pg_p_h32_l1e-2.chkb
GAN_CHK_FOLDER: ./checkpoints/pg_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAVE_FILE_GAN: ./checkpoints/pg_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/pg_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
2017-04-07 09:05:29.716956: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 09:05:29.717016: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 09:05:29.717023: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 09:05:29.717028: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 09:05:29.717033: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 09:05:32.668772: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:86:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-07 09:05:32.668833: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-07 09:05:32.668846: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-07 09:05:32.668861: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:86:00.0)
WARNING:tensorflow:From model.py:246: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Valid g_pre_loss after loading model: 6.33040109
Test g_pre_loss after loading model: 6.28802313
2017-04-07 09:05:55.694707: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 12307 get requests, put_count=12255 evicted_count=1000 eviction_rate=0.0815993 and unsatisfied allocation rate=0.0936053
2017-04-07 09:05:55.694761: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-07 09:06:03.918774: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 7522 get requests, put_count=7557 evicted_count=1000 eviction_rate=0.132328 and unsatisfied allocation rate=0.131348
2017-04-07 09:06:03.918863: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Valid Loss after epoch 0: 6.33080275
Test Loss after epoch 0: 6.28797805
Valid Loss after epoch 1: 6.32986493
Test Loss after epoch 1: 6.28863173
Valid Loss after epoch 2: 6.33079466
Test Loss after epoch 2: 6.28916988
Valid Loss after epoch 3: 6.33092375
Test Loss after epoch 3: 6.28924561
Valid Loss after epoch 4: 6.33119079
Test Loss after epoch 4: 6.28872814
Valid Loss after epoch 5: 6.32986110
Test Loss after epoch 5: 6.28843752
Valid Loss after epoch 6: 6.32883912
Test Loss after epoch 6: 6.28944342
Valid Loss after epoch 7: 6.32959956
Test Loss after epoch 7: 6.28847768
Valid Loss after epoch 8: 6.33026516
Test Loss after epoch 8: 6.28890567
Valid Loss after epoch 9: 6.33001535
Test Loss after epoch 9: 6.29134086
Valid Loss after epoch 10: 6.33081566
Test Loss after epoch 10: 6.28996934
2017-04-07 09:17:38.185215: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 5177569 get requests, put_count=5177573 evicted_count=5000 eviction_rate=0.000965703 and unsatisfied allocation rate=0.000972657
Valid Loss after epoch 11: 6.33036394
Test Loss after epoch 11: 6.29084619
Valid Loss after epoch 12: 6.33180717
Test Loss after epoch 12: 6.28987340
Valid Loss after epoch 13: 6.33017412
Test Loss after epoch 13: 6.28939266
Valid Loss after epoch 14: 6.33013667
Test Loss after epoch 14: 6.28940735
Valid Loss after epoch 15: 6.33071740
Test Loss after epoch 15: 6.28884351
Valid Loss after epoch 16: 6.32946494
Test Loss after epoch 16: 6.28984807
Valid Loss after epoch 17: 6.33150159
Test Loss after epoch 17: 6.29115600
Valid Loss after epoch 18: 6.33179174
Test Loss after epoch 18: 6.29038829
Valid Loss after epoch 19: 6.33122112
Test Loss after epoch 19: 6.29096085
Valid Loss after epoch 20: 6.33115141
Test Loss after epoch 20: 6.29096919
Valid Loss after epoch 21: 6.33214550
Test Loss after epoch 21: 6.29086004
Valid Loss after epoch 22: 6.33211721
Test Loss after epoch 22: 6.29231901
Valid Loss after epoch 23: 6.33290899
Test Loss after epoch 23: 6.29158415
Valid Loss after epoch 24: 6.33483446
Test Loss after epoch 24: 6.29362891
Valid Loss after epoch 25: 6.33488360
Test Loss after epoch 25: 6.29398023
Valid Loss after epoch 26: 6.33409654
Test Loss after epoch 26: 6.29329072
Valid Loss after epoch 27: 6.33406512
Test Loss after epoch 27: 6.29386794
Valid Loss after epoch 28: 6.33411503
Test Loss after epoch 28: 6.29375682
Valid Loss after epoch 29: 6.33585617
Test Loss after epoch 29: 6.29331867
Valid Loss after epoch 30: 6.33444813
Test Loss after epoch 30: 6.29438446
Valid Loss after epoch 31: 6.33482853
Test Loss after epoch 31: 6.29565879
Valid Loss after epoch 32: 6.33536766
Test Loss after epoch 32: 6.29572559
Valid Loss after epoch 33: 6.33679862
Test Loss after epoch 33: 6.29547451
Valid Loss after epoch 34: 6.33818394
Test Loss after epoch 34: 6.29757543
Valid Loss after epoch 35: 6.33836795
Test Loss after epoch 35: 6.29834347
Valid Loss after epoch 36: 6.33878846
Test Loss after epoch 36: 6.29688626
2017-04-07 09:43:29.116903: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 17033372 get requests, put_count=17033376 evicted_count=15000 eviction_rate=0.000880624 and unsatisfied allocation rate=0.000882738
Valid Loss after epoch 37: 6.33931328
Test Loss after epoch 37: 6.29993746
Valid Loss after epoch 38: 6.33947244
Test Loss after epoch 38: 6.29994080
Valid Loss after epoch 39: 6.34009773
Test Loss after epoch 39: 6.29901900
Valid Loss after epoch 40: 6.34074250
Test Loss after epoch 40: 6.30003231
Valid Loss after epoch 41: 6.33964840
Test Loss after epoch 41: 6.29942514
Valid Loss after epoch 42: 6.34105919
Test Loss after epoch 42: 6.30014640
Valid Loss after epoch 43: 6.34336026
Test Loss after epoch 43: 6.30211796
Valid Loss after epoch 44: 6.34254900
Test Loss after epoch 44: 6.30335322
Valid Loss after epoch 45: 6.34373955
Test Loss after epoch 45: 6.30235458
Valid Loss after epoch 46: 6.34517512
Test Loss after epoch 46: 6.30364077
Valid Loss after epoch 47: 6.34631030
Test Loss after epoch 47: 6.30538545
Valid Loss after epoch 48: 6.34666052
Test Loss after epoch 48: 6.30735673
Valid Loss after epoch 49: 6.34967330
Test Loss after epoch 49: 6.30729677
Valid Loss after epoch 50: 6.35004863
Test Loss after epoch 50: 6.30874431
Valid Loss after epoch 51: 6.35076353
Test Loss after epoch 51: 6.30932816
Valid Loss after epoch 52: 6.35263580
Test Loss after epoch 52: 6.31280658
Valid Loss after epoch 53: 6.35278463
Test Loss after epoch 53: 6.31259792
Valid Loss after epoch 54: 6.35380131
Test Loss after epoch 54: 6.31326742
Valid Loss after epoch 55: 6.35633039
Test Loss after epoch 55: 6.31566425
Valid Loss after epoch 56: 6.35608779
Test Loss after epoch 56: 6.31657902
Valid Loss after epoch 57: 6.35751835
Test Loss after epoch 57: 6.31728952
Valid Loss after epoch 58: 6.35784715
Test Loss after epoch 58: 6.31784893
Valid Loss after epoch 59: 6.35966758
Test Loss after epoch 59: 6.31757092
2017-04-07 10:06:11.220815: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 27430950 get requests, put_count=27430953 evicted_count=25000 eviction_rate=0.000911379 and unsatisfied allocation rate=0.000912728
Valid Loss after epoch 60: 6.36172350
Test Loss after epoch 60: 6.32081275
Valid Loss after epoch 61: 6.36380970
Test Loss after epoch 61: 6.32218118
Valid Loss after epoch 62: 6.36683154
Test Loss after epoch 62: 6.32643652
Valid Loss after epoch 63: 6.36818875
Test Loss after epoch 63: 6.32797678
Valid Loss after epoch 64: 6.37014261
Test Loss after epoch 64: 6.32988891
Valid Loss after epoch 65: 6.37274857
Test Loss after epoch 65: 6.33342446
Valid Loss after epoch 66: 6.37496868
Test Loss after epoch 66: 6.33358046
Valid Loss after epoch 67: 6.37844458
Test Loss after epoch 67: 6.33789864
Valid Loss after epoch 68: 6.37955594
Test Loss after epoch 68: 6.33767922
Valid Loss after epoch 69: 6.38133757
Test Loss after epoch 69: 6.34084269
Valid Loss after epoch 70: 6.38547727
Test Loss after epoch 70: 6.34378704
Valid Loss after epoch 71: 6.38899106
Test Loss after epoch 71: 6.34828686
Valid Loss after epoch 72: 6.39154376
Test Loss after epoch 72: 6.35121055
Valid Loss after epoch 73: 6.39407362
Test Loss after epoch 73: 6.35384441
Valid Loss after epoch 74: 6.39818799
Test Loss after epoch 74: 6.35816649
Valid Loss after epoch 75: 6.40176785
Test Loss after epoch 75: 6.36085438
Valid Loss after epoch 76: 6.40714157
Test Loss after epoch 76: 6.36545769
Valid Loss after epoch 77: 6.40652370
Test Loss after epoch 77: 6.36585786
Valid Loss after epoch 78: 6.40919434
Test Loss after epoch 78: 6.37105223
Valid Loss after epoch 79: 6.41024317
Test Loss after epoch 79: 6.37005524
Valid Loss after epoch 80: 6.41095559
Test Loss after epoch 80: 6.36995306
Valid Loss after epoch 81: 6.41445255
Test Loss after epoch 81: 6.37379826
Valid Loss after epoch 82: 6.41498232
Test Loss after epoch 82: 6.37439743
2017-04-07 10:29:16.631971: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 38003592 get requests, put_count=38003550 evicted_count=35000 eviction_rate=0.000920967 and unsatisfied allocation rate=0.000923123
Valid Loss after epoch 83: 6.41670483
Test Loss after epoch 83: 6.37638970
Valid Loss after epoch 84: 6.42158829
Test Loss after epoch 84: 6.38035605
Valid Loss after epoch 85: 6.42277348
Test Loss after epoch 85: 6.38401687
Valid Loss after epoch 86: 6.42827736
Test Loss after epoch 86: 6.38841421
Valid Loss after epoch 87: 6.43237616
Test Loss after epoch 87: 6.39148668
Valid Loss after epoch 88: 6.43595065
Test Loss after epoch 88: 6.39699618
Valid Loss after epoch 89: 6.44068870
Test Loss after epoch 89: 6.39965737
Valid Loss after epoch 90: 6.44613411
Test Loss after epoch 90: 6.40397547
Valid Loss after epoch 91: 6.45076874
Test Loss after epoch 91: 6.40939396
Valid Loss after epoch 92: 6.46310207
Test Loss after epoch 92: 6.42216272
Valid Loss after epoch 93: 6.46684088
Test Loss after epoch 93: 6.42546335
Valid Loss after epoch 94: 6.47004601
Test Loss after epoch 94: 6.42861409
Valid Loss after epoch 95: 6.47241136
Test Loss after epoch 95: 6.43010976
Valid Loss after epoch 96: 6.47535855
Test Loss after epoch 96: 6.43347866
Valid Loss after epoch 97: 6.47348616
Test Loss after epoch 97: 6.43046511
Valid Loss after epoch 98: 6.47527647
Test Loss after epoch 98: 6.43362933
Valid Loss after epoch 99: 6.47863473
Test Loss after epoch 99: 6.43574756
Valid Loss after epoch 100: 6.47535996
Test Loss after epoch 100: 6.43435898
Valid Loss after epoch 101: 6.47863284
Test Loss after epoch 101: 6.43677328
Valid Loss after epoch 102: 6.48064882
Test Loss after epoch 102: 6.44035256
Valid Loss after epoch 103: 6.48477527
Test Loss after epoch 103: 6.44311637
Valid Loss after epoch 104: 6.48811474
Test Loss after epoch 104: 6.44495055
Valid Loss after epoch 105: 6.48782770
Test Loss after epoch 105: 6.44705363
Valid Loss after epoch 106: 6.49269979
Test Loss after epoch 106: 6.45088733
2017-04-07 10:52:44.721503: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 48770061 get requests, put_count=48770065 evicted_count=45000 eviction_rate=0.000922697 and unsatisfied allocation rate=0.000923435
Valid Loss after epoch 107: 6.49563138
Test Loss after epoch 107: 6.45330180
Valid Loss after epoch 108: 6.49690090
Test Loss after epoch 108: 6.45419276
Valid Loss after epoch 109: 6.50253528
Test Loss after epoch 109: 6.46041692
Valid Loss after epoch 110: 6.50532927
Test Loss after epoch 110: 6.46475701
Valid Loss after epoch 111: 6.50622512
Test Loss after epoch 111: 6.46515148
Valid Loss after epoch 112: 6.51558965
Test Loss after epoch 112: 6.47483309
Valid Loss after epoch 113: 6.51814194
Test Loss after epoch 113: 6.47696012
Valid Loss after epoch 114: 6.51893832
Test Loss after epoch 114: 6.47897496
Valid Loss after epoch 115: 6.52063772
Test Loss after epoch 115: 6.47907945
Valid Loss after epoch 116: 6.51913875
Test Loss after epoch 116: 6.47837856
Valid Loss after epoch 117: 6.51811172
Test Loss after epoch 117: 6.47750928
Valid Loss after epoch 118: 6.52015349
Test Loss after epoch 118: 6.48006942
Valid Loss after epoch 119: 6.53295915
Test Loss after epoch 119: 6.49428314
Valid Loss after epoch 120: 6.53702787
Test Loss after epoch 120: 6.49773953
Valid Loss after epoch 121: 6.53837349
Test Loss after epoch 121: 6.49852041
Valid Loss after epoch 122: 6.54300879
Test Loss after epoch 122: 6.50287845
Valid Loss after epoch 123: 6.54818547
Test Loss after epoch 123: 6.50811423
Valid Loss after epoch 124: 6.55151452
Test Loss after epoch 124: 6.51058683
Valid Loss after epoch 125: 6.55188717
Test Loss after epoch 125: 6.51082034
Valid Loss after epoch 126: 6.55487679
Test Loss after epoch 126: 6.51544728
Valid Loss after epoch 127: 6.55613548
Test Loss after epoch 127: 6.51608349
Valid Loss after epoch 128: 6.55798274
Test Loss after epoch 128: 6.51714094
Valid Loss after epoch 129: 6.55630551
Test Loss after epoch 129: 6.51639848
Valid Loss after epoch 130: 6.55925737
Test Loss after epoch 130: 6.51874585
Valid Loss after epoch 131: 6.56232692
Test Loss after epoch 131: 6.52304519
2017-04-07 11:16:58.641571: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 59877852 get requests, put_count=59877856 evicted_count=55000 eviction_rate=0.000918537 and unsatisfied allocation rate=0.000919138
Valid Loss after epoch 132: 6.56330436
Test Loss after epoch 132: 6.52177468
Valid Loss after epoch 133: 6.56638700
Test Loss after epoch 133: 6.52728059
Valid Loss after epoch 134: 6.57122076
Test Loss after epoch 134: 6.53126386
Valid Loss after epoch 135: 6.57481863
Test Loss after epoch 135: 6.53601140
Valid Loss after epoch 136: 6.58094047
Test Loss after epoch 136: 6.53995695
Valid Loss after epoch 137: 6.58134706
Test Loss after epoch 137: 6.54339524
Valid Loss after epoch 138: 6.59833427
Test Loss after epoch 138: 6.55747480
Valid Loss after epoch 139: 6.60036145
Test Loss after epoch 139: 6.55980156
Valid Loss after epoch 140: 6.60380989
Test Loss after epoch 140: 6.56432867
Valid Loss after epoch 141: 6.60814331
Test Loss after epoch 141: 6.56997059
Valid Loss after epoch 142: 6.61401409
Test Loss after epoch 142: 6.57473707
Valid Loss after epoch 143: 6.61634399
Test Loss after epoch 143: 6.57690940
Valid Loss after epoch 144: 6.62222010
Test Loss after epoch 144: 6.58187523
Valid Loss after epoch 145: 6.62752038
Test Loss after epoch 145: 6.58742154
Valid Loss after epoch 146: 6.63181406
Test Loss after epoch 146: 6.59432460
Valid Loss after epoch 147: 6.63850188
Test Loss after epoch 147: 6.60047030
Valid Loss after epoch 148: 6.64803734
Test Loss after epoch 148: 6.60881075
Valid Loss after epoch 149: 6.64918590
Test Loss after epoch 149: 6.61115986
