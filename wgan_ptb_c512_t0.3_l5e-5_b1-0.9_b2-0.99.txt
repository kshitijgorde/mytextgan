nohup: ignoring input
DATASET: ptb
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 0
LEARNING_RATE_PRE_G: 0.001
HIDDEN_STATE_SIZE: 512
HIDDEN_STATE_SIZE_D: 512
LEARNING_RATE_G: 5e-05
LEARNING_RATE_D: 5e-05
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.9
BETA1_D: 0.9
BETA2_G: 0.99
BETA2_D: 0.99
LOG_LOCATION: ./logs/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l5e-5/
SAMPLES_FOLDER: samples/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l5e-5/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l5e-5/
SAVE_FILE_GAN: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l5e-5/chk
LOAD_FILE_GAN: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l5e-5/chk
TEMPERATURE: 0.3
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 99, 49)
(32, 99, 49)
2017-04-10 00:26:11.447722: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-10 00:26:11.447765: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-10 00:26:11.447773: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-10 00:26:11.447778: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-10 00:26:11.447783: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-10 00:26:16.777175: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:85:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-10 00:26:16.777230: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-10 00:26:16.777242: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-10 00:26:16.777257: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:85:00.0)
WARNING:tensorflow:From model.py:289: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
2017-04-10 00:29:00.408806: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6534 get requests, put_count=6528 evicted_count=1000 eviction_rate=0.153186 and unsatisfied allocation rate=0.169268
2017-04-10 00:29:00.408857: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
Test g_pre_loss before training: 3.94317824
Calculated in 453.372s
2017-04-10 00:36:36.170965: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2762071 get requests, put_count=2761855 evicted_count=3000 eviction_rate=0.00108623 and unsatisfied allocation rate=0.00117122
2017-04-10 00:36:43.823614: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 791 get requests, put_count=1446 evicted_count=1000 eviction_rate=0.691563 and unsatisfied allocation rate=0.482933
2017-04-10 00:36:43.823667: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 409 to 449
2017-04-10 00:36:53.968458: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6535 get requests, put_count=6633 evicted_count=1000 eviction_rate=0.150761 and unsatisfied allocation rate=0.152563
2017-04-10 00:36:53.968537: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 1053 to 1158
Test Loss after epoch 0: 3.15265640
Test Loss after epoch 1: 3.16888886
Test Loss after epoch 2: 3.13484677
Test Loss after epoch 3: 3.08490772
Test Loss after epoch 4: 3.06718222
Test Loss after epoch 5: 3.03114342
Test Loss after epoch 6: 3.01252433
Test Loss after epoch 7: 3.00279554
Test Loss after epoch 8: 2.99317625
Test Loss after epoch 9: 3.01706088
Test Loss after epoch 10: 2.97911617
Test Loss after epoch 11: 2.99769495
Test Loss after epoch 12: 2.97235712
Test Loss after epoch 13: 2.97613777
Test Loss after epoch 14: 2.97264063
Test Loss after epoch 15: 2.97444169
Test Loss after epoch 16: 2.98156803
Test Loss after epoch 17: 2.98267537
Test Loss after epoch 18: 2.99719437
Test Loss after epoch 19: 2.97997113
Test Loss after epoch 20: 2.98436332
Test Loss after epoch 21: 2.99302011
Test Loss after epoch 22: 2.98092185
Test Loss after epoch 23: 2.99106672
Test Loss after epoch 24: 2.97792685
Test Loss after epoch 25: 3.05900298
2017-04-10 11:11:09.536722: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 161766505 get requests, put_count=161766486 evicted_count=8000 eviction_rate=4.9454e-05 and unsatisfied allocation rate=5.03565e-05
Test Loss after epoch 26: 3.00763061
Test Loss after epoch 27: 3.01899200
Test Loss after epoch 28: 3.00201730
Test Loss after epoch 29: 3.01907761
Test Loss after epoch 30: 3.00897115
Test Loss after epoch 31: 3.00378553
Test Loss after epoch 32: 2.98943921
Test Loss after epoch 33: 2.98895520
Test Loss after epoch 34: 3.02114872
Test Loss after epoch 35: 3.02457870
Test Loss after epoch 36: 3.01466724
Test Loss after epoch 37: 3.01123951
Test Loss after epoch 38: 3.05492534
Test Loss after epoch 39: 3.03610910
Test Loss after epoch 40: 2.97705488
Test Loss after epoch 41: 3.00982159
Test Loss after epoch 42: 2.99524852
Test Loss after epoch 43: 2.99745821
Test Loss after epoch 44: 3.00307103
Test Loss after epoch 45: 3.05653170
Test Loss after epoch 46: 3.00224004
Test Loss after epoch 47: 2.99634740
Test Loss after epoch 48: 3.01576537
Test Loss after epoch 49: 3.02387864
Test Loss after epoch 50: 3.00566606
Test Loss after epoch 51: 3.06754681
Test Loss after epoch 52: 3.04246557
Test Loss after epoch 53: 3.04407561
Test Loss after epoch 54: 3.11264349
Test Loss after epoch 55: 3.12873565
Test Loss after epoch 56: 3.02695346
2017-04-11 00:00:25.182425: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 358030746 get requests, put_count=358030747 evicted_count=18000 eviction_rate=5.0275e-05 and unsatisfied allocation rate=5.06269e-05
Test Loss after epoch 57: 3.02358592
Test Loss after epoch 58: 3.04437532
Test Loss after epoch 59: 3.08282852
