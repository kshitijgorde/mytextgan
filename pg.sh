#!/usr/bin/env bash
if [ $CUDA_VISIBLE_DEVICES -eq 0 ]; then
	lr=5e-3; nd=5
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
	lr=5e-3; nd=3
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
	lr=5e-3; nd=7
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
elif [ $CUDA_VISIBLE_DEVICES -eq 1 ]; then
	lr=1e-3; nd=5
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
	lr=1e-3; nd=3
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
	lr=1e-3; nd=7
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
elif [ $CUDA_VISIBLE_DEVICES -eq 2 ]; then
	lr=5e-4; nd=5
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
	lr=5e-4; nd=3
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
	lr=5e-4; nd=7
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
elif [ $CUDA_VISIBLE_DEVICES -eq 3 ]; then
	lr=1e-4; nd=5
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
	lr=1e-4; nd=3
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
	lr=1e-4; nd=7
	python -u model.py --lr_g=$lr --lr_d=$lr --num_d=$nd --num_g=1 --dataset=pg --pretrain_epochs=6 --lr_pretrain=1e-2 --epochs=150 > pg32/pg_p_1g${nd}d32l${lr}.txt 2>&1
fi