nohup: ignoring input
DATASET: ptb
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 0
LEARNING_RATE_PRE_G: 0.001
HIDDEN_STATE_SIZE: 512
HIDDEN_STATE_SIZE_D: 512
LEARNING_RATE_G: 0.0005
LEARNING_RATE_D: 0.0005
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.9
BETA1_D: 0.9
BETA2_G: 0.99
BETA2_D: 0.99
LOG_LOCATION: ./logs/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l5e-4/
SAMPLES_FOLDER: samples/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l5e-4/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l5e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l5e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspt_t0.3_b1-0.9_b2-0.99_g1d5_g512_d512_pe0_pl1e-3_l5e-4/chk
TEMPERATURE: 0.3
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 99, 49)
(32, 99, 49)
2017-04-09 08:27:06.057083: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-09 08:27:06.057132: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-09 08:27:06.057139: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-09 08:27:06.057156: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-09 08:27:06.057161: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-09 08:27:11.617007: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:85:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-09 08:27:11.617045: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-09 08:27:11.617052: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-09 08:27:11.617060: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:85:00.0)
WARNING:tensorflow:From model.py:289: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
2017-04-09 08:29:54.032703: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6546 get requests, put_count=6524 evicted_count=1000 eviction_rate=0.15328 and unsatisfied allocation rate=0.171402
2017-04-09 08:29:54.032787: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
Test g_pre_loss before training: 3.89404880
Calculated in 467.862s
2017-04-09 08:37:44.085838: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2766893 get requests, put_count=2766730 evicted_count=3000 eviction_rate=0.00108431 and unsatisfied allocation rate=0.00115003
2017-04-09 08:37:51.839426: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 791 get requests, put_count=1446 evicted_count=1000 eviction_rate=0.691563 and unsatisfied allocation rate=0.482933
2017-04-09 08:37:51.839484: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 409 to 449
2017-04-09 08:38:02.260766: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6536 get requests, put_count=6633 evicted_count=1000 eviction_rate=0.150761 and unsatisfied allocation rate=0.152693
2017-04-09 08:38:02.260851: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 1053 to 1158
Test Loss after epoch 0: 4.27256211
Test Loss after epoch 1: 3.02415015
Test Loss after epoch 2: 3.01388920
Test Loss after epoch 3: 3.52041803
Test Loss after epoch 4: 3.04388068
Test Loss after epoch 5: 3.97818651
Test Loss after epoch 6: 3.08429129
Test Loss after epoch 7: 3.00528885
Test Loss after epoch 8: 4.27973573
Test Loss after epoch 9: 3.10940935
Test Loss after epoch 10: 3.02393866
Test Loss after epoch 11: 3.17455313
Test Loss after epoch 12: 3.15822989
Test Loss after epoch 13: 3.16253536
Test Loss after epoch 14: 3.10231678
Test Loss after epoch 15: 3.19667355
Test Loss after epoch 16: 3.38061823
Test Loss after epoch 17: 3.39465270
Test Loss after epoch 18: 3.46084438
Test Loss after epoch 19: 3.49298129
Test Loss after epoch 20: 3.69176070
Test Loss after epoch 21: 3.71391799
Test Loss after epoch 22: 3.89062980
Test Loss after epoch 23: 4.03016439
Test Loss after epoch 24: 4.23652547
Test Loss after epoch 25: 4.06578414
2017-04-09 19:25:24.349505: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 161766079 get requests, put_count=161766150 evicted_count=8000 eviction_rate=4.94541e-05 and unsatisfied allocation rate=4.98003e-05
Test Loss after epoch 26: 3.79323592
Test Loss after epoch 27: 4.04601922
Test Loss after epoch 28: 3.61212253
Test Loss after epoch 29: 3.35897239
Test Loss after epoch 30: 3.63097047
Test Loss after epoch 31: 6.77483816
Test Loss after epoch 32: 3.91656058
Test Loss after epoch 33: 3.66880312
Test Loss after epoch 34: 3.63009382
Test Loss after epoch 35: 3.73716722
