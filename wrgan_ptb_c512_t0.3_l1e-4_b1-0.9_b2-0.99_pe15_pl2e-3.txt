nohup: ignoring input
DATASET: ptb
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 15
LEARNING_RATE_PRE_G: 0.002
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.9
BETA1_D: 0.9
BETA2_G: 0.99
BETA2_D: 0.99
LOG_LOCATION: ./logs/RWasspt_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe15_pl2e-3_l1e-4/
SAMPLES_FOLDER: samples/RWasspt_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe15_pl2e-3_l1e-4/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/RWasspt_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe15_pl2e-3_l1e-4/
SAVE_FILE_GAN: ./checkpoints/RWasspt_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe15_pl2e-3_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/RWasspt_t0.2_b1-0.9_b2-0.99_g1d5_g32_d32_pe15_pl2e-3_l1e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 99, 49)
(32, 99, 49)
2017-04-20 08:58:18.799126: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-20 08:58:18.799178: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-20 08:58:18.799185: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-20 08:58:18.799189: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-20 08:58:18.799194: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-20 08:58:24.281010: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:85:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-20 08:58:24.281068: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-20 08:58:24.281089: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-20 08:58:24.281101: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:85:00.0)
WARNING:tensorflow:From model.py:292: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
2017-04-20 09:01:38.681161: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 6546 get requests, put_count=6544 evicted_count=1000 eviction_rate=0.152812 and unsatisfied allocation rate=0.168347
2017-04-20 09:01:38.681206: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
Test g_pre_loss before training: 3.86625400
Calculated in 442.057s
2017-04-20 09:08:56.932397: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2770881 get requests, put_count=2770551 evicted_count=3000 eviction_rate=0.00108282 and unsatisfied allocation rate=0.00120864
2017-04-20 09:08:58.006421: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2373 get requests, put_count=1883 evicted_count=1000 eviction_rate=0.531067 and unsatisfied allocation rate=0.640539
2017-04-20 09:08:58.006473: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 339 to 372
2017-04-20 09:08:59.053125: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2373 get requests, put_count=2317 evicted_count=1000 eviction_rate=0.431593 and unsatisfied allocation rate=0.472398
2017-04-20 09:08:59.053188: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 720 to 792
Train Loss after pre-train epoch 0: 2.35973098
Test Loss after pre-train epoch 0: 2.10486608
Train Loss after pre-train epoch 1: 2.04628849
Test Loss after pre-train epoch 1: 1.97155268
Train Loss after pre-train epoch 2: 1.93099177
Test Loss after pre-train epoch 2: 1.87430277
Train Loss after pre-train epoch 3: 1.85104291
Test Loss after pre-train epoch 3: 1.80935990
Train Loss after pre-train epoch 4: 1.80034065
Test Loss after pre-train epoch 4: 1.76734153
Train Loss after pre-train epoch 5: 1.76599081
Test Loss after pre-train epoch 5: 1.73642180
Train Loss after pre-train epoch 6: 1.74135486
Test Loss after pre-train epoch 6: 1.71372510
Train Loss after pre-train epoch 7: 1.72288300
Test Loss after pre-train epoch 7: 1.69774878
Train Loss after pre-train epoch 8: 1.70799939
Test Loss after pre-train epoch 8: 1.68500953
Train Loss after pre-train epoch 9: 1.69672741
Test Loss after pre-train epoch 9: 1.67507799
Train Loss after pre-train epoch 10: 1.68744898
Test Loss after pre-train epoch 10: 1.66638808
Train Loss after pre-train epoch 11: 1.67985453
Test Loss after pre-train epoch 11: 1.66088487
Train Loss after pre-train epoch 12: 1.67312865
Test Loss after pre-train epoch 12: 1.65380277
Train Loss after pre-train epoch 13: 1.66662653
Test Loss after pre-train epoch 13: 1.64810791
Train Loss after pre-train epoch 14: 1.66123160
Test Loss after pre-train epoch 14: 1.64221628
2017-04-20 11:55:56.561087: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 97701846 get requests, put_count=97699852 evicted_count=1000 eviction_rate=1.02354e-05 and unsatisfied allocation rate=3.2374e-05
2017-04-20 11:56:03.881300: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 97772644 get requests, put_count=97772318 evicted_count=11000 eviction_rate=0.000112506 and unsatisfied allocation rate=0.000117569
2017-04-20 11:56:12.130028: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 97858491 get requests, put_count=97858134 evicted_count=21000 eviction_rate=0.000214596 and unsatisfied allocation rate=0.000219971
2017-04-20 11:56:21.462892: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 97948003 get requests, put_count=97947880 evicted_count=31000 eviction_rate=0.000316495 and unsatisfied allocation rate=0.000319476
2017-04-20 11:56:32.346295: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98059900 get requests, put_count=98057877 evicted_count=41000 eviction_rate=0.00041812 and unsatisfied allocation rate=0.000440465
2017-04-20 11:56:40.989460: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98149411 get requests, put_count=98147843 evicted_count=51000 eviction_rate=0.000519624 and unsatisfied allocation rate=0.000537313
2017-04-20 11:56:50.393049: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98238925 get requests, put_count=98237303 evicted_count=61000 eviction_rate=0.000620945 and unsatisfied allocation rate=0.000639166
2017-04-20 11:56:57.007387: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98309728 get requests, put_count=98309666 evicted_count=71000 eviction_rate=0.000722208 and unsatisfied allocation rate=0.000724557
2017-04-20 11:57:05.354092: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98395575 get requests, put_count=98395502 evicted_count=81000 eviction_rate=0.000823208 and unsatisfied allocation rate=0.000825667
2017-04-20 11:57:14.659893: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98485088 get requests, put_count=98484691 evicted_count=91000 eviction_rate=0.000924001 and unsatisfied allocation rate=0.000929745
2017-04-20 11:57:23.415281: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98574600 get requests, put_count=98574224 evicted_count=101000 eviction_rate=0.00102461 and unsatisfied allocation rate=0.00103013
2017-04-20 11:57:32.609731: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98664110 get requests, put_count=98663882 evicted_count=111000 eviction_rate=0.00112503 and unsatisfied allocation rate=0.00112905
2017-04-20 11:57:41.056161: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98753622 get requests, put_count=98753641 evicted_count=121000 eviction_rate=0.00122527 and unsatisfied allocation rate=0.00122679
2017-04-20 11:57:51.752990: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98865512 get requests, put_count=98864405 evicted_count=131000 eviction_rate=0.00132505 and unsatisfied allocation rate=0.00133794
2017-04-20 11:58:00.244809: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 98955022 get requests, put_count=98954172 evicted_count=141000 eviction_rate=0.0014249 and unsatisfied allocation rate=0.00143519
2017-04-20 11:58:08.601108: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 99044541 get requests, put_count=99043621 evicted_count=151000 eviction_rate=0.00152458 and unsatisfied allocation rate=0.00153556
2017-04-20 11:58:17.049095: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 99137715 get requests, put_count=99137627 evicted_count=161000 eviction_rate=0.001624 and unsatisfied allocation rate=0.0016266
2017-04-20 11:58:25.448224: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 99223564 get requests, put_count=99222912 evicted_count=171000 eviction_rate=0.00172339 and unsatisfied allocation rate=0.00173166
2017-04-20 11:58:33.801118: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 99313074 get requests, put_count=99312697 evicted_count=181000 eviction_rate=0.00182253 and unsatisfied allocation rate=0.00182802
2017-04-20 11:58:42.191330: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 99402590 get requests, put_count=99402094 evicted_count=191000 eviction_rate=0.00192149 and unsatisfied allocation rate=0.00192817
2017-04-20 11:58:52.614781: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 22380 get requests, put_count=22377 evicted_count=2000 eviction_rate=0.0893775 and unsatisfied allocation rate=0.0978105
2017-04-20 11:58:52.614853: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 2049 to 2253
Test Loss after epoch 0: 1.64270402
Test Loss after epoch 1: 1.64277037
Test Loss after epoch 2: 1.64279427
Test Loss after epoch 3: 1.64277738
Test Loss after epoch 4: 1.64272589
Test Loss after epoch 5: 1.64335894
Test Loss after epoch 6: 1.64392977
Test Loss after epoch 7: 1.64442149
Test Loss after epoch 8: 1.64485287
Test Loss after epoch 9: 1.64469347
Test Loss after epoch 10: 1.64500083
Test Loss after epoch 11: 1.64608611
Test Loss after epoch 12: 1.64724903
Test Loss after epoch 13: 1.64705796
Test Loss after epoch 14: 1.64723881
Test Loss after epoch 15: 1.64769348
Test Loss after epoch 16: 1.64752050
Test Loss after epoch 17: 1.64756363
Test Loss after epoch 18: 1.64782038
Test Loss after epoch 19: 1.64818786
Test Loss after epoch 20: 1.64862298
Test Loss after epoch 21: 1.64872647
Test Loss after epoch 22: 1.64977354
Test Loss after epoch 23: 1.64959931
Test Loss after epoch 24: 1.65111189
Test Loss after epoch 25: 1.65136134
Test Loss after epoch 26: 1.65205211
Test Loss after epoch 27: 1.65228795
Test Loss after epoch 28: 1.65229895
Test Loss after epoch 29: 1.65225598
Test Loss after epoch 30: 1.65225070
Test Loss after epoch 31: 1.65224827
Test Loss after epoch 32: 1.65223068
Test Loss after epoch 33: 1.65234001
Test Loss after epoch 34: 1.65307990
Test Loss after epoch 35: 1.65373251
Test Loss after epoch 36: 1.65318945
Test Loss after epoch 37: 1.65312567
Test Loss after epoch 38: 1.65381320
Test Loss after epoch 39: 1.65404519
Test Loss after epoch 40: 1.65388609
Test Loss after epoch 41: 1.65386743
Test Loss after epoch 42: 1.65650737
Test Loss after epoch 43: 1.65685353
Test Loss after epoch 44: 1.65679944
Test Loss after epoch 45: 1.65747735
Test Loss after epoch 46: 1.65788790
Test Loss after epoch 47: 1.65704209
Test Loss after epoch 48: 1.65719384
Test Loss after epoch 49: 1.65696314
Test Loss after epoch 50: 1.65703526
Test Loss after epoch 51: 1.65616877
Test Loss after epoch 52: 1.65820847
Test Loss after epoch 53: 1.65882823
Test Loss after epoch 54: 1.65865513
Test Loss after epoch 55: 1.66008876
Test Loss after epoch 56: 1.66008772
Test Loss after epoch 57: 1.65979323
Test Loss after epoch 58: 1.66119151
Test Loss after epoch 59: 1.66132147
Test Loss after epoch 60: 1.66129926
Test Loss after epoch 61: 1.66070559
Test Loss after epoch 62: 1.66097144
Test Loss after epoch 63: 1.66162207
Test Loss after epoch 64: 1.66445318
Test Loss after epoch 65: 1.66387070
Test Loss after epoch 66: 1.66470505
Test Loss after epoch 67: 1.66450226
Test Loss after epoch 68: 1.66442286
Test Loss after epoch 69: 1.66607427
Test Loss after epoch 70: 1.66587423
Test Loss after epoch 71: 1.66596543
Test Loss after epoch 72: 1.66551192
Test Loss after epoch 73: 1.66521515
Test Loss after epoch 74: 1.66450207
Test Loss after epoch 75: 1.66556131
Test Loss after epoch 76: 1.66572596
Test Loss after epoch 77: 1.66588833
Test Loss after epoch 78: 1.66581646
Test Loss after epoch 79: 1.66625075
Test Loss after epoch 80: 1.66569413
Test Loss after epoch 81: 1.66817561
Test Loss after epoch 82: 1.66818107
Test Loss after epoch 83: 1.66851998
Test Loss after epoch 84: 1.66668312
Test Loss after epoch 85: 1.66700665
Test Loss after epoch 86: 1.66731925
Test Loss after epoch 87: 1.66797300
Test Loss after epoch 88: 1.66886895
Test Loss after epoch 89: 1.66620806
Test Loss after epoch 90: 1.66748866
Test Loss after epoch 91: 1.66880863
Test Loss after epoch 92: 1.66938064
Test Loss after epoch 93: 1.66820600
Test Loss after epoch 94: 1.66677439
Test Loss after epoch 95: 1.66673628
Test Loss after epoch 96: 1.66639181
Test Loss after epoch 97: 1.66631468
Test Loss after epoch 98: 1.66595267
Test Loss after epoch 99: 1.66481195
Test Loss after epoch 100: 1.66494704
Test Loss after epoch 101: 1.66550259
Test Loss after epoch 102: 1.66686774
Test Loss after epoch 103: 1.66725058
Test Loss after epoch 104: 1.66698631
Test Loss after epoch 105: 1.66708247
Test Loss after epoch 106: 1.66938922
Test Loss after epoch 107: 1.66905410
Test Loss after epoch 108: 1.66981378
Test Loss after epoch 109: 1.67063954
Test Loss after epoch 110: 1.67101154
Test Loss after epoch 111: 1.66929458
Test Loss after epoch 112: 1.67183921
Test Loss after epoch 113: 1.66898245
Test Loss after epoch 114: 1.66988506
Test Loss after epoch 115: 1.67035276
Test Loss after epoch 116: 1.66947200
Test Loss after epoch 117: 1.67161844
Test Loss after epoch 118: 1.67192645
Test Loss after epoch 119: 1.67713515
Test Loss after epoch 120: 1.67702656
Test Loss after epoch 121: 1.68275972
Test Loss after epoch 122: 1.68327241
Test Loss after epoch 123: 1.68346022
Test Loss after epoch 124: 1.68124645
Test Loss after epoch 125: 1.68497327
Test Loss after epoch 126: 1.68454468
Test Loss after epoch 127: 1.68524184
Test Loss after epoch 128: 1.68311194
Test Loss after epoch 129: 1.68317289
Test Loss after epoch 130: 1.68350504
Test Loss after epoch 131: 1.68079020
Test Loss after epoch 132: 1.68116101
Test Loss after epoch 133: 1.68134922
Test Loss after epoch 134: 1.68254779
Test Loss after epoch 135: 1.68208923
Test Loss after epoch 136: 1.68178515
Test Loss after epoch 137: 1.68151912
Test Loss after epoch 138: 1.67631685
Test Loss after epoch 139: 1.67565694
Test Loss after epoch 140: 1.67627369
Test Loss after epoch 141: 1.67536122
Test Loss after epoch 142: 1.67627410
Test Loss after epoch 143: 1.67617598
Test Loss after epoch 144: 1.67653570
Test Loss after epoch 145: 1.67903386
Test Loss after epoch 146: 1.67889929
Test Loss after epoch 147: 1.68066009
Test Loss after epoch 148: 1.67671357
Test Loss after epoch 149: 1.67734533
Test Loss after epoch 150: 1.68011524
Test Loss after epoch 151: 1.68016197
Test Loss after epoch 152: 1.68067994
Test Loss after epoch 153: 1.67999340
Test Loss after epoch 154: 1.68003347
Test Loss after epoch 155: 1.67975086
Test Loss after epoch 156: 1.67946301
Test Loss after epoch 157: 1.67940930
Test Loss after epoch 158: 1.67970734
Test Loss after epoch 159: 1.68023710
Test Loss after epoch 160: 1.68029663
Test Loss after epoch 161: 1.68051784
Test Loss after epoch 162: 1.68011815
Test Loss after epoch 163: 1.67969589
Test Loss after epoch 164: 1.67969407
Test Loss after epoch 165: 1.67600309
Test Loss after epoch 166: 1.67606350
Test Loss after epoch 167: 1.67749586
Test Loss after epoch 168: 1.67635037
Test Loss after epoch 169: 1.67746470
Test Loss after epoch 170: 1.67745544
Test Loss after epoch 171: 1.67568630
Test Loss after epoch 172: 1.67440258
Test Loss after epoch 173: 1.67935278
Test Loss after epoch 174: 1.67990770
Test Loss after epoch 175: 1.67890019
Test Loss after epoch 176: 1.67877067
Test Loss after epoch 177: 1.68076396
Test Loss after epoch 178: 1.68245058
Test Loss after epoch 179: 1.68240333
Test Loss after epoch 180: 1.68252818
Test Loss after epoch 181: 1.68247757
Test Loss after epoch 182: 1.68313830
Test Loss after epoch 183: 1.68281982
Test Loss after epoch 184: 1.68296683
Test Loss after epoch 185: 1.68361736
Test Loss after epoch 186: 1.68666741
Test Loss after epoch 187: 1.68667802
Test Loss after epoch 188: 1.69031812
Test Loss after epoch 189: 1.69091808
Test Loss after epoch 190: 1.69086706
Test Loss after epoch 191: 1.69076940
Test Loss after epoch 192: 1.69056795
Test Loss after epoch 193: 1.69045887
Test Loss after epoch 194: 1.69345642
Test Loss after epoch 195: 1.69498814
Test Loss after epoch 196: 1.69095916
Test Loss after epoch 197: 1.69028168
Test Loss after epoch 198: 1.69016131
Test Loss after epoch 199: 1.69109771
