# Dataset related
DATASET = 'oracle'
FIRST_CHAR = 'start'

# Misc params
BATCH_SIZE = 100

# Pre-training
PRETRAIN_EPOCHS = 0
LEARNING_RATE_PRE_G = 0.001

# GAN
HIDDEN_STATE_SIZE = 64
HIDDEN_STATE_SIZE_D = HIDDEN_STATE_SIZE

LEARNING_RATE_G = 1e-4
LEARNING_RATE_D = 1e-4

BETA1_G = 0.5
BETA1_D = 0.5
BETA2_G = 0.9
BETA2_D = 0.9

N_EPOCHS = 1#200

NUM_D = 1
NUM_G = 3

TEMPERATURE = 0.1

######################## No configuration required ##############################
# Misc params
N_D_CLASSES = 2

# Dataset related
PG_VOCAB_SIZE = 5369
PG_SEQ_LENGTH = 21

PTB_VOCAB_SIZE = 0 # These are char level
PTB_SEQ_LENGTH = 0 # These are char level

ORACLE_VOCAB_SIZE = 5000
ORACLE_SEQ_LENGTH = 21
ORACLE_TEST_SIZE = 9984

# sanity checks
DATASET_LIST = ['ptb', 'pg', 'oracle']
assert DATASET in DATASET_LIST
# assert ORACLE_TEST_SIZE%BATCH_SIZE == 0
assert NUM_G==1  or NUM_D==1

TEST_BATCH_SIZE = -1 # defined later

LAMBDA = 10 # Gradient penalty lambda hyperparameter.
LOAD_FILE_PRETRAIN = False
