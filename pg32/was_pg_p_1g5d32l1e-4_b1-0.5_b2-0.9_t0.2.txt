DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.5
BETA1_D: 0.5
BETA2_G: 0.9
BETA2_D: 0.9
LOG_LOCATION: ./logs/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAMPLES_FOLDER: samples/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6_was2/
SAVE_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6_was2/pg_p_h32_l1e-2.chk
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5369)
(32, 20, 5369)
Test g_pre_loss before training: 8.58900947
Calculated in 3.576s
Train Loss after pre-train epoch 0: 7.22269136
Test Loss after pre-train epoch 0: 6.89433381
Train Loss after pre-train epoch 1: 6.93938150
Test Loss after pre-train epoch 1: 6.62544495
Train Loss after pre-train epoch 2: 6.65334621
Test Loss after pre-train epoch 2: 6.47613578
Train Loss after pre-train epoch 3: 6.39943984
Test Loss after pre-train epoch 3: 6.36268882
Train Loss after pre-train epoch 4: 6.15763265
Test Loss after pre-train epoch 4: 6.29995620
Train Loss after pre-train epoch 5: 5.95906783
Test Loss after pre-train epoch 5: 6.29359473
Test Loss after epoch 0: 6.29492617
Test Loss after epoch 1: 6.29245224
Test Loss after epoch 2: 6.29282458
DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 6
LEARNING_RATE_PRE_G: 0.01
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 0.0001
LEARNING_RATE_D: 0.0001
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
BETA1_G: 0.5
BETA1_D: 0.5
BETA2_G: 0.9
BETA2_D: 0.9
LOG_LOCATION: ./logs/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAMPLES_FOLDER: samples/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
PRETRAIN_CHK_FOLDER: ./checkpoints/pg_p_h32_l1e-2_e6_was2/
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: ./checkpoints/pg_p_h32_l1e-2_e6_was2/pg_p_h32_l1e-2.chk
GAN_CHK_FOLDER: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/
SAVE_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
LOAD_FILE_GAN: ./checkpoints/Wasspg_t0.2_b1-0.5_b2-0.9_g1d5_g32_d32_pe6_pl1e-2_l1e-4/chk
TEMPERATURE: 0.2
FIRST_CHAR: start
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5369)
(32, 20, 5369)
Test g_pre_loss after loading model: 6.29276640
Test Loss after epoch 0: 6.29383133
Test Loss after epoch 1: 6.29435205
Test Loss after epoch 2: 6.29333911
Test Loss after epoch 3: 6.29386371
Test Loss after epoch 4: 6.29495970
Test Loss after epoch 5: 6.29357670
Test Loss after epoch 6: 6.29173065
Test Loss after epoch 7: 6.28991713
Test Loss after epoch 8: 6.28868583
Test Loss after epoch 9: 6.28910672
Test Loss after epoch 10: 6.28908905
Test Loss after epoch 11: 6.28720582
Test Loss after epoch 12: 6.28807946
Test Loss after epoch 13: 6.28806933
Test Loss after epoch 14: 6.28659388
Test Loss after epoch 15: 6.28548642
Test Loss after epoch 16: 6.28503343
Test Loss after epoch 17: 6.28477812
Test Loss after epoch 18: 6.28458306
Test Loss after epoch 19: 6.28485757
Test Loss after epoch 20: 6.28482828
Test Loss after epoch 21: 6.28246221
Test Loss after epoch 22: 6.28246618
Test Loss after epoch 23: 6.28410273
Test Loss after epoch 24: 6.28320723
Test Loss after epoch 25: 6.28174796
Test Loss after epoch 26: 6.28272827
Test Loss after epoch 27: 6.28173767
Test Loss after epoch 28: 6.28208848
Test Loss after epoch 29: 6.28099688
Test Loss after epoch 30: 6.28037280
Test Loss after epoch 31: 6.27903315
Test Loss after epoch 32: 6.28032584
Test Loss after epoch 33: 6.27825343
Test Loss after epoch 34: 6.28059129
Test Loss after epoch 35: 6.27859980
Test Loss after epoch 36: 6.27848445
Test Loss after epoch 37: 6.27828536
Test Loss after epoch 38: 6.27858183
Test Loss after epoch 39: 6.27898884
Test Loss after epoch 40: 6.28009722
Test Loss after epoch 41: 6.27909833
Test Loss after epoch 42: 6.28218268
Test Loss after epoch 43: 6.27996217
Test Loss after epoch 44: 6.27899601
Test Loss after epoch 45: 6.27969731
Test Loss after epoch 46: 6.28026690
Test Loss after epoch 47: 6.27866670
Test Loss after epoch 48: 6.28044259
Test Loss after epoch 49: 6.27801911
Test Loss after epoch 50: 6.27908836
Test Loss after epoch 51: 6.27642758
Test Loss after epoch 52: 6.27609356
Test Loss after epoch 53: 6.27632741
Test Loss after epoch 54: 6.27505290
Test Loss after epoch 55: 6.27554224
Test Loss after epoch 56: 6.27550896
Test Loss after epoch 57: 6.27579134
Test Loss after epoch 58: 6.27472882
Test Loss after epoch 59: 6.27464956
Test Loss after epoch 60: 6.27436501
Test Loss after epoch 61: 6.27631220
Test Loss after epoch 62: 6.27592991
Test Loss after epoch 63: 6.27592930
Test Loss after epoch 64: 6.27601831
Test Loss after epoch 65: 6.27646006
Test Loss after epoch 66: 6.27442200
Test Loss after epoch 67: 6.27381931
Test Loss after epoch 68: 6.27343747
Test Loss after epoch 69: 6.27379134
Test Loss after epoch 70: 6.27246501
Test Loss after epoch 71: 6.27274530
Test Loss after epoch 72: 6.27419539
Test Loss after epoch 73: 6.27438573
Test Loss after epoch 74: 6.27462286
Test Loss after epoch 75: 6.27332492
Test Loss after epoch 76: 6.27248907
Test Loss after epoch 77: 6.27425628
Test Loss after epoch 78: 6.27347695
Test Loss after epoch 79: 6.27129452
Test Loss after epoch 80: 6.27420316
Test Loss after epoch 81: 6.27245861
Test Loss after epoch 82: 6.27101474
Test Loss after epoch 83: 6.26989737
Test Loss after epoch 84: 6.27271448
Test Loss after epoch 85: 6.27303470
Test Loss after epoch 86: 6.27252059
Test Loss after epoch 87: 6.27259477
Test Loss after epoch 88: 6.27246243
Test Loss after epoch 89: 6.27041163
Test Loss after epoch 90: 6.26860911
Test Loss after epoch 91: 6.26976358
Test Loss after epoch 92: 6.26931240
Test Loss after epoch 93: 6.26904278
Test Loss after epoch 94: 6.26981984
Test Loss after epoch 95: 6.27103661
Test Loss after epoch 96: 6.27138693
Test Loss after epoch 97: 6.26997131
Test Loss after epoch 98: 6.26986899
Test Loss after epoch 99: 6.27280907
Test Loss after epoch 100: 6.27204524
Test Loss after epoch 101: 6.27194440
Test Loss after epoch 102: 6.27323334
Test Loss after epoch 103: 6.27305351
Test Loss after epoch 104: 6.27354368
Test Loss after epoch 105: 6.27219946
Test Loss after epoch 106: 6.27424854
Test Loss after epoch 107: 6.27389303
Test Loss after epoch 108: 6.27141805
Test Loss after epoch 109: 6.27316184
Test Loss after epoch 110: 6.27317081
Test Loss after epoch 111: 6.27255644
Test Loss after epoch 112: 6.27314632
Test Loss after epoch 113: 6.27479784
Test Loss after epoch 114: 6.27376796
Test Loss after epoch 115: 6.27496032
Test Loss after epoch 116: 6.27312263
Test Loss after epoch 117: 6.27472531
Test Loss after epoch 118: 6.27555944
Test Loss after epoch 119: 6.27544354
Test Loss after epoch 120: 6.27326951
Test Loss after epoch 121: 6.27353664
Test Loss after epoch 122: 6.27299704
Test Loss after epoch 123: 6.27348961
Test Loss after epoch 124: 6.27251182
Test Loss after epoch 125: 6.27395751
Test Loss after epoch 126: 6.27293101
Test Loss after epoch 127: 6.27375844
Test Loss after epoch 128: 6.27402521
Test Loss after epoch 129: 6.27218954
Test Loss after epoch 130: 6.27211832
Test Loss after epoch 131: 6.27157664
Test Loss after epoch 132: 6.27193891
Test Loss after epoch 133: 6.27272990
Test Loss after epoch 134: 6.27222140
Test Loss after epoch 135: 6.27219039
Test Loss after epoch 136: 6.27267315
Test Loss after epoch 137: 6.27109246
Test Loss after epoch 138: 6.27152834
Test Loss after epoch 139: 6.27234639
Test Loss after epoch 140: 6.27120853
Test Loss after epoch 141: 6.27070301
Test Loss after epoch 142: 6.26969802
Test Loss after epoch 143: 6.26912948
Test Loss after epoch 144: 6.26886434
Test Loss after epoch 145: 6.26925756
Test Loss after epoch 146: 6.26999051
Test Loss after epoch 147: 6.27169663
Test Loss after epoch 148: 6.27088625
Test Loss after epoch 149: 6.27023820
Test Loss after epoch 150: 6.27056040
Test Loss after epoch 151: 6.27045165
Test Loss after epoch 152: 6.27027664
Test Loss after epoch 153: 6.26959628
Test Loss after epoch 154: 6.27038167
Test Loss after epoch 155: 6.27057829
Test Loss after epoch 156: 6.27154238
Test Loss after epoch 157: 6.27190232
Test Loss after epoch 158: 6.27254108
Test Loss after epoch 159: 6.27143071
Test Loss after epoch 160: 6.27294590
Test Loss after epoch 161: 6.27251297
Test Loss after epoch 162: 6.27308649
Test Loss after epoch 163: 6.27094573
Test Loss after epoch 164: 6.27122384
Test Loss after epoch 165: 6.26950642
Test Loss after epoch 166: 6.27086409
Test Loss after epoch 167: 6.27066469
Test Loss after epoch 168: 6.27239872
Test Loss after epoch 169: 6.27303023
Test Loss after epoch 170: 6.27245680
Test Loss after epoch 171: 6.27253379
Test Loss after epoch 172: 6.27245189
Test Loss after epoch 173: 6.27198498
Test Loss after epoch 174: 6.27262215
Test Loss after epoch 175: 6.27280669
Test Loss after epoch 176: 6.27109780
Test Loss after epoch 177: 6.27236565
Test Loss after epoch 178: 6.27097333
Test Loss after epoch 179: 6.27256066
Test Loss after epoch 180: 6.27256704
Test Loss after epoch 181: 6.27224030
Test Loss after epoch 182: 6.27220634
Test Loss after epoch 183: 6.27158745
Test Loss after epoch 184: 6.27215708
Test Loss after epoch 185: 6.27038210
Test Loss after epoch 186: 6.27273810
Test Loss after epoch 187: 6.27305761
Test Loss after epoch 188: 6.27312688
Test Loss after epoch 189: 6.27263811
Test Loss after epoch 190: 6.27368510
Test Loss after epoch 191: 6.27408406
Test Loss after epoch 192: 6.27608924
Test Loss after epoch 193: 6.27697662
Test Loss after epoch 194: 6.27575244
Test Loss after epoch 195: 6.27575425
Test Loss after epoch 196: 6.27582862
Test Loss after epoch 197: 6.27551954
Test Loss after epoch 198: 6.27299258
Test Loss after epoch 199: 6.27322963
