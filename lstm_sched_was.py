#!/usr/bin/env python
import tensorflow as tf
from tensorflow.contrib.rnn import LSTMCell
from tensorflow.contrib import rnn
from tensorflow.contrib import legacy_seq2seq
from gumbel_softmax import gumbel_softmax
from constants import *
import numpy as np
import os
import time

from data.ptb import CharLevelPTB, WordLevelPTB
from data.rnnpg import CharLevelRNNPG
from data.oracle import OracleDataloader, OracleVerifier
from utils import *

import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='gumbel-gans')

    parser.add_argument('--dataset', type=str, default=DATASET, choices=['ptb', 'wptb', 'pg', 'oracle'])
    parser.add_argument('--batch_size', type=int, default=BATCH_SIZE)
    parser.add_argument('--pretrain_epochs', type=int, default=PRETRAIN_EPOCHS)
    parser.add_argument('--lr_pretrain', type=float, default=LEARNING_RATE_PRE_G)
    parser.add_argument('--hidden_size_g', type=int, default=HIDDEN_STATE_SIZE)
    parser.add_argument('--hidden_size_d', type=int, default=HIDDEN_STATE_SIZE_D)
    parser.add_argument('--lr_g', type=float, default=LEARNING_RATE_G)
    parser.add_argument('--lr_d', type=float, default=LEARNING_RATE_D)
    parser.add_argument('--epochs', type=int, default=N_EPOCHS)
    parser.add_argument('--beta1_g', type=float, default=BETA1_G)
    parser.add_argument('--beta1_d', type=float, default=BETA1_D)
    parser.add_argument('--beta2_g', type=float, default=BETA2_G)
    parser.add_argument('--beta2_d', type=float, default=BETA2_D)
    parser.add_argument('--num_d', type=int, default=NUM_D)
    parser.add_argument('--num_g', type=int, default=NUM_G)
    parser.add_argument('--temp', type=float, default=TEMPERATURE)
    parser.add_argument('--first_char', type=str, default=FIRST_CHAR, choices=['start', 'rand'])

    return parser.parse_args()

args = parse_args()

DATASET = args.dataset
BATCH_SIZE = args.batch_size
PRETRAIN_EPOCHS = args.pretrain_epochs
LEARNING_RATE_PRE_G = args.lr_pretrain
HIDDEN_STATE_SIZE = args.hidden_size_g
HIDDEN_STATE_SIZE_D = args.hidden_size_d
LEARNING_RATE_G = args.lr_g
LEARNING_RATE_D = args.lr_d
N_EPOCHS = args.epochs
BETA1_G = args.beta1_g
BETA1_D = args.beta1_d
BETA2_G = args.beta2_g
BETA2_D = args.beta2_d
NUM_D = args.num_d
NUM_G = args.num_g
TEMPERATURE = args.temp
FIRST_CHAR = args.first_char

from decimal import Decimal

# define other constants
SUBFOLDER_NAME = 'Wass_' + DATASET[:2] + '_t' + str(TEMPERATURE) + '_' + FIRST_CHAR + '_b1-' + str(BETA1_G) + '_b2-' + str(BETA2_G) + '_g' + str(NUM_G) + 'd' + str(NUM_D) + '_g' + str(HIDDEN_STATE_SIZE) + '_d' + str(HIDDEN_STATE_SIZE_D) + '_pe' + str(PRETRAIN_EPOCHS) + '_pl' + str("{:.0e}".format(Decimal(LEARNING_RATE_PRE_G))) + '_l'+ str("{:.0e}".format(Decimal(LEARNING_RATE_G))) + '/'
LOG_LOCATION = './logs/' + SUBFOLDER_NAME
PRETRAIN_CHK_FOLDER = False
if DATASET == 'oracle':
    SAVE_FILE_PRETRAIN = './checkpoints/or_pi_h32_le-3_e15/or_h32_le-3.chk'
else:
    SAVE_FILE_PRETRAIN = './checkpoints/'+ SUBFOLDER_NAME + 'chk'
LOAD_FILE_PRETRAIN = SAVE_FILE_PRETRAIN # './checkpoints/or_p_h32_le-3_e20/or_p_h32_le-3.chk'
GAN_CHK_FOLDER = './checkpoints/' + SUBFOLDER_NAME
SAVE_FILE_GAN = GAN_CHK_FOLDER + 'chk'
LOAD_FILE_GAN = SAVE_FILE_GAN
SAMPLES_FOLDER = 'samples/' + SUBFOLDER_NAME

if DATASET == 'ptb':
    TEST_BATCH_SIZE = 1
else:
    # TODO: Make this larger for faster validation, test time
    TEST_BATCH_SIZE = BATCH_SIZE

def print_hyperparam_consts():
    print('DATASET: ' + str(DATASET))
    print('BATCH_SIZE: ' + str(BATCH_SIZE))
    print('PRETRAIN_EPOCHS: ' + str(PRETRAIN_EPOCHS))
    print('LEARNING_RATE_PRE_G: ' + str(LEARNING_RATE_PRE_G))
    print('HIDDEN_STATE_SIZE: ' + str(HIDDEN_STATE_SIZE))
    print('HIDDEN_STATE_SIZE_D: ' + str(HIDDEN_STATE_SIZE_D))
    print('LEARNING_RATE_G: ' + str(LEARNING_RATE_G))
    print('LEARNING_RATE_D: ' + str(LEARNING_RATE_D))
    print('N_EPOCHS: ' + str(N_EPOCHS))
    print('NUM_D: ' + str(NUM_D))
    print('NUM_G: ' + str(NUM_G))
    print('BETA1_G: ' + str(BETA1_G))
    print('BETA1_D: ' + str(BETA1_D))
    print('BETA2_G: ' + str(BETA2_G))
    print('BETA2_D: ' + str(BETA2_D))
    print('LOG_LOCATION: ' + str(LOG_LOCATION))
    print('SAMPLES_FOLDER: ' + str(SAMPLES_FOLDER))
    print('PRETRAIN_CHK_FOLDER: ' + str(PRETRAIN_CHK_FOLDER))
    print('SAVE_FILE_PRETRAIN: ' + str(SAVE_FILE_PRETRAIN))
    print('LOAD_FILE_PRETRAIN: ' + str(LOAD_FILE_PRETRAIN))
    print('GAN_CHK_FOLDER: ' + str(GAN_CHK_FOLDER))
    print('SAVE_FILE_GAN: ' + str(SAVE_FILE_GAN))
    print('LOAD_FILE_GAN: ' + str(LOAD_FILE_GAN))
    print('TEMPERATURE: ' + str(TEMPERATURE))
    print('FIRST_CHAR: ' + str(FIRST_CHAR))

print_hyperparam_consts()

# create dirs that don't exist
if SAVE_FILE_GAN:
    create_dir_if_not_exists('/'.join(SAVE_FILE_GAN.split('/')[:-1]))
create_dir_if_not_exists(LOG_LOCATION)
create_dir_if_not_exists(SAMPLES_FOLDER)

SEQ_LENGTH, VOCAB_SIZE, TEST_SIZE, c = None, None, None, None

if DATASET == 'ptb':
    c = CharLevelPTB()
    VOCAB_SIZE = PTB_VOCAB_SIZE
    SEQ_LENGTH = PTB_SEQ_LENGTH
elif DATASET == 'pg':
    c = CharLevelRNNPG()
    VOCAB_SIZE = PG_VOCAB_SIZE
    SEQ_LENGTH = PG_SEQ_LENGTH
elif DATASET == 'oracle':
    c = OracleDataloader(BATCH_SIZE, ORACLE_VOCAB_SIZE)
    VOCAB_SIZE = ORACLE_VOCAB_SIZE
    SEQ_LENGTH = ORACLE_SEQ_LENGTH
    TEST_SIZE = ORACLE_TEST_SIZE
elif DATASET == 'wptb':
    c = WordLevelPTB()
    VOCAB_SIZE = c.vocab_size
    SEQ_LENGTH = c.seqlen

initial_c = tf.placeholder(tf.float32, shape=(None, HIDDEN_STATE_SIZE))
initial_h = tf.placeholder(tf.float32, shape=(None, HIDDEN_STATE_SIZE))

inputs_pre = tf.placeholder(tf.float32, [None, SEQ_LENGTH-1, VOCAB_SIZE])
inputs = tf.placeholder(tf.float32, [None, SEQ_LENGTH-1, VOCAB_SIZE])

targets = tf.placeholder(tf.float32, [None, SEQ_LENGTH-1, VOCAB_SIZE])


def generator(initial_c, initial_h, mode='gan', first='start', inputs=None, targets=None, reuse=False):
    assert mode in ['test', 'pretrain', 'gan']
    assert first in ['start', 'rand']
    with tf.variable_scope("generator") as scope:
        if reuse:
            scope.reuse_variables()
        softmax_w = tf.get_variable("softmax_w", [HIDDEN_STATE_SIZE, VOCAB_SIZE])
        softmax_b = tf.get_variable("softmax_b", [VOCAB_SIZE])

        def loop_function(prev_output, _):
            prev_output = tf.matmul(prev_output, softmax_w) + softmax_b
            return gumbel_softmax(prev_output, temperature=TEMPERATURE, hard=True)

        first_input = np.zeros((BATCH_SIZE, VOCAB_SIZE))
        # Create a dummy first input
        if first == 'start':
            first_input[:, c.start_char_idx] = 1
        # Create a first input randomly
        elif first == 'rand':
            start_chars = np.random.randint(0, VOCAB_SIZE, BATCH_SIZE)
            first_input[np.arange(BATCH_SIZE), start_chars] = 1
        # TODO: Create a first input based on statistical distribution
        first_input = tf.constant(first_input, dtype=tf.float32)

        cell = LSTMCell(HIDDEN_STATE_SIZE, state_is_tuple=True, reuse=reuse)
        if mode == 'pretrain':
            inputs = tf.unstack(inputs, axis=1)
            outputs, states = legacy_seq2seq.rnn_decoder(decoder_inputs=inputs,
                                                         initial_state=(initial_c, initial_h),
                                                         cell=cell, loop_function=None, scope=scope)
            logits = [tf.matmul(output, softmax_w) + softmax_b for output in outputs]

            targets = tf.unstack(tf.transpose(targets, [1, 0, 2]))

            loss = [tf.nn.softmax_cross_entropy_with_logits(labels=target, logits=logit)
                    for target, logit in zip(targets, logits)] # ignore start token
            loss = tf.reduce_mean(loss)
            return loss, states
        elif mode == 'test':
            outputs, states = legacy_seq2seq.rnn_decoder(decoder_inputs=(SEQ_LENGTH-1) * [first_input],
                                                         initial_state=(initial_c, initial_h),
                                                         cell=cell, loop_function=loop_function, scope=scope)
            logits = [tf.matmul(output, softmax_w) + softmax_b for output in outputs]
            return tf.transpose(tf.argmax(logits, axis=2))
        else:
            outputs, states = legacy_seq2seq.rnn_decoder(decoder_inputs=(SEQ_LENGTH-1) * [first_input],
                                                         initial_state=(initial_c, initial_h),
                                                         cell=cell, loop_function=loop_function, scope=scope)

            logits = [tf.matmul(output, softmax_w) + softmax_b for output in outputs]

            ys = [gumbel_softmax(logit, temperature=TEMPERATURE, hard=True) for logit in logits]
            ys = tf.stack(ys, axis=1)
            return ys


def discriminator(x, reuse=False):
    with tf.variable_scope("discriminator") as scope:
        if reuse:
            scope.reuse_variables()
        lstm = LSTMCell(HIDDEN_STATE_SIZE_D, state_is_tuple=True, reuse=reuse)
        softmax_w = tf.get_variable("softmax_w", [HIDDEN_STATE_SIZE_D, N_D_CLASSES])
        softmax_b = tf.get_variable("softmax_b", [N_D_CLASSES])

        x = tf.unstack(x, axis=1)
        lstm_outputs, _states = tf.contrib.rnn.static_rnn(lstm, x, dtype=tf.float32, scope=scope)

        return tf.matmul(lstm_outputs[-1], softmax_w) + softmax_b

def generate_test_file(g_test, sess, eval_file):
    z = sample_Z(BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
    c_z, h_z = np.vsplit(z, 2)

    with open(eval_file, 'w') as f:
        for _ in xrange(TEST_SIZE/BATCH_SIZE):
            oracle_out = sess.run([g_test], feed_dict={
                initial_c: c_z,
                initial_h: h_z
            })
            for l in oracle_out[0]:
                buffer = ' '.join([str(x) for x in l]) + '\n'
                f.write(buffer)

# Evaluate the losses
d_logits = discriminator(inputs)
g_pre_loss, g_Z_states = generator(initial_c, initial_h, mode='pretrain', inputs=inputs_pre, targets=targets)

# Optimizers
t_vars = tf.trainable_variables()
print [var.name for var in t_vars]
g_vars = [var for var in t_vars if var.name.startswith('generator')]
d_vars = [var for var in t_vars if var.name.startswith('discriminator')]

g_pre_optim = tf.train.AdamOptimizer(LEARNING_RATE_PRE_G, name="Adam_g_pre").minimize(g_pre_loss, var_list=g_vars)
g_pre_loss_sum = tf.summary.scalar("g_pre_loss", g_pre_loss)

g = generator(initial_c, initial_h, first=FIRST_CHAR, reuse=True)
g_test = generator(initial_c, initial_h, reuse=True, mode='test')
d_logits_ = discriminator(g, reuse=True)

d_loss = tf.reduce_mean(d_logits_) - tf.reduce_mean(d_logits)
g_loss = -tf.reduce_mean(d_logits_)

# WGAN lipschitz-penalty
alpha = tf.random_uniform(
    shape=[BATCH_SIZE, 1, 1],
    minval=0.,
    maxval=1.
)
fake_inputs, real_inputs = g, inputs
differences = fake_inputs - real_inputs
interpolates = real_inputs + (alpha*differences)
print interpolates.get_shape()
gradients = tf.gradients(discriminator(interpolates, reuse=True), [interpolates])[0]
print gradients.get_shape()
slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1, 2]))
gradient_penalty = tf.reduce_mean((slopes-1.)**2)
d_loss += LAMBDA*gradient_penalty

g_loss_sum = tf.summary.scalar("g_loss", g_loss)
d_loss_sum = tf.summary.scalar("d_loss", d_loss)

d_optim = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE_D, beta1=BETA1_D, beta2=BETA2_D).minimize(d_loss, var_list=d_vars)
g_optim = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE_G, beta1=BETA1_G, beta2=BETA2_G, name="Adam_g").minimize(g_loss, var_list=g_vars)

with tf.Session() as sess:

    def generate_samples():
        z = sample_Z(BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
        c_z, h_z = np.vsplit(z, 2)

        samples = sess.run(g, feed_dict={
            initial_c: c_z,
            initial_h: h_z
        })

        decoded_samples = []
        for i in xrange(len(samples)):
            decoded = []
            for j in xrange(len(samples[i])):
                if DATASET == 'wptb':
                    decoded.append(c.idx2word[np.argmax(samples[i][j])])
                else:
                    decoded.append(c.idx2char[np.argmax(samples[i][j])])
            decoded_samples.append(tuple(decoded))
        return decoded_samples

    if DATASET == 'oracle':
        t=OracleVerifier(BATCH_SIZE, sess)
    tf.initialize_all_variables().run()
    writer = tf.summary.FileWriter(LOG_LOCATION, sess.graph)
    counter = 1

    saver = tf.train.Saver()

    if LOAD_FILE_PRETRAIN: # and tf.train.latest_checkpoint(PRETRAIN_CHK_FOLDER) == LOAD_FILE_PRETRAIN:
        # saver = tf.train.import_meta_graph(LOAD_FILE_PRETRAIN + '.meta')
        saver.restore(sess, LOAD_FILE_PRETRAIN)
        if DATASET == 'oracle':
            generate_test_file(g_test, sess, t.eval_file)
            print "NLL Oracle Loss after loading model: %.8f" % t.get_loss()
        else:
            batch_idx = 0
            ltot = 0.
            z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
            c_z, h_z = np.vsplit(z, 2)
            for batch in c.get_test_batch(TEST_BATCH_SIZE):
                batch = c.convert_batch_to_input_target(batch)
                batch_input, batch_targets = batch

                g_pre_loss_curr, summary_str, z_stateful_out = sess.run([g_pre_loss, g_pre_loss_sum, g_Z_states], feed_dict={
                    inputs_pre: batch_input,
                    initial_c: c_z,
                    initial_h: h_z,
                    targets: batch_targets
                })

                if TEST_BATCH_SIZE == 1:
                    c_z, h_z = z_stateful_out
                else:
                    z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
                    c_z, h_z = np.vsplit(z, 2)
                ltot += g_pre_loss_curr
                writer.add_summary(summary_str, counter)
                batch_idx += 1
            print "Test g_pre_loss after loading model: %.8f" % (ltot/batch_idx)

    for pre_epoch in xrange(PRETRAIN_EPOCHS):
        batch_idx = 0
        train_ltot = 0.
        for batch in c.get_train_batch(BATCH_SIZE):
            batch = c.convert_batch_to_input_target(batch)
            batch_input, batch_targets = batch

            z = sample_Z(BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
            c_z, h_z = np.vsplit(z, 2)
            _, g_pre_loss_curr, summary_str = sess.run([g_pre_optim, g_pre_loss, g_pre_loss_sum], feed_dict={
                inputs_pre: batch_input,
                initial_c: c_z,
                initial_h: h_z,
                targets: batch_targets
            })
            train_ltot += g_pre_loss_curr
            writer.add_summary(summary_str, counter)
            batch_idx += 1
        print "Train Loss after pre-train epoch %d: %.8f" % (pre_epoch, train_ltot / batch_idx)
            # print "Epoch: [%d] Batch: %d, g_pre_loss: %.8f" % (pre_epoch, batch_idx, g_pre_loss_curr)
        if DATASET == 'oracle':
            generate_test_file(g_test, sess, t.eval_file)
            print "NLL Oracle Loss after pre-train epoch %d: %.8f" % (pre_epoch, t.get_loss())
        else:
            batch_idx = 0
            ltot = 0.
            z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
            c_z, h_z = np.vsplit(z, 2)
            for batch in c.get_valid_batch(TEST_BATCH_SIZE):
                batch = c.convert_batch_to_input_target(batch)
                batch_input, batch_targets = batch

                g_pre_loss_curr, summary_str, z_stateful_out = sess.run([g_pre_loss, g_pre_loss_sum, g_Z_states], feed_dict={
                    inputs_pre: batch_input,
                    initial_c: c_z,
                    initial_h: h_z,
                    targets: batch_targets
                })

                if TEST_BATCH_SIZE == 1:
                    c_z, h_z = z_stateful_out
                else:
                    z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
                    c_z, h_z = np.vsplit(z, 2)
                ltot += g_pre_loss_curr
                writer.add_summary(summary_str, counter)
                batch_idx += 1
            print "Valid Loss after pre-train epoch %d: %.8f" % (pre_epoch, ltot / batch_idx)

            batch_idx = 0
            ltot = 0.
            z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
            c_z, h_z = np.vsplit(z, 2)
            for batch in c.get_test_batch(TEST_BATCH_SIZE):
                batch = c.convert_batch_to_input_target(batch)
                batch_input, batch_targets = batch

                g_pre_loss_curr, summary_str, z_stateful_out = sess.run([g_pre_loss, g_pre_loss_sum, g_Z_states], feed_dict={
                    inputs_pre: batch_input,
                    initial_c: c_z,
                    initial_h: h_z,
                    targets: batch_targets
                })

                if TEST_BATCH_SIZE == 1:
                    c_z, h_z = z_stateful_out
                else:
                    z = sample_Z(TEST_BATCH_SIZE * 2, HIDDEN_STATE_SIZE)
                    c_z, h_z = np.vsplit(z, 2)
                ltot += g_pre_loss_curr
                writer.add_summary(summary_str, counter)
                batch_idx += 1
            print "Test Loss after pre-train epoch %d: %.8f" % (pre_epoch, ltot / batch_idx)