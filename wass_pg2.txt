DATASET: pg
BATCH_SIZE: 32
PRETRAIN_EPOCHS: 0
LEARNING_RATE_PRE_G: 0.001
HIDDEN_STATE_SIZE: 32
HIDDEN_STATE_SIZE_D: 32
LEARNING_RATE_G: 5e-05
LEARNING_RATE_D: 5e-05
N_EPOCHS: 200
NUM_D: 5
NUM_G: 1
LOG_LOCATION: ./logs/Wasspg_g1d5_g32_d32_pe0_pl1e-3_l5e-5/
PRETRAIN_CHK_FOLDER: False
SAVE_FILE_PRETRAIN: False
LOAD_FILE_PRETRAIN: False
GAN_CHK_FOLDER: ./checkpoints/Wasspg_g1d5_g32_d32_pe0_pl1e-3_l5e-5/
SAVE_FILE_GAN: ./checkpoints/Wasspg_g1d5_g32_d32_pe0_pl1e-3_l5e-5/chk
LOAD_FILE_GAN: ./checkpoints/Wasspg_g1d5_g32_d32_pe0_pl1e-3_l5e-5/chk
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
(32, 20, 5369)
(32, 20, 5369)
2017-04-07 19:57:42.465852: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 19:57:42.465905: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 19:57:42.465911: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 19:57:42.465916: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 19:57:42.465920: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-07 19:57:53.159609: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:07:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-07 19:57:53.159657: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-07 19:57:53.159665: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-07 19:57:53.159673: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:07:00.0)
WARNING:tensorflow:From model.py:253: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Test g_pre_loss before training: 8.58979542
2017-04-07 19:58:26.526087: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 5660 get requests, put_count=5626 evicted_count=1000 eviction_rate=0.177746 and unsatisfied allocation rate=0.200353
2017-04-07 19:58:26.526163: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-07 19:58:35.423019: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2203 get requests, put_count=2316 evicted_count=1000 eviction_rate=0.431779 and unsatisfied allocation rate=0.413073
2017-04-07 19:58:35.423112: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Test Loss after epoch 0: 8.58707653
Test Loss after epoch 1: 8.58330554
Test Loss after epoch 2: 8.57897066
Test Loss after epoch 3: 8.57417356
Test Loss after epoch 4: 8.56859810
Test Loss after epoch 5: 8.56067448
Test Loss after epoch 6: 8.54836833
Test Loss after epoch 7: 8.52501838
Test Loss after epoch 8: 8.47092256
Test Loss after epoch 9: 8.38783630
Test Loss after epoch 10: 8.29865726
Test Loss after epoch 11: 8.20919317
Test Loss after epoch 12: 8.12288017
Test Loss after epoch 13: 8.03798806
Test Loss after epoch 14: 7.95214838
Test Loss after epoch 15: 7.87077188
Test Loss after epoch 16: 7.78495409
Test Loss after epoch 17: 7.70448782
Test Loss after epoch 18: 7.63033381
Test Loss after epoch 19: 7.55681473
Test Loss after epoch 20: 7.48684492
Test Loss after epoch 21: 7.42274495
Test Loss after epoch 22: 7.36174773
Test Loss after epoch 23: 7.31178545
Test Loss after epoch 24: 7.27081034
Test Loss after epoch 25: 7.23644334
Test Loss after epoch 26: 7.21276731
Test Loss after epoch 27: 7.19010159
Test Loss after epoch 28: 7.17885707
Test Loss after epoch 29: 7.16647662
Test Loss after epoch 30: 7.15844187
Test Loss after epoch 31: 7.14853691
Test Loss after epoch 32: 7.14128517
Test Loss after epoch 33: 7.12898139
Test Loss after epoch 34: 7.12513387
Test Loss after epoch 35: 7.11622397
Test Loss after epoch 36: 7.10648987
Test Loss after epoch 37: 7.10065033
Test Loss after epoch 38: 7.09944124
Test Loss after epoch 39: 7.09581506
Test Loss after epoch 40: 7.09174153
Test Loss after epoch 41: 7.08381111
Test Loss after epoch 42: 7.07871262
Test Loss after epoch 43: 7.07937919
Test Loss after epoch 44: 7.07333523
Test Loss after epoch 45: 7.06674101
Test Loss after epoch 46: 7.06255425
Test Loss after epoch 47: 7.06257245
Test Loss after epoch 48: 7.06074278
Test Loss after epoch 49: 7.05601594
Test Loss after epoch 50: 7.05306978
Test Loss after epoch 51: 7.04992565
Test Loss after epoch 52: 7.04995606
Test Loss after epoch 53: 7.04763043
Test Loss after epoch 54: 7.04549931
Test Loss after epoch 55: 7.04171222
Test Loss after epoch 56: 7.04117060
Test Loss after epoch 57: 7.03939033
Test Loss after epoch 58: 7.03606805
Test Loss after epoch 59: 7.03818886
Test Loss after epoch 60: 7.03137427
Test Loss after epoch 61: 7.03259542
Test Loss after epoch 62: 7.02777556
Test Loss after epoch 63: 7.02742192
Test Loss after epoch 64: 7.02413347
Test Loss after epoch 65: 7.02345980
Test Loss after epoch 66: 7.02419146
Test Loss after epoch 67: 7.02194309
Test Loss after epoch 68: 7.01819844
Test Loss after epoch 69: 7.01931738
Test Loss after epoch 70: 7.01617370
Test Loss after epoch 71: 7.01709698
Test Loss after epoch 72: 7.01420024
Test Loss after epoch 73: 7.01235611
Test Loss after epoch 74: 7.01207241
Test Loss after epoch 75: 7.01221937
Test Loss after epoch 76: 7.00784955
Test Loss after epoch 77: 7.00558521
Test Loss after epoch 78: 7.00637222
Test Loss after epoch 79: 7.00417276
Test Loss after epoch 80: 7.00368878
Test Loss after epoch 81: 7.00452006
Test Loss after epoch 82: 7.00495200
Test Loss after epoch 83: 7.00330722
Test Loss after epoch 84: 7.00187342
Test Loss after epoch 85: 7.00016185
Test Loss after epoch 86: 6.99987132
Test Loss after epoch 87: 6.99929968
Test Loss after epoch 88: 6.99942463
Test Loss after epoch 89: 6.99675134
Test Loss after epoch 90: 6.99877502
Test Loss after epoch 91: 6.99701471
Test Loss after epoch 92: 6.99467510
Test Loss after epoch 93: 6.99398318
Test Loss after epoch 94: 6.99358282
Test Loss after epoch 95: 6.99380029
Test Loss after epoch 96: 6.99116264
Test Loss after epoch 97: 6.99324226
Test Loss after epoch 98: 6.99204834
Test Loss after epoch 99: 6.99137694
Test Loss after epoch 100: 6.99110480
Test Loss after epoch 101: 6.99002198
Test Loss after epoch 102: 6.98899244
Test Loss after epoch 103: 6.98920905
Test Loss after epoch 104: 6.98908597
Test Loss after epoch 105: 6.98792316
Test Loss after epoch 106: 6.98755661
Test Loss after epoch 107: 6.98747498
Test Loss after epoch 108: 6.98630534
Test Loss after epoch 109: 6.98468839
Test Loss after epoch 110: 6.98631317
Test Loss after epoch 111: 6.98566375
Test Loss after epoch 112: 6.98377731
Test Loss after epoch 113: 6.98400237
Test Loss after epoch 114: 6.98260929
Test Loss after epoch 115: 6.98196931
Test Loss after epoch 116: 6.98259411
Test Loss after epoch 117: 6.98168448
Test Loss after epoch 118: 6.98102413
Test Loss after epoch 119: 6.97978915
Test Loss after epoch 120: 6.97994014
Test Loss after epoch 121: 6.98046044
Test Loss after epoch 122: 6.97998348
Test Loss after epoch 123: 6.97905700
Test Loss after epoch 124: 6.97917764
Test Loss after epoch 125: 6.97817442
Test Loss after epoch 126: 6.97838357
Test Loss after epoch 127: 6.97749119
Test Loss after epoch 128: 6.97814042
Test Loss after epoch 129: 6.97744563
Test Loss after epoch 130: 6.97631791
Test Loss after epoch 131: 6.97626177
Test Loss after epoch 132: 6.97618432
Test Loss after epoch 133: 6.97729457
Test Loss after epoch 134: 6.97641296
Test Loss after epoch 135: 6.97616171
Test Loss after epoch 136: 6.97616689
Test Loss after epoch 137: 6.97547108
Test Loss after epoch 138: 6.97499972
Test Loss after epoch 139: 6.97437934
Test Loss after epoch 140: 6.97540720
Test Loss after epoch 141: 6.97407864
Test Loss after epoch 142: 6.97436520
Test Loss after epoch 143: 6.97334570
Test Loss after epoch 144: 6.97323327
Test Loss after epoch 145: 6.97297904
Test Loss after epoch 146: 6.97364126
Test Loss after epoch 147: 6.97283936
Test Loss after epoch 148: 6.97167423
Test Loss after epoch 149: 6.97155812
Test Loss after epoch 150: 6.97053040
Test Loss after epoch 151: 6.97096256
Test Loss after epoch 152: 6.97094325
Test Loss after epoch 153: 6.97037631
Test Loss after epoch 154: 6.96974154
Test Loss after epoch 155: 6.97004064
Test Loss after epoch 156: 6.97059095
Test Loss after epoch 157: 6.97037628
Test Loss after epoch 158: 6.97029640
Test Loss after epoch 159: 6.96953080
Test Loss after epoch 160: 6.96955855
Test Loss after epoch 161: 6.96971638
Test Loss after epoch 162: 6.97026662
Test Loss after epoch 163: 6.96946985
Test Loss after epoch 164: 6.96832912
Test Loss after epoch 165: 6.96834936
Test Loss after epoch 166: 6.96753114
Test Loss after epoch 167: 6.96774434
Test Loss after epoch 168: 6.96829756
Test Loss after epoch 169: 6.96747395
Test Loss after epoch 170: 6.96703509
Test Loss after epoch 171: 6.96724213
Test Loss after epoch 172: 6.96685513
Test Loss after epoch 173: 6.96738546
Test Loss after epoch 174: 6.96761391
Test Loss after epoch 175: 6.96734065
Test Loss after epoch 176: 6.96693687
Test Loss after epoch 177: 6.96607181
Test Loss after epoch 178: 6.96679743
Test Loss after epoch 179: 6.96605502
Test Loss after epoch 180: 6.96649538
Test Loss after epoch 181: 6.96597370
Test Loss after epoch 182: 6.96576717
Test Loss after epoch 183: 6.96527732
Test Loss after epoch 184: 6.96469716
Test Loss after epoch 185: 6.96475366
Test Loss after epoch 186: 6.96472886
Test Loss after epoch 187: 6.96578776
Test Loss after epoch 188: 6.96613213
Test Loss after epoch 189: 6.96595455
Test Loss after epoch 190: 6.96568241
Test Loss after epoch 191: 6.96525441
Test Loss after epoch 192: 6.96480599
Test Loss after epoch 193: 6.96470065
Test Loss after epoch 194: 6.96525005
Test Loss after epoch 195: 6.96528725
Test Loss after epoch 196: 6.96519275
Test Loss after epoch 197: 6.96510529
Test Loss after epoch 198: 6.96584159
Test Loss after epoch 199: 6.96537573
