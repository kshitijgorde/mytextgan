nohup: ignoring input
[u'discriminator/softmax_w:0', u'discriminator/softmax_b:0', u'discriminator/lstm_cell/weights:0', u'discriminator/lstm_cell/biases:0', u'generator/softmax_w:0', u'generator/softmax_b:0', u'generator/lstm_cell/weights:0', u'generator/lstm_cell/biases:0']
2017-04-01 14:42:31.270866: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-01 14:42:31.270916: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-01 14:42:31.270928: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-04-01 14:42:31.270934: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-04-01 14:42:31.270939: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-04-01 14:42:36.983651: I tensorflow/core/common_runtime/gpu/gpu_device.cc:887] Found device 0 with properties: 
name: Tesla K80
major: 3 minor: 7 memoryClockRate (GHz) 0.8235
pciBusID 0000:85:00.0
Total memory: 11.17GiB
Free memory: 11.11GiB
2017-04-01 14:42:36.983726: I tensorflow/core/common_runtime/gpu/gpu_device.cc:908] DMA: 0 
2017-04-01 14:42:36.983737: I tensorflow/core/common_runtime/gpu/gpu_device.cc:918] 0:   Y 
2017-04-01 14:42:36.983752: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977] Creating TensorFlow device (/gpu:0) -> (device: 0, name: Tesla K80, pci bus id: 0000:85:00.0)
WARNING:tensorflow:From model.py:153: initialize_all_variables (from tensorflow.python.ops.variables) is deprecated and will be removed after 2017-03-02.
Instructions for updating:
Use `tf.global_variables_initializer` instead.
Test g_pre_loss after loading model: 6.17169261
2017-04-01 14:43:02.446002: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 15093 get requests, put_count=15019 evicted_count=1000 eviction_rate=0.0665823 and unsatisfied allocation rate=0.0777844
2017-04-01 14:43:02.446058: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 100 to 110
2017-04-01 14:43:18.232901: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 7514 get requests, put_count=7554 evicted_count=1000 eviction_rate=0.13238 and unsatisfied allocation rate=0.130822
2017-04-01 14:43:18.232971: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:259] Raising pool_size_limit_ from 256 to 281
Test Loss after epoch 0: 6.17109405
Test Loss after epoch 1: 6.17153732
Test Loss after epoch 2: 6.17127273
Test Loss after epoch 3: 6.17293277
Test Loss after epoch 4: 6.16919955
Test Loss after epoch 5: 6.17186086
2017-04-01 14:55:53.901784: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 2218141 get requests, put_count=2218141 evicted_count=3000 eviction_rate=0.00135248 and unsatisfied allocation rate=0.00137457
Test Loss after epoch 6: 6.17067551
Test Loss after epoch 7: 6.17204875
Test Loss after epoch 8: 6.17120768
Test Loss after epoch 9: 6.17256963
Test Loss after epoch 10: 6.17102926
Test Loss after epoch 11: 6.17222503
Test Loss after epoch 12: 6.16954825
Test Loss after epoch 13: 6.17314097
Test Loss after epoch 14: 6.17032689
Test Loss after epoch 15: 6.17039423
Test Loss after epoch 16: 6.17159309
Test Loss after epoch 17: 6.17135780
Test Loss after epoch 18: 6.17447095
Test Loss after epoch 19: 6.17169545
Test Loss after epoch 20: 6.16960296
Test Loss after epoch 21: 6.17085038
2017-04-01 15:26:14.563539: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 9188372 get requests, put_count=9188372 evicted_count=13000 eviction_rate=0.00141483 and unsatisfied allocation rate=0.00142016
Test Loss after epoch 22: 6.16900349
Test Loss after epoch 23: 6.17139491
Test Loss after epoch 24: 6.17036218
Test Loss after epoch 25: 6.16949359
Test Loss after epoch 26: 6.16985759
Test Loss after epoch 27: 6.17013643
Test Loss after epoch 28: 6.17101696
Test Loss after epoch 29: 6.16946674
Test Loss after epoch 30: 6.16985362
Test Loss after epoch 31: 6.16834435
Test Loss after epoch 32: 6.16946431
Test Loss after epoch 33: 6.16975129
Test Loss after epoch 34: 6.17011770
Test Loss after epoch 35: 6.16911616
2017-04-01 15:52:16.113536: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 15169757 get requests, put_count=15169757 evicted_count=23000 eviction_rate=0.00151617 and unsatisfied allocation rate=0.0015194
Test Loss after epoch 36: 6.16995221
Test Loss after epoch 37: 6.16938127
Test Loss after epoch 38: 6.16953104
Test Loss after epoch 39: 6.16922912
Test Loss after epoch 40: 6.16963069
Test Loss after epoch 41: 6.16933429
Test Loss after epoch 42: 6.17109328
Test Loss after epoch 43: 6.16913577
Test Loss after epoch 44: 6.16897612
Test Loss after epoch 45: 6.17181726
Test Loss after epoch 46: 6.17129855
Test Loss after epoch 47: 6.16938920
Test Loss after epoch 48: 6.16892857
Test Loss after epoch 49: 6.16920569
2017-04-01 16:18:06.855846: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 21101588 get requests, put_count=21101588 evicted_count=33000 eviction_rate=0.00156386 and unsatisfied allocation rate=0.00156619
Test Loss after epoch 50: 6.16963076
Test Loss after epoch 51: 6.17002533
Test Loss after epoch 52: 6.16888147
Test Loss after epoch 53: 6.16944541
Test Loss after epoch 54: 6.16915381
Test Loss after epoch 55: 6.16945200
Test Loss after epoch 56: 6.17002305
Test Loss after epoch 57: 6.17084103
Test Loss after epoch 58: 6.16978555
Test Loss after epoch 59: 6.17126465
Test Loss after epoch 60: 6.16952747
Test Loss after epoch 61: 6.17046051
Test Loss after epoch 62: 6.16902667
2017-04-01 16:43:18.853591: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 26885219 get requests, put_count=26885213 evicted_count=43000 eviction_rate=0.00159939 and unsatisfied allocation rate=0.00160144
Test Loss after epoch 63: 6.17021749
Test Loss after epoch 64: 6.17276833
Test Loss after epoch 65: 6.17133444
Test Loss after epoch 66: 6.17200675
Test Loss after epoch 67: 6.17193055
Test Loss after epoch 68: 6.17036932
Test Loss after epoch 69: 6.17242487
Test Loss after epoch 70: 6.17167122
Test Loss after epoch 71: 6.17274267
Test Loss after epoch 72: 6.16943763
Test Loss after epoch 73: 6.17143324
Test Loss after epoch 74: 6.17177477
Test Loss after epoch 75: 6.17102418
Test Loss after epoch 76: 6.16958155
2017-04-01 17:09:43.711715: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 32957627 get requests, put_count=32957625 evicted_count=53000 eviction_rate=0.00160813 and unsatisfied allocation rate=0.00160967
Test Loss after epoch 77: 6.16875180
Test Loss after epoch 78: 6.17041442
Test Loss after epoch 79: 6.17169874
Test Loss after epoch 80: 6.17046326
Test Loss after epoch 81: 6.17285912
Test Loss after epoch 82: 6.17165841
Test Loss after epoch 83: 6.17207475
Test Loss after epoch 84: 6.17038390
Test Loss after epoch 85: 6.17065656
Test Loss after epoch 86: 6.17124518
Test Loss after epoch 87: 6.16970193
Test Loss after epoch 88: 6.17081295
Test Loss after epoch 89: 6.17060283
Test Loss after epoch 90: 6.17180191
2017-04-01 17:35:57.686547: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 38988584 get requests, put_count=38988582 evicted_count=63000 eviction_rate=0.00161586 and unsatisfied allocation rate=0.00161717
Test Loss after epoch 91: 6.17362348
Test Loss after epoch 92: 6.17360422
Test Loss after epoch 93: 6.17188638
Test Loss after epoch 94: 6.17232193
Test Loss after epoch 95: 6.17320942
Test Loss after epoch 96: 6.17232188
Test Loss after epoch 97: 6.17150820
Test Loss after epoch 98: 6.17234693
Test Loss after epoch 99: 6.17296412
Test Loss after epoch 100: 6.17208157
Test Loss after epoch 101: 6.17273955
Test Loss after epoch 102: 6.17288951
Test Loss after epoch 103: 6.17205083
Test Loss after epoch 104: 6.17221174
2017-04-01 18:01:28.327420: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 44848355 get requests, put_count=44848350 evicted_count=73000 eviction_rate=0.00162771 and unsatisfied allocation rate=0.00162891
Test Loss after epoch 105: 6.17134876
Test Loss after epoch 106: 6.17430385
Test Loss after epoch 107: 6.17453141
Test Loss after epoch 108: 6.17317138
Test Loss after epoch 109: 6.17317227
Test Loss after epoch 110: 6.17500346
Test Loss after epoch 111: 6.17404004
Test Loss after epoch 112: 6.17470510
Test Loss after epoch 113: 6.17260637
Test Loss after epoch 114: 6.17467181
Test Loss after epoch 115: 6.17495884
Test Loss after epoch 116: 6.17467383
Test Loss after epoch 117: 6.17480127
2017-04-01 18:25:34.293341: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 50385993 get requests, put_count=50385992 evicted_count=83000 eviction_rate=0.00164728 and unsatisfied allocation rate=0.00164828
Test Loss after epoch 118: 6.17566987
Test Loss after epoch 119: 6.17431077
Test Loss after epoch 120: 6.17474651
Test Loss after epoch 121: 6.17506687
Test Loss after epoch 122: 6.17528645
Test Loss after epoch 123: 6.17527404
Test Loss after epoch 124: 6.17614838
Test Loss after epoch 125: 6.17534073
Test Loss after epoch 126: 6.17540954
Test Loss after epoch 127: 6.17644489
Test Loss after epoch 128: 6.17664780
Test Loss after epoch 129: 6.17660678
Test Loss after epoch 130: 6.17764301
2017-04-01 18:50:07.118773: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 56030200 get requests, put_count=56030190 evicted_count=93000 eviction_rate=0.00165982 and unsatisfied allocation rate=0.00166087
Test Loss after epoch 131: 6.17751070
Test Loss after epoch 132: 6.17817516
Test Loss after epoch 133: 6.17860533
Test Loss after epoch 134: 6.17808477
Test Loss after epoch 135: 6.17937014
Test Loss after epoch 136: 6.17894499
Test Loss after epoch 137: 6.17799749
Test Loss after epoch 138: 6.18150968
Test Loss after epoch 139: 6.18084380
Test Loss after epoch 140: 6.18014654
Test Loss after epoch 141: 6.18235658
Test Loss after epoch 142: 6.18252932
2017-04-01 19:13:21.360883: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 61372938 get requests, put_count=61372938 evicted_count=103000 eviction_rate=0.00167826 and unsatisfied allocation rate=0.00167906
Test Loss after epoch 143: 6.18318373
Test Loss after epoch 144: 6.18177163
Test Loss after epoch 145: 6.18296327
Test Loss after epoch 146: 6.18365850
Test Loss after epoch 147: 6.18288003
Test Loss after epoch 148: 6.18306988
Test Loss after epoch 149: 6.18400377
Test Loss after epoch 150: 6.18468433
Test Loss after epoch 151: 6.18471216
Test Loss after epoch 152: 6.18440184
Test Loss after epoch 153: 6.18518627
Test Loss after epoch 154: 6.18603189
2017-04-01 19:36:08.344065: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 66612642 get requests, put_count=66612642 evicted_count=113000 eviction_rate=0.00169637 and unsatisfied allocation rate=0.00169711
Test Loss after epoch 155: 6.18512599
Test Loss after epoch 156: 6.18738610
Test Loss after epoch 157: 6.18511786
Test Loss after epoch 158: 6.18579583
Test Loss after epoch 159: 6.18729255
Test Loss after epoch 160: 6.18853333
Test Loss after epoch 161: 6.18930849
Test Loss after epoch 162: 6.18947809
Test Loss after epoch 163: 6.19150184
Test Loss after epoch 164: 6.19148289
Test Loss after epoch 165: 6.19198022
Test Loss after epoch 166: 6.19119608
Test Loss after epoch 167: 6.19095915
2017-04-01 19:59:34.729877: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 71995831 get requests, put_count=71995831 evicted_count=123000 eviction_rate=0.00170843 and unsatisfied allocation rate=0.00170911
Test Loss after epoch 168: 6.19339659
Test Loss after epoch 169: 6.19493602
Test Loss after epoch 170: 6.19343437
Test Loss after epoch 171: 6.19453154
Test Loss after epoch 172: 6.19487650
Test Loss after epoch 173: 6.19634326
Test Loss after epoch 174: 6.19699044
Test Loss after epoch 175: 6.19728921
Test Loss after epoch 176: 6.19976063
Test Loss after epoch 177: 6.19985176
Test Loss after epoch 178: 6.19970641
Test Loss after epoch 179: 6.19939436
Test Loss after epoch 180: 6.20072326
2017-04-01 20:24:46.086139: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 77785820 get requests, put_count=77785808 evicted_count=133000 eviction_rate=0.00170982 and unsatisfied allocation rate=0.00171061
Test Loss after epoch 181: 6.20254670
Test Loss after epoch 182: 6.20114471
Test Loss after epoch 183: 6.20330824
Test Loss after epoch 184: 6.20655487
Test Loss after epoch 185: 6.20842554
Test Loss after epoch 186: 6.20761956
Test Loss after epoch 187: 6.20846917
Test Loss after epoch 188: 6.20786037
Test Loss after epoch 189: 6.20817849
Test Loss after epoch 190: 6.21016719
Test Loss after epoch 191: 6.21202569
Test Loss after epoch 192: 6.21109826
Test Loss after epoch 193: 6.21328168
Test Loss after epoch 194: 6.21608753
2017-04-01 20:50:03.308139: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 83589743 get requests, put_count=83589742 evicted_count=143000 eviction_rate=0.00171074 and unsatisfied allocation rate=0.00171133
Test Loss after epoch 195: 6.21549557
Test Loss after epoch 196: 6.21711200
Test Loss after epoch 197: 6.21999956
Test Loss after epoch 198: 6.22051509
Test Loss after epoch 199: 6.22127929
Test Loss after epoch 200: 6.22419698
Test Loss after epoch 201: 6.22544821
Test Loss after epoch 202: 6.22583315
Test Loss after epoch 203: 6.22871219
Test Loss after epoch 204: 6.22987832
Test Loss after epoch 205: 6.23161227
Test Loss after epoch 206: 6.23288905
2017-04-01 21:13:22.952762: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 88956187 get requests, put_count=88956187 evicted_count=153000 eviction_rate=0.00171995 and unsatisfied allocation rate=0.0017205
Test Loss after epoch 207: 6.23261467
Test Loss after epoch 208: 6.23914545
Test Loss after epoch 209: 6.24061044
Test Loss after epoch 210: 6.24156159
Test Loss after epoch 211: 6.24350163
Test Loss after epoch 212: 6.24770047
Test Loss after epoch 213: 6.24881252
Test Loss after epoch 214: 6.25135962
Test Loss after epoch 215: 6.25473400
Test Loss after epoch 216: 6.25702472
Test Loss after epoch 217: 6.26200716
Test Loss after epoch 218: 6.26471122
Test Loss after epoch 219: 6.26744656
2017-04-01 21:37:56.020068: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 94599267 get requests, put_count=94599265 evicted_count=163000 eviction_rate=0.00172306 and unsatisfied allocation rate=0.0017236
Test Loss after epoch 220: 6.27192096
Test Loss after epoch 221: 6.27489690
Test Loss after epoch 222: 6.27944371
Test Loss after epoch 223: 6.28387474
Test Loss after epoch 224: 6.28666227
Test Loss after epoch 225: 6.29168818
Test Loss after epoch 226: 6.29579548
Test Loss after epoch 227: 6.29693892
Test Loss after epoch 228: 6.30315968
Test Loss after epoch 229: 6.30818186
Test Loss after epoch 230: 6.31419672
Test Loss after epoch 231: 6.31993710
Test Loss after epoch 232: 6.32697802
2017-04-01 22:02:19.594281: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 100206609 get requests, put_count=100206609 evicted_count=173000 eviction_rate=0.00172643 and unsatisfied allocation rate=0.00172692
Test Loss after epoch 233: 6.33315815
Test Loss after epoch 234: 6.33898275
Test Loss after epoch 235: 6.34664394
Test Loss after epoch 236: 6.35320981
Test Loss after epoch 237: 6.36218518
Test Loss after epoch 238: 6.36858009
Test Loss after epoch 239: 6.38041901
Test Loss after epoch 240: 6.39142698
Test Loss after epoch 241: 6.40135623
Test Loss after epoch 242: 6.41172427
Test Loss after epoch 243: 6.42678222
Test Loss after epoch 244: 6.43522752
2017-04-01 22:25:28.530381: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 105534346 get requests, put_count=105534344 evicted_count=183000 eviction_rate=0.00173403 and unsatisfied allocation rate=0.00173452
Test Loss after epoch 245: 6.45181250
Test Loss after epoch 246: 6.46263990
Test Loss after epoch 247: 6.47665764
Test Loss after epoch 248: 6.49142167
Test Loss after epoch 249: 6.50581471
Test Loss after epoch 250: 6.52692416
Test Loss after epoch 251: 6.54639056
Test Loss after epoch 252: 6.56402428
Test Loss after epoch 253: 6.58195250
Test Loss after epoch 254: 6.60621915
Test Loss after epoch 255: 6.63278548
Test Loss after epoch 256: 6.65286462
Test Loss after epoch 257: 6.67982981
2017-04-01 22:50:02.590267: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 111182051 get requests, put_count=111182047 evicted_count=193000 eviction_rate=0.00173589 and unsatisfied allocation rate=0.00173637
Test Loss after epoch 258: 6.70597358
Test Loss after epoch 259: 6.72423882
Test Loss after epoch 260: 6.75665462
Test Loss after epoch 261: 6.77846216
Test Loss after epoch 262: 6.80959093
Test Loss after epoch 263: 6.84462416
Test Loss after epoch 264: 6.87765292
Test Loss after epoch 265: 6.90781106
Test Loss after epoch 266: 6.94639525
Test Loss after epoch 267: 6.98110896
Test Loss after epoch 268: 7.01040638
Test Loss after epoch 269: 7.04522857
Test Loss after epoch 270: 7.08250890
2017-04-01 23:14:22.050627: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 116775563 get requests, put_count=116775563 evicted_count=203000 eviction_rate=0.00173838 and unsatisfied allocation rate=0.0017388
Test Loss after epoch 271: 7.12164090
Test Loss after epoch 272: 7.14882010
Test Loss after epoch 273: 7.18821249
Test Loss after epoch 274: 7.23034805
Test Loss after epoch 275: 7.26009065
Test Loss after epoch 276: 7.29205606
Test Loss after epoch 277: 7.33199733
Test Loss after epoch 278: 7.36700374
Test Loss after epoch 279: 7.39938731
Test Loss after epoch 280: 7.43176534
Test Loss after epoch 281: 7.46539358
Test Loss after epoch 282: 7.50716733
Test Loss after epoch 283: 7.53991926
2017-04-01 23:38:17.537248: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 122270500 get requests, put_count=122270499 evicted_count=213000 eviction_rate=0.00174204 and unsatisfied allocation rate=0.00174245
Test Loss after epoch 284: 7.57330285
Test Loss after epoch 285: 7.60461485
Test Loss after epoch 286: 7.64195717
Test Loss after epoch 287: 7.68277956
Test Loss after epoch 288: 7.71212169
Test Loss after epoch 289: 7.75321243
Test Loss after epoch 290: 7.79355405
Test Loss after epoch 291: 7.83507675
Test Loss after epoch 292: 7.86496664
Test Loss after epoch 293: 7.90679711
Test Loss after epoch 294: 7.95520145
Test Loss after epoch 295: 7.99812689
Test Loss after epoch 296: 8.04220630
2017-04-02 00:02:51.442238: I tensorflow/core/common_runtime/gpu/pool_allocator.cc:247] PoolAllocator: After 127913041 get requests, put_count=127913033 evicted_count=223000 eviction_rate=0.00174337 and unsatisfied allocation rate=0.00174382
Test Loss after epoch 297: 8.07772873
Test Loss after epoch 298: 8.11860512
Test Loss after epoch 299: 8.15734227
